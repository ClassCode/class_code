<?php 

  // Test looking at https://pixees.fr/wp-content/plugins/class_code/attestation/?raw=tr

include_once(plugin_dir_path( __FILE__ ).'../oc_api/OpenClassroomsAPI.php'); 
include_once(plugin_dir_path( __FILE__ ).'../bp-profiles.php'); 
include_once(plugin_dir_path( __FILE__ ).'../rencontre/rencontre_post_type.php'); 

function get_user_learning_data($userID, $withOC) {
  global $OpenClassroomsAPI;
  $classcodeUrl='';
  if(empty($_SERVER["HTTPS"])){
    $classcodeUrl='http://';
  }else{
    $classcodeUrl='https://';
  }
  $classcodeUrl.=$_SERVER["SERVER_NAME"];
   
  $modules = array(
		   1 => array("title" => "#1 Module fondamental : découvrir la programmation créative", "img" => $classcodeUrl."/wp-content/uploads/2016/03/formation_en_ligne_classcode_module_1_prog-creativ-300x211.png", "rencontres" => array(), "content" => '', "slug" => "decouvrir-la-programmation-creative"),
		   2 => array("title" => "#2 Module thématique : manipuler l’information", "img" => $classcodeUrl."/wp-content/uploads/2016/03/formation_en_ligne_classcode_module_2_manip-info-300x211.png", "rencontres" => array(), "content" => '', "slug" => "manipuler-l-information"),
		   3 => array("title" => "#3 Module thématique : diriger les robots", "img" => $classcodeUrl."/wp-content/uploads/2016/03/formation_en_ligne_classcode_module_4_diriger-robot-300x211.png", "rencontres" => array(), "content" => '', "slug" => "s-initier-a-la-robotique"),
		   4 => array("title" => "#4 Module thématique : connecter le réseau", "img" => $classcodeUrl."/wp-content/uploads/2016/03/formation_en_ligne_classcode_module_3_connecter-reseau-300x211.png", "rencontres" => array(), "content" => '', "slug" => "connecter-le-reseau"),
		   5 => array("title" => "#5 Module fondamental : le processus de création de A à Z", "img" => $classcodeUrl."/wp-content/uploads/2016/03/formation_en_ligne_classcode_module_5_process-crea-300x211.png", "rencontres" => array(), "content" => '', "slug" => "gerer-un-projet-informatique-avec-des-enfants"),
		   0 => array("title" => "#0 Autres activités", "img" => $classcodeUrl."/wp-content/uploads/2017/10/image1.jpg", "rencontres" => array(), "content" => '', "slug" => ""),
		   // This is added because there is a strange PHP the last array element is overwritten by the last before last
		   array("title" => "", "img"=> "", "rencontres" => array(), "content" => '', "slug" => ""));
  
  //- echo "<pre>modules:".print_r($modules, true)."</pre>\n";

  if ($withOC)
    $courses = $OpenClassroomsAPI->getUserLearningActivity($withOC);
  $user_data = get_user_meta($userID, 'OpenClassroomsAPI/UserData', true);
  //- echo "<pre>user_data:".print_r($user_data, true)."</pre>\n";
  if (is_array($user_data)) {
    $user_remote_profile = $user_data['user_public_profile'];
    if (!$withOC)
      $courses = $user_data['user_learning_activity'];
  }
  $user_local_profile =
    array('ID' =>$userID,
	  'Prénom' => bp_get_profile_field_data(array('field' => 'Prénom', 'user_id' => $userID)),
	  'Nom' => bp_get_profile_field_data(array('field' => 'Nom', 'user_id' => $userID)),
	  'Login' => get_user_by('id', $userID)->display_name,
	  'Email' => get_user_by('id', $userID)->user_email,
	  'Profil de l´apprenant'  => bp_get_profile_field_data(array('field' => 'Profil', 'user_id' => $userID)),
	  'Discipline informatique'  => bp_get_profile_field_data(array('field' => 'Chercheur Etudiant Industriel Precision', 'user_id' => $userID)),
	  'Matière enseignée en tant que professeur'  => bp_get_profile_field_data(array('field' => 'Matière enseignée', 'user_id' => $userID)),
	  'Matière suivie en tant qu´étudiant'  => bp_get_profile_field_data(array('field' => 'Matière suivie', 'user_id' => $userID)),
	  'Facilitateur' => bp_get_profile_field_data(array('field' => 'Facilitateur', 'user_id' => $userID)),
	  'Compétences partagées ici'  => bp_get_profile_field_data(array('field' => 'Mes compétences', 'user_id' => $userID)),
	  'Structure d’appartenance' => bp_get_profile_field_data(array('field' => 'Structure', 'user_id' => $userID)),
	  'Cadre de la formation' => bp_get_profile_field_data(array('field' => 'Cadre', 'user_id' => $userID)),
	  'Localisation régionale' => rencontre_post_type::get_location($userID, "members", "region"),
	  'Pourcentage de participation au module #1' => 0,
	  'Pourcentage de participation au module #2' => 0,
	  'Pourcentage de participation au module #3' => 0,
	  'Pourcentage de participation au module #4' => 0,
	  'Pourcentage de participation au module #5' => 0,
	  'Nombre de participation à une rencontre' => 0,
	  'Nombre d´organisation de rencontre' => 0,
	  );

  $user_local_profile['Facilitateur'] = (isset($user_local_profile['Facilitateur'][0]) && $user_local_profile['Facilitateur'][0] != '') ? "oui" : "non";
  
  //- echo "<pre>courses:".print_r($courses, true)."</pre>\n";
  if (is_array($courses))
    foreach($courses as $course) 
      foreach($modules as $index => &$module)
      if ($module['slug'] == $course['slug']) {
	if (isset($course['table-of-content']) && isset($course['table-of-content']['children'])) {
	  $done = 0; $all = 0;
	  foreach($course['table-of-content']['children'] as $section) 
	    if (isset($section['children']))
	      foreach($section['children'] as $unit) {
		$all++;
		if ($unit['isCompleted'] || $unit['isDone'] || $unit['isPassed'])
		  $done++;
	      }
	  if ($all > 0)
	    $module['done'] = round(100*$done/$all);
	  else
	    $module['done'] = 0;
	} else
	  $module['done'] = 0;
	$module['content'] .= $module['done'] == 0 ? 
	  '<p>Participation au cours en ligne.</p>' : 
	  '<p>Participation à '.$module['done'].'% du cours en ligne.</p>';
	$user_local_profile["Pourcentage de participation au module #".$index] = $module['done'];
      }
  
  $rencontres = rencontre_post_type::get_rencontres(array_merge(array("mes_rencontres" => true, "user_id" => $userID, "when" => "past")));
  //- echo "<pre>".print_r($rencontres, true)."</pre>\n";
  foreach($rencontres as $rencontre) {
    $tobeadded = true;
    foreach($modules as &$module)
      if ($module['title'] == $rencontre['module']) {
	$module['rencontres'][] = $rencontre;
	$tobeadded = false;
      }
    if ($tobeadded)
      $modules[0]['rencontres'][] = $rencontre;
  }
  //- echo "<pre>modules:".print_r($modules, true)."</pre>\n";
  
  foreach($modules as &$module) {
    foreach($module['rencontres'] as $rencontre) {
      $date1 = get_post_meta($rencontre['post']->ID, 'rencontre_date_1', true);
      $date2 = get_post_meta($rencontre['post']->ID, 'rencontre_date_2', true);
      $et = $date1 != '' && $date2 != '';
      $module['content'] .= '<p>'.($rencontre['post']->post_author == $userID ? 'Organisation de' : 'Participation à').' la rencontre <a href='.$classcodeUrl.'/rencontre/'.$rencontre['post']->ID.'">#'.$rencontre['post']->ID.'</a>, '.
	($date1 != '' ? 'le '.$date1 : '').($date1 != '' && $date2 != '' ? ' et ' : '').($date2 != '' ? 'le '.$date2 : '').($date1 != '' || $date2 != '' ? ', ' : '').
	rencontre_post_type::get_location($rencontre['post']->ID, "posts", "address").'.</p>';
      $user_local_profile[$rencontre['post']->post_author == $userID ? 'Nombre d´organisation de rencontre' : 'Nombre de participation à une rencontre']++;
    }
  }
  unset($modules[6]);
  return array('user_local_profile' => $user_local_profile,
	       'user_remote_profile' => $user_remote_profile,
	       'courses' => $courses,
	       'rencontres' => $rencontres,
	       'modules' => $modules);
}
?>