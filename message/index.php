<?php
define('WP_USE_THEMES', false);
require_once("../../../../wp-load.php");
include_once(plugin_dir_path( __FILE__ ).'./rencontre_messages.php'); 

$classcodeUrl='';
if(empty($_SERVER["HTTPS"])){
  $classcodeUrl='http://';
}else{
  $classcodeUrl='https://';
}
$classcodeUrl.=$_SERVER["SERVER_NAME"];

if(wp_get_current_user()->ID == 0) {
  header('Location: '.wp_login_url(empty($_SERVER["HTTPS"]) ? "http://" : "https://").$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]);
  exit;
 }

// Ici https://pixees.fr/wp-content/plugins/class_code/message?who=nicename permet à un administrateur de voir le message du lundi de qq
$user = in_array('administrator', wp_get_current_user()->roles) && isset($_REQUEST['who']) ? 
  get_user_by('slug', $_REQUEST['who']) : wp_get_current_user();
?>

<?php header('X-Frame-Options: GOFORIT'); ?>
<?php include_once(get_template_directory().'/_inc/display-functions.php'); ?>
<!DOCTYPE html>
<html>
 <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>"/>    
  <title>Message hebdomadaire Class'Code</title>
  <link href="<?php the_theme_file('/classcode.css');?>" type="text/css" rel="stylesheet" />
  <?php wp_head(); ?>
</head>
<body id="body" style="padding:0px;margin-left:auto;margin-right:auto;">  
<div style="padding:0px 10px;">
  <?php rencontre_messages::update_rencontres($user->ID); echo str_replace('<p align="right"><i>Cliquer <a href="'.$classcodeUrl.'/wp-content/plugins/class_code/message/">ICI</a> si ce message s´affiche mal.</i></p>', '', rencontre_messages::get_monday_message($user, true)); ?>
</div>
</body></html>
