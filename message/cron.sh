#!/bin/bash

export LANG="fr_FR.UTF-8"
export DISPLAY=:0

#
# Time should be specified as minute, hour, day of month, month, day of week as following.
#  .---------------- minute (0 - 59)
#  |  .-------------- hour (0 - 23)
#  |  |  .----------- day of month (1 - 31)
#  |  |  |  .--------- month (1 - 12) OR Jan,Feb,Mar,Apr ...
#  |  |  |  |  .------ day of week (0 - 6) (Sunday=0 or 7) OR sun,Mon,Tue,wed,Thu,Fri,Sat
#  |  |  |  |  |
#  */2  *  *  *  * [command_to_be_executed] # every 2 minuts
#  0  8  *  * 1    [command_to_be_executed] # every monday, at 08:00
#  
# Notations
# - 5 (un nombre) : exécuté lorsque le champ prend la valeur 5 ;
# - * : exécuté tout le temps (toutes les valeurs sont bonnes) ;
# - 3,5,10 : exécuté lorsque le champ prend la valeur 3, 5 ou 10. Ne pas mettre d'espace après la virgule ;
# - 3-7 : exécuté pour les valeurs 3 à 7 ;
# - */3 : exécuté tous les multiples de 3 (par exemple à 0 h, 3 h, 6 h, 9 h…).
#
# Commandes:
# |Editing>crontab -e
# |Listing>crontab -l
#
# Ref: https://doc.ubuntu-fr.org/cron
#

cd `dirname $0`

php -r "require_once('../../../../wp-load.php'); include_once('./rencontre_messages.php'); rencontre_messages::send_monday_mails();" >> /var/log/class_code_cron_stdout.log 2 >> /var/log/class_code_cron_stderr.log

# Log visible dans https://pixees.fr/wp-admin/plugin-editor.php?file=class_code%2Flog%2Fclass_code_cron_stdout.log.php&plugin=class_code%2Flog%2Fclass_code_cron_stderr.log.php
