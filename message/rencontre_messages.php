<?php
include_once(plugin_dir_path( __FILE__ ).'../rencontre/rencontre_post_type.php'); 
include_once(plugin_dir_path( __FILE__ ).'../structures/ClassCode_config_coordinations.php');
include_once(plugin_dir_path( __FILE__ ).'./rencontre_messages_refusing.php');

/**
 * Defines the mechanism to manage user messages regarding Class'Code hybrid formation hangout.
 * Ref: https://docs.google.com/document/d/1GHEQXJfsfyrw4Gk0Jn25bCViWNm0ReDcR64lhqBGgfs/edit?ts=57dfa1bd
 */
class rencontre_messages {
  /** Sends the monday morning mail. */
  public static function send_monday_mails() {
    return; // Coupé pour lété ...
    self::update_rencontres(true);
    $subjects = 
      array("Class´Code: le message coup de pouce.",
	    "Class´Code: votre point hebdomadaire.",
	    "Class´Code: les informations de la semaine.",
	    "Class´Code: quoi de neuf cette semaine ?",
	    );
    srand();
    $subject = $subjects[rand(0, count($subjects) - 1)];
    foreach(get_users() as $user)
      self::send_mail($user->ID, $subject, self::get_monday_message($user));
    self::send_message(get_user_by('slug', 'tvieville')->ID, $subject, "Le mail hebdomadaire du lundi matin est bien parti");
  }
  /** Sends the rencontre inscription message. */
  public static function send_inscription_message($rencontre_id, $user_id) {
    $rencontre_data = self::get_rencontre_data($rencontre_id);
    $facilitateur = bp_get_profile_field_data(array('field' => 'Facilitateur', 'user_id' => $user_id));
    $message = '
<p>Vous êtes inscrit aux temps de rencontre pour le module <i>'.$rencontre_data['module'].'</i> qui ont lieu à <i>'.$rencontre_data['lieu'].'</ii> aux dates suivantes : <i>'.$rencontre_data['date1'].'</i> et <i>'.$rencontre_data['date2'].'</i></p>';
    if (!$facilitateur)
      $message .= '<p>D’ici là, n’oubliez pas de démarrer le module de formation en ligne ! Vous profiterez au mieux de ces rencontres après avoir suivi la semaine 1 et 3 de la formation en ligne.</p>';
    $message .= '<p>Sur la <a target="_blank" href="https://pixees.fr/rencontre/'.$rencontre_id.'#classCodeMeetingResourcesConsultation">page de la rencontre</a> vous pouvez consulter les kits, utiliser le pad pour interagir avec les autres personnes, etc.</p>';
    if ($facilitateur)
      $message = '
<p>Merci de votre contribution à la formation Class’Code en tant que facilitateur !</p>
'.$message.'
<p>Pour en savoir plus ne pas hésiter à <a target="_blank" href="https://pixees.fr/classcode/accueil/documentation/classcode-la-documentation-facilitateur/">consulter la documentation</a>.</p>';
    self::send_message($user_id, "À propos de votre inscription à une rencontre Class´Code", $message);
    // Send a mail to the organizer
    {
      $author_id = get_post($rencontre_id)->post_author;
      $user_slug = get_user_by('ID', $user_id)->user_nicename;
      $message = '<p>Sur la <a target="_blank" href="https://pixees.fr/rencontre/'.$rencontre_id.'#classCodeMeetingResourcesConsultation">page de la rencontre</a>, le participant <a target="_blank" href="https://pixees.fr/members/'.$user_slug.'">'.$user_slug.'</a> vient de s´inscrire';
      if ($facilitateur)
	$message .= ', c´est un-e facilitateur-e';
      $message .= '.</p>';
      self::send_message($author_id, "À propos d´une inscription à votre rencontre Class´Code", $message);
    }
  }
  /** Sends the rencontre creation message. */
  public static function send_creation_message($rencontre_id) {
    $message = '
<p>Merci de votre contribution à la formation Class’Code ! Votre rencontre <a target="_blank" href="https://pixees.fr/rencontre/'.$rencontre_id.'">@'.$rencontre_id.'</a> apparaît maintenant dans le calendrier et les participants peuvent désormais s’y inscrire.</p>
<p>Si vous rencontrez des difficultés, ou que le nombre de participants à la rencontre n’est pas suffisant, n’hésitez pas à prendre contact avec la <a target="_blank" href="https://pixees.fr/classcode/accueil/aide/#aideAction3">coordination de la région ou de la structure</a> qui pourra vous aider.</p>
<p>Pour en savoir plus ne pas hésiter à <a target="_blank" href="https://pixees.fr/classcode/accueil/documentation/classcode-la-documentation-organisateur/">consulter la documentation</a>.</p>';
    self::send_message(wp_get_current_user()->ID, "À propos de la création de votre rencontre Class´Code", $message);
    // Alert the coordination
    {
      foreach($data_coordinations as $data) {
	$about = '';
	if ($data['alert']) {
	  if ($data['structure'] && preg_match($data['structure'], get_post_meta($rencontre_id, 'structure', true)))
	    $about .= ' pour votre structure,';
	  if ($data['region'] && preg_match($data['region'], rencontre_post_type::get_location($rencontre_id, "posts", "region")))
	    $about .= ' pour votre région,';
	}
	$message = 
	  '<p>La rencontre <a target="_blank" href="https://pixees.fr/rencontre/'.$rencontre_id.'>@'.$rencontre_id.'</a> vient d´être crée par l´utilisateur <a target="_blank" href="https://pixees.fr/members/'.wp_get_current_user()->user_nicename.'">'.wp_get_current_user()->user_nicename.'</a>.</p>'.
	  '<p>Vous pouvez accéder aux données de votre coordination sur le <a target="_blank" href="https://pixees.fr/wp-content/plugins/class_code/structures/index.php">tableau de bord</a>.</p>';
	self::send_message(get_user_by('slug', $data['slug'])->ID, "À propos de la création d'une rencontre Class´Code", $message);
      }
    }
  }
  // This defines all messages to be sent
  private static $messages = 
    array(
	  'inscription' => '
<p>Vous êtes inscrit sur la plateforme de formation Class’Code et nous sommes ravis de vous compter parmi les futurs explorateurs du code et de la pensée informatique !</p>
<p>Le parcours se compose de 5 modules en ligne complétés par des temps deux temps de rencontre présentielle par module qui ont lieu près de chez vous !</p>
<p>Lancez-vous dans l’aventure Class’Code en vous inscrivant ou en créant un temps de rencontre près de chez vous.</p>
<p>En fin de module, n’hésitez pas à télécharger votre attestation de participation (en cours de mise en place) ! D’ici là, bonne découverte du premier module et à très vite!</p>',
	  'hello' => '
<p>Voici les données de votre activité à venir sur la plateforme de formation Class’Code.</p>',
	  'modifier-profil-header' => '
<p>Vous êtes inscrit sur la plateforme de formation Class’Code et nous sommes ravis de vous compter parmi les futurs explorateurs du code et de la pensée informatique !</p>',
	  'modifier-profil' => '<p>Dès maintenant, <a target="_blank" href="https://pixees.fr/members/@who@/profile/edit/group/1/">complétez votre profil</a> @what@ pour prendre part aux modules l’un après l’autre et trouver plus facilement un temps de rencontre près de chez vous, participer, aider en tant que facilitateur ou organiser des rencontres (voir le <a target="_blank" href="https://www.youtube.com/watch?v=Neeo_JdaY0s">tutoriel</a>).</p>',
	  'porte-entree' => '<p><img alt="module porte d´entrée" style="float:right;" width="120" src="https://project.inria.fr/classcode/files/2016/12/CS-First-Google-150x150.jpg"/><i>Manque de temps ? Trop de travail ? Autres priorités ?...</i><br/>Class´Code vous tente mais vous n´avez pas encore eu le temps de vous lancer vraiment ?<br/>Profitez en quelques minutes du parcours ``<a href="https://classcode.fr/formations/module0" target="_blank">porte d´entrée</a>´´ de Class´Code qui décortique pour vous la formation et vous donne les premières bases de la pensée informatique de façon synthétique. Des notions utiles pour briller en société et vous mettre en confiance... et puis qui sait ? Ces éléments introductifs vous donneront peut-être l´envie irrésistible d´aller plus loin !</p>',
	  'delay-avant-rappel' => 21, // jours
	  'rappel-facilitateur' => '<p>Vous vous êtes inscrit pour être facilitateur sur un ou plusieurs temps de rencontre Class’Code près de chez vous dans les mois à venir, merci de votre engagement !</p>@yadesrencontres@',
	  'rappel-participant' => '<p>Il y a quelques temps, vous vous êtes inscrit au programme de formation Class’Code.</p><!--p>N’hésitez pas à aller faire un tour sur la plateforme à nouveau et à vous inscrire aux temps de rencontre, dont vous auriez besoin.</p-->@yadesrencontres@',
	  'yadesrencontres' => '<p>Pour vous faciliter la vie et vous tenir informé, nous vous transmettons ici le calendrier des rencontres. Vous n’aurez plus qu’à choisir celle qui vous convient en fonction de votre emploi du temps.</p>',
	  // Here are the formated message elements
	  'header' => '
    <style type="text/css">
p {
      color:#000000;
      font-family:"Minion Pro", serif;
      font-size:16px;
      font-style:normal;
      font-variant:normal;
      font-weight:normal;
      line-height:1.2;
      margin-bottom:0px;
      margin-left:0px;
      margin-right:0px;
      text-align:justified;
      text-decoration:none;
      text-indent:0px;
}
p.title {
      color:#9b9da0;
      font-family:Arial, sans-serif;
      font-size:18px;
      font-style:normal;
      font-weight:bold;
      text-align:left;
}
p.trailer { 
      padding: 5px 20px;
      background-color:lightgray;
      font-size:12px;
      font-style:normal;
      text-align:center;
}
table.rencontres {
      width:100%;
      border:none;
}
    </style>
    <table style="width:100%;background-color:white;max-width:800px;border:1px solid gray;">
      <tr><td colspan="2"><img style="width:100%" src="https://pixees.fr/wp-content/plugins/class_code/message/header.png" /></td></tr>
      <tr><td valign="top"><img style="width:120px;" src="https://pixees.fr/wp-content/plugins/class_code/message/bonjour.png" /></td>
      <td style="padding-right:5px;"><p align="right"><i>Cliquer <a href="https://pixees.fr/wp-content/plugins/class_code/message/">ICI</a> si ce message s´affiche mal.</i></p>
        <p>Bonjour,</p>',
	  'trailer-body' => '
        <p>L’équipe de Class’Code.</p>
        <p style="float:right;"><small>Plus d´information ? <a href="https://twitter.com/classcode_fr"><img align="middle" height="25px;"src="https://project.inria.fr/classcode/files/2016/12/classcodesurtwitter.png"/></a> <a href="https://twitter.com/classcode_fr">@classcode_fr</a></small> </p>',
	  'header-rencontre' => '
      </td></tr>
      <tr><td valign="top"><img style="width:120px;" src="https://pixees.fr/wp-content/plugins/class_code/message/rencontres.png" /></td>
      <td style="padding-right:5px;"><p></p>',
	  'trailer' => '
      </td></tr>
      <tr><td align="center" colspan="2"><img style="width:90%" src="https://pixees.fr/wp-content/plugins/class_code/message/trailer.png" /></td></tr>
      <tr><td colspan="2"><p class="trailer">
Vous recevez cet email automatique (au plus hebdomadaire) car vous vous êtes inscrit sur <a target="_blank" href="https://classcode.fr">Class’Code</a>.<br/> 
Si vous avez des suggestions pour l’améliorer ou tout autre remarque, nous <a target="_blank" href="https://classcode.fr/aide">sommes à votre contact</a>.<br/>
Si vous souhaitez vous désabonner de ces messages, <a target="_blank" href="mailto:classcode-accueil@inria.fr?subject=Veuillez me désinscrire des emails envoyés par Class´Code&body=Bonjour, merci de me désinscrire des emails envoyés par Class´Code@whoID@. Bien Cordialement. P.S.: Je les recevrai de nouveau si je reprends mon activité sur Class´Code">cliquez ici</a>.<br/>
</p></td></tr>
    </table>',
	  );
  // Returns the monday user message
  public static function get_monday_message($user, $anyway = false) {
    $last_mail_time = get_user_meta($user->ID, "rencontre_messages/last_mail_time", true);
    $user_location = rencontre_post_type::get_location($user->ID, "members", "coordinates");
    $facilitateur = bp_get_profile_field_data(array('field' => 'Facilitateur', 'user_id' => $user->ID));
    $send = $anyway;
    $message = "";
    // Manages the 1st mail to a new user
    $now = time();
    if(!$last_mail_time) {
      $message.= self::$messages['inscription'];
      $send = true;
      $last_mail_time = 0;
    } else {
      // Remainders if inactive
      if (0 < $last_mail_time && $last_mail_time < $now - self::$messages['delay-avant-rappel'] * 24 * 3600) {
	$message.= $facilitateur ? 
	  self::$messages['rappel-facilitateur'] : 
	  self::$messages['rappel-participant'];
	$send = true;
      }
    }
    // Checks if the profile is ok
    {
      $missing = array();
      foreach(array('Prénom','Nom', 'Profil', 'Mes compétences') as $item) 
	if (bp_get_profile_field_data(array('field' => $item, 'user_id' => $user->ID)) == '')
	  $missing[] = $item == 'Mes compétences' ? ' Compétences' : $item;
      if (!$user_location)
	$missing[] = 'Localisation';
      $user_link = 'https://pixees.fr/members/'.$user->user_nicename.'/profile/edit/group/1/';
      if (count($missing) > 0) {
	if ($message == '') $message = self::$messages['modifier-profil-header'];
	$message .= str_replace(array('@who@', '@what@'), array($user->user_nicename, '('.join(', ', $missing).')'), self::$messages['modifier-profil']);
      }
    }
    // Adds recent rencontres message
    if (self::$recent_rencontres && isset(self::$recent_rencontres[$user->ID])) {
      if ($message == '') $message = self::$messages['hello'];
      foreach(self::$recent_rencontres[$user->ID] as $recent_rencontre)
	$message .= 
	($recent_rencontre['organizer'] ? '<p>Vous avez organisé le' : 
	 ($recent_rencontre['oneortwo'] == 1 ? 'Vous étiez inscrit au temps de rencontre au' : 'Votre deuxième')).' temps de rencontre suivant :<a target="_blank" href="https://pixees.fr/rencontre/'.$recent_rencontre['rencontre_id'].'">@'.$recent_rencontre['rencontre_id'].'</a>'.
	($recent_rencontre['oneortwo'] == 1 ? ' Nous vous donnons rendez-vous pour le temps de rencontre suivant.' : 
	 ($recent_rencontre['organizer'] || $facilitateur ? '>Merci beaucoup de votre contribution; n´hésitez pas à revenir nous aider !' : 
	  '</p> <p>Votre deuxième temps de rencontre vient de s’achever mais n’oubliez pas que la formation Class’Code comporte 5 modules... N’hésitez pas à poursuivre votre parcours en explorant un <a target="_blank" href="https://pixees.fr/classcode/accueil/#moocs">module suivant</a> et en vous inscrivant dès à présent au <a target="_blank" href="https://pixees.fr/classcode/rencontres/">prochain temps de rencontre</a> !')).'</p>';
      $message .= '<p>N´hésitez pas à contacter à prendre contact avec la <a target="_blank" href="https://pixees.fr/classcode/accueil/aide/#aideAction3">coordination de la région ou de la structure</a> pour rapporter un souci, partager une expérience, proposer une amélioration, nous recevrons avec plaisir votre évaluation concernant ce module.</p>';
      // '<p>Vous avez persévéré dans votre parcours de formation et vous voici maintenant armé pour commencer à animer des activités en présence de jeunes de 8 à 14 ans, vous pouvez désormais <a target="_blank" href="XXX">attester de votre parcours Class’Code</a>, vos nouvelles connaissances et compétences dans votre cursus professionnel.';
      $send = true;
    }
    if ($anyway && $message == '') {
      $message.= $facilitateur ? 
	self::$messages['rappel-facilitateur'] : 
	(self::$messages['rappel-participant'].self::$messages['porte-entree']);
      $send = true;
    }
    // Adds rencontre table
    $more = '';
    {
      if (self::$rencontre_organizers && isset(self::$rencontre_organizers[$user->ID]))
	$more.= self::get_html_table_rencontres('que vous organizez', self::$rencontre_organizers[$user->ID], $user_location);
      if (self::$rencontre_participants && isset(self::$rencontre_participants[$user->ID]))
	$more.= self::get_html_table_rencontres('aux quelles vous participez', self::$rencontre_participants[$user->ID], $user_location);
      if ($more == '') {
	$local_rencontres = array_map(function($rencontre) { return $rencontre['post']->ID; }, rencontre_post_type::get_rencontres(array("user_id" => $user->ID, "when" => "future", "nearby" => 50), "distance"));
	if (count($local_rencontres) > 0) {
	  $more2 = $facilitateur ? ', il manque peut-être un facilitateur' : '';
	  $more.= self::get_html_table_rencontres('près de chez vous (vous pouvez vous inscrire sur la page'.$more2.')', $local_rencontres, $user_location);
	} else
	  $send = true;
      }
      if ($more != '') {	
	if ($message == '') $message = self::$messages['hello'];
	$more = self::$messages['header-rencontre'].$more;
      }
      $message = str_replace('@yadesrencontres@', ($more != '' ? self::$messages['yadesrencontres'] : ''), $message);
    }
    // Returns message if not empty 
    if ($send && $message != '') {
      update_user_meta($user->ID, "rencontre_messages/last_mail_time", $now);
      return self::get_message($message, $more, $user->ID);
    } else 
      return false;
  }
  // Returns a formated message.
  // * @param $body The message body.
  // * @param $more An additional content after the letter signature
  // * @param $user_ID The recipient user ID.
  public static function get_message($body, $more = '', $userID = false) {
    return self::$messages['header'].$body.self::$messages['trailer-body'].$more.
      str_replace('@whoID@', ($userID ? ', mon ID est le @'.$userID : ''), self::$messages['trailer']);
  }
  // Returns a formated message.
  // * @param $user_id The recipient user ID.
  // * @param $subject The message subject
  // * @param $body The message body.
  // * @param $more An additional content after the letter signature
  public static function send_message($userID, $subject, $body, $more = '') {
    self::send_mail($userID, $subject, self::get_message($body, $more, $userID));
  }
  // Retrieves rencontres index for participants and organizers
  // * @param $for_user_id If true retrieves for all users, else if not false, retrieves for the given user ID.
  public static function update_rencontres($for_user_id = false) {
    self::$rencontre_organizers = array();
    self::$rencontre_participants = array();
    {
      $rencontres = rencontre_post_type::get_rencontres(array("when" => "future"), "date");
      foreach($rencontres as $rencontre) {
	$post_id = $rencontre['post']->ID;
	foreach(explode('|', get_post_meta($post_id, 'rencontre_participants', true)) as $user_id)
	  if ($user_id != '' && ($for_user_id === true || $for_user_id == $user_id)) {
	    if (!isset(self::$rencontre_participants[$user_id])) self::$rencontre_participants[$user_id] = array();
	    self::$rencontre_participants[$user_id][] = $post_id;
	  }
	$user_id = $rencontre['post']->post_author;
	if ($for_user_id === true || $for_user_id == $user_id) {
	  if (!isset(self::$rencontre_organizers[$user_id])) self::$rencontre_organizers[$user_id] = array();
	  self::$rencontre_organizers[$user_id][] = $post_id;
	}
      }
    }
    self::$recent_rencontres = array();
    {
      $rencontres = rencontre_post_type::get_rencontres(array("when_past" => "future"), "date");
      $now = time();
      foreach($rencontres as $rencontre) {
	$post_id = $rencontre['post']->ID;
	$date1 = get_post_meta($id, 'rencontre_date_1', true);
	$date1 = $date1 != '' ? date_timestamp_get(DateTime::createFromFormat('d/m/Y', $date1)) : 0;
	$date2 = get_post_meta($id, 'rencontre_date_2', true);
	$date2 = $date2 != '' ? date_timestamp_get(DateTime::createFromFormat('d/m/Y', $date2)) : 0;
	$recent1 = 0 < $date1 && $now - 3600 * 34 * 7 < $date1 && $date1 <= $now;
	$recent2 = 0 < $date2 && $now - 3600 * 34 * 7 < $date2 && $date2 <= $now;
	if ($recent1 || $recent2) {
	  foreach(explode('|', get_post_meta($post_id, 'rencontre_participants', true)) as $user_id)
	    if ($user_id != '' && ($for_user_id === true || $for_user_id == $user_id)) {
	      if (!isset(self::$recent_rencontres[$user_id])) self::$recent_rencontres[$user_id] = array();
	      self::$recent_rencontres[$user_id][] = 
		array('rencontre_id' => $post_id, 'oneortwo' => $recent2 ? 2 : 1, 'organizer' => false);
	    }
	  $user_id = $rencontre['post']->post_author;
	  if (!isset(self::$recent_rencontres[$user_id])) self::$recent_rencontres[$user_id] = array();
	  self::$recent_rencontres[$user_id][] = 
	    array('rencontre_id' => $post_id, 'oneortwo' => $recent2 ? 2 : 1, 'organizer' => true);
	}
      }
    }
  }
  // Rencontre indexes
  private static $rencontre_organizers = false, $rencontre_participants = false, $recent_rencontres = false;
  
  // Returns a table corresponding to rencontres IDs
  public static function get_html_table_rencontres($header, $rencontre_ids, $user_location = false,$map = false) {
    if (count($rencontre_ids) == 0)
      return '<p class="title">Pas de rencontre '.$header.'.</p>';
    $html = '<p class="title">'.(count($rencontre_ids) == 1 ? 'La rencontre' : 'Les rencontres').' '.$header.' :</p>';
    $htmlTable='';
    $htmlMapMarkers='';
    $meetings = array_map(function($rencontre_id) use ($user_location) { return self::get_rencontre_data($rencontre_id, $user_location); }, $rencontre_ids);
    usort($meetings, function($m1, $m2) { return $m1['distance'] - $m2['distance']; });
    $meeting_inc = 0;
    foreach($meetings as $meeting) {
      $htmlTable .= '<tr><td><a target="_blank" href="https://pixees.fr/rencontre/'.$meeting['id'].'#classCodeMeetingTierce">'.$meeting['module'].'</a></td><td>'.$meeting['lieu'].'</td>'.($user_location ? '<td>'.$meeting['distance'].'</td>' : '').'<td>'.$meeting['date1'].'</td><td>'.$meeting['date2'].'</td></tr>';
      if(isset($meeting['latlong']) && $meeting['latlong']!=''){         
          $htmlMapMarkers.= "meetingContentString[".$meeting_inc."] = '<div class=\"meetingMapInfoTitle\">".addslashes($meeting['module'])."</div>';";
          $htmlMapMarkers.= "meetingContentString[".$meeting_inc."]+=  '".($meeting['structure'] ? "<div>".addslashes($meeting['structure'])."</div>" : "")."';";
          $htmlMapMarkers.= "meetingContentString[".$meeting_inc."]+=  '<div>".addslashes($meeting['lieu'])."</div>';";
          $htmlMapMarkers.= "meetingContentString[".$meeting_inc."]+=  '".($meeting['date1'] ? "<div>Le ".$meeting['date1']."</div>" : "")."';";
          $htmlMapMarkers.= "meetingContentString[".$meeting_inc."]+=  '".($meeting['date2'] ? "<div>Le ".$meeting['date2']."</div>" : "")."';";
          if (is_user_logged_in()) {
            $htmlMapMarkers.= "meetingContentString[".$meeting_inc."]+=  '<a href=\"".$meeting['url']."\">Voir la rencontre</a>';";
          }
          $htmlMapMarkers.= "meetingInfowindow[".$meeting_inc."] = new google.maps.InfoWindow({content: meetingContentString[".$meeting_inc."]});";
          $htmlMapMarkers.= "var meetingLatLong = new google.maps.LatLng(".$meeting['latlong'].");";
          
          $htmlMapMarkers.= "meetingArrayMarker[".$meeting_inc."] = new google.maps.Marker({map: map,icon: meetingMarkerImage,position: meetingLatLong,title: 'Temps de rencontre',});";
          
          $htmlMapMarkers.= "meetingArrayMarker[".$meeting_inc."].addListener('click', function() { meetingInfowindow[".$meeting_inc."].open(map, meetingArrayMarker[".$meeting_inc."]);});";
      }
      $meeting_inc ++;
    }
    if($map){
      global $wpdb;
      $visitorId = bp_loggedin_user_id();  
      $visitorLocation = $wpdb->get_row($wpdb->prepare( "SELECT * FROM wppl_friends_locator WHERE member_id = %s", $visitorId ) );
      $visitorLat = $visitorLocation->lat;
      $visitorLong = $visitorLocation->long;
      $noVisitorLocation =false;
      $mapZoom = '11';
      $visitorLatLong =  '46.52863469527167,2.43896484375'; 
      $mapZoom = '5';

      $htmlMap.= '<div id="classCodeMeetingMap" class="contentMedia"></div>';
      $htmlMap.= '<script src="https://maps.googleapis.com/maps/api/js"></script>'; 
      $htmlMap.= '<script type="text/javascript">';
        $htmlMap.= "var meetingMarkerImage = '".get_template_directory_uri()."/_img/classcode_pictos/meeting/Img-rencontre-07.png';";
        $htmlMap.= 'jQuery( document ).ready(function() {';
        $htmlMap.= "var latlng = new google.maps.LatLng(".$visitorLatLong.");";

        $htmlMap.= 'loadGmap(latlng);';
        $htmlMap.= '});';

        $htmlMap.= 'function loadGmap(latlng) {';
          
        $htmlMap.= 'var options = {center: latlng,zoom: '.$mapZoom.',mapTypeId: google.maps.MapTypeId.ROADMAP};';
        $htmlMap.= 'map = new google.maps.Map(document.getElementById("classCodeMeetingMap"), options);';
        $htmlMap.= 'geocoder = new google.maps.Geocoder();';
        $htmlMap.= 'var meetingArrayMarker = [];';
        $htmlMap.= 'var meetingContentString = [];';
        $htmlMap.= 'var meetingInfowindow = [];';
        $htmlMap.= $htmlMapMarkers;
        $htmlMap.= "}";
      $htmlMap.= '</script>';
      
      $html .= "\n".$htmlMap."\n";
    }
    
    $html .= '<table class="rencontres"><tr><th>Module</th><th>Lieu</th>'.($user_location ? '<th>Distance (km)' : '').'</th><th>1ère date</th><th>2ème date</th></tr>';
    $html.= $htmlTable;
    return $html.'</table>';
  }
  // Returns a table corresponding to a rencontre data
  private static function get_rencontre_data($rencontre_id, $user_location = false) {
    global $wpdb;
    $postLocation = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM wp_places_locator WHERE post_id = %d", array( $rencontre_id ) ) );

    if(!isset($postLocation->lat) || $postLocation->lat =='' || !isset($postLocation->long) || $postLocation->long == ''){
      $latLong = '';
    }else{
      $latLong = strval($postLocation->lat).",".strval($postLocation->long);  
    }
    return array(
		 'id' => $rencontre_id,
	       'module' => get_post_meta($rencontre_id, 'rencontre_module', true),
	       'lieu' => rencontre_post_type::get_location($rencontre_id, "posts", "address"),
	       'distance' => $user_location ? round(rencontre_post_type::get_location_distance(rencontre_post_type::get_location($rencontre_id, "posts", "coordinates"), $user_location)) : '?',
	       'date1' => get_post_meta($rencontre_id, 'rencontre_date_1', true),
	       'date2' => get_post_meta($rencontre_id, 'rencontre_date_2', true),
         'structure' => get_post_meta($rencontre_id, 'structure', true),
         'url' => get_site_url()."/?post_type=rencontre&p=".$rencontre_id,
         'latlong' => $latLong
		 );
  }
  /** Properly sends a mail.
   * @param $user_id The recipient user ID.
   * @param $subject The message subject.
   * @param $message The message HTML text.
   */
  public static function send_mail($user_id, $subject, $message = false) {
    global $rencontre_messages_refusing; 
    $email = get_user_by('id', $user_id)->user_email;
    if ($message && !in_array($user_id, $rencontre_messages_refusing)) {
      self::write_log("To: ".$email."\n<br/>".$message);
      mail($email, $subject, $message, "From: classcode-accueil@inria.fr\r\nContent-type: text/html; charset=utf-8\r\nContent-Transfer-Encoding: 8bit\r\n");
    }
  }
  private static function write_log($message, $append = true) {
    $echo_file = plugin_dir_path( __FILE__ )."../../../uploads/ClassCodeMessages76099758412996.html";
    file_put_contents($echo_file,
		      "\n<hr><tt>[".date('c')."]</tt><br/>".preg_replace("/\s+/", " ", $message)."\n",
		      $append ? FILE_APPEND : 0);
    if (!$append)
      chmod($echo_file, 0777);
  }
  public static function clear_log() {
    self::write_log('<html><head><title>Page de log des messages de Class´Code</title><meta charset="utf-8"></head><body><tt>Clear de la page de log</tt>', false);
  }
}
?>