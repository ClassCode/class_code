var indexBanniere = 1;
function banniere() {
   var nbBanniere = 4;
   var numero = indexBanniere++ % nbBanniere; // Math.floor(Math.random() * nbBanniere);
    
   switch(numero) {
      case 0: 
         var contenu = '<div id="ban4" class="ban"><span><center><div style="padding-bottom:30px;"><i>Décodons le numérique !</i></div></br><div>TROUVEZ UNE OFFRE ADAPTEE</br>A <span style="color:#14BDEF">VOS</span> BESOINS</div></center></div>';
         break;
      case 1:
         var contenu = '<div id="ban1" class="ban"><span class="white small">Enseignantes, Animateurs, Médiatrices, Parents...</span></br><div style="padding-top:20px;"><span class="dark big">ACTEURS</span><span class="white big"> EDUCATIFS</span></div></div>';
         break;
      case 2:
         var contenu = '<div id="ban2" class="ban"><span class="white small">Informaticiennes, Etudiants, Développeurs...</span></br><div style="padding-top:20px;"><span class="dark big">ACTEURS</span><span class="white big"> DU NUMERIQUE</span></div></div>';
         break;
      case 3:
         var contenu = '<div id="ban3" class="ban"><span class="white small">Reservistes, Service Civique, Bénévoles, Curieux...</span></br><div style="padding-top:20px;"><span class="dark big">ACTEURS</span><span class="white big"> DE LA SOCIETE CIVILE</span></div></div>';
         break;
   }
   return '<a href="./menu.html" target="_blank">'+contenu+'</a>';
}
document.write('<div id="banniere">'+banniere()+'</div>');
var timer = window.setInterval("rotation()", 1800);
function rotation() {
  document.getElementById('banniere').innerHTML = banniere();
}
