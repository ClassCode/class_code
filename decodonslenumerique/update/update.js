// Ce script se lance avec :
// > node update.js
// et j ai du installer
// > npm install deasync
// > npm install csvtojson
 
// Loads some packages
const deasync = require('deasync');
const fs = require('fs');
const http = require('http');
const https = require('https');
const util = require('util');
const csvtojson = require('csvtojson');

/** Encapsulates a https request.
 * @param url A string. The request URL.
 * @return The http body.
 */
function http_request(url) {
  var done = false;
  var body = "";
  var protocol = url.indexOf("https") == 0 ? https : http;
  var req = protocol.request(url,
    function(res) {
      //console.log('statusCode: ', res.statusCode);
      //console.log('headers: ', res.headers);
      res.on('data', function(chunck) {
	  body += chunck.toString();
	});
      res.on('end', function() {
	  done = true;
	});
    });
  req.on('error', function(error) {
      console.error(error+" -> "+url);
      done = true;
    });
  req.end();
  while(!done) deasync.sleep(100);
  return body;
}

/** Converts a CSV string to JSON.
 * @param data The CSV string.
 * @return The related JSON structure.
 */
function csv_to_json(data) {
  var done = false;
  var json;
  var Converter = require("csvtojson").Converter;
  var converter = new Converter({});
  converter.fromString(data, function(error, result) { json = result; done = true; });
  while(!done) deasync.sleep(100);
  return json;
}

// Here is the code

var csv_data = http_request('https://docs.google.com/spreadsheets/d/1IUctlNpzosUc09e8umMzXyYEjSuFs9c2hX5lTS6cJu0/export?exportFormat=csv');

var json_data = csv_to_json(csv_data);

fs.writeFileSync('./matrice.json', JSON.stringify(json_data, null, '  '));
