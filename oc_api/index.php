<?php
require_once("../../../../wp-load.php");
include_once(plugin_dir_path( __FILE__ ).'./OpenClassroomsAPI.php'); 

// https://pixees.fr/wp-content/plugins/class_code/oc_api/?test=1[&clean=1]
if (isset($_REQUEST['test']) && $_REQUEST['test']) {
  require_once('../doc/error_for_all.php');
  echo "<pre>OK: we start the test</pre>";
  $OpenClassroomsAPI->do_token_request($_REQUEST);
  echo "<pre>OK: the http hook has been run</pre>";
  if (isset($_REQUEST['clean']) && $_REQUEST['clean']) {
    $OpenClassroomsAPI->clearAccessToken();  
    echo "<pre>OK: we re-connect</pre>";
  }
  $data = $OpenClassroomsAPI->getUserLearningActivity();
  echo "<pre>OK: we got the OC data:".print_r($data, true)."</pre>\n";
  exit;
}

$OpenClassroomsAPI->do_token_request($_REQUEST);
if (wp_get_current_user()->ID > 0)
  $OpenClassroomsAPI->noticePage();
else
  $OpenClassroomsAPI->userConnect();
OpenClassroomsAPI::httpRedirect("https://classcode.fr", true);

?>
