<?php
define('WP_USE_THEMES', false);
require_once("../../../../wp-load.php");

$classcodeUrl='';
if(empty($_SERVER["HTTPS"])){
  $classcodeUrl='http://';
}else{
  $classcodeUrl='https://';
}
$classcodeUrl.=$_SERVER["SERVER_NAME"];

$codes = get_option('classcode/codes-promo-OC'); if (!is_array($codes)) $codes = array();
if (isset($codes[''])) unset($codes['']);

$count_used = 0; foreach($codes as $code => $email) if ($email) $count_used++;

// https://pixees.fr/wp-content/plugins/class_code/code-premium/index.php?load-the-codes=please
if (isset($_REQUEST['load-the-codes']) &&  $_REQUEST['load-the-codes'] == "please") {
  foreach($codes as $code => $email)
    if($email == false)
      unset($codes[$code]);
  require_once('codes-promo-OC.php');
  // https://pixees.fr/wp-content/plugins/class_code/code-premium/index.php?load-the-codes=please&purge-the-code=
  if (isset($_REQUEST['purge-the-code'])) $codes[$_REQUEST['purge-the-code']] = false;
  $count_new = 0;
  foreach($codes_promo as $code)
    if (!isset($codes[$code])) {
      $codes[$code] = false;
      $count_new++;
    }
  update_option('classcode/codes-promo-OC', $codes);
  echo "<pre>Fichier de codes : ".count($codes_promo)." codes dont $count_new chargés\nTables de codes  : ".count($codes_promo)." codes dont $count_used utilisés (".(count($codes) - $count_used)." disponibles)\n</pre>\n";
  //echo "<pre>".print_r($codes, true)."</pre>\n";
  foreach($codes as $code => $email) if ($email) echo "<p>$code => $email</p>\n";
  exit(0);
 }

$what = "";
if (isset($_REQUEST['who']) && ($_REQUEST['who'] != '') && ((strlen($_REQUEST['who']) * 104729) % 10000) == $_REQUEST['ohw']) {
  if ($count_used < count($codes)) {
    $your_code = false; foreach($codes as $code => $email) if ($email == $_REQUEST['who']) $your_code = $code;
    if ($your_code) {
      $what = "<i>Un code était déjà attribué à l'email <tt>".$_REQUEST['who']."</tt>, le voici <tt>$your_code</tt></i>";
    } else {
      foreach($codes as $code => $email) if (!$email) { $your_code = $code; $codes[$code] = $_REQUEST['who']; break; }
      update_option('classcode/codes-promo-OC', $codes);
      $what = "<i>Un code est donc attribué à l'email <tt>".$_REQUEST['who']."</tt>, le voici <tt>$your_code</tt></i>";
    }
  } else {
    echo "<p>Upss il n'y a plus de code disponibles, désolé, nous allons vous contacter</p>";
    mail("thierry.vieville@inria.fr", "Alerte: plus de code premium disponible sur ".$classcodeUrl."/wp-content/plugins/class_code/code-premium/index.php", "L'email ".$_REQUEST['who']." n'a pu avoir de code :(", "From: classcode-accueil@inria.fr\r\nContent-type: text/plain; charset=utf-8\r\nContent-Transfer-Encoding: 8bit\r\n");
    exit(0);
  }
 }
if($what == "") {
  if (isset($_REQUEST['who']) && $_REQUEST['who'] == '') 
    echo "<pre>Attention de bien remplir le champ email avant de cliquer sur le lien !</pre>";
  else
    echo "<pre>Incorrect acces to this page.</pre>";
  exit(0);
 }
?>

<?php header('X-Frame-Options: GOFORIT'); ?>
<?php include_once(get_template_directory().'/_inc/display-functions.php'); ?>
<!DOCTYPE html>
<html>
 <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>"/>    
  <title>Tableau de bord Class'Code</title>
  <link href="<?php the_theme_file('/classcode.css');?>" type="text/css" rel="stylesheet" />
  <?php wp_head(); ?>
</head>
<body id="body" style="padding:0px;margin-left:auto;margin-right:auto;background-color:white">
<img style="width:100%" src="<?php echo $classcodeUrl; ?>/wp-content/plugins/class_code/message/header.png" />
<div style="padding:0px 10px;">

<h3>Class'Code et OpenClassrooms vous offre un mois d'abonnement Premium</h3>
Félicitation, vous faites partie des 100 premier-e-s inscrit-e-s depuis le lancement de la nouvelle version de Class'Code !<br>
<br>Bénéficiez immédiatement d'un accès Premium à OpenClassrooms de 1 mois pour démarrer votre formation sur les chapeaux de roues<br>
- Grâce à ce code, visionner les vidéos sans limite directement sur OpenClassrooms<br>
- Passer autant de Quiz que vous le désirer et ce sur tout le catalogue OpenClassrooms<br>
- Finissez le MOOC et obtenez gratuitement la certification OpenClassrooms !<br>
<br>Alors n'attendez plus entrer le code dans le champ «J'ai un code promo» en suivant ce lien <a href="https://openclassrooms.com/premium">https://openclassrooms.com/premium</a>.<br>
<br><?php echo $what; ?><br>
<br>On se retrouve très bientôt sur un <a href="https://classcode.fr">temps de rencontre</a> près de chez vous&nbsp; ?<br>
<br>A très vite,<br>
<br>L'équipe Class'Code<br>
</div>
<img style="display:block;width:90%;margin-left:auto;margin-right:auto;" src="<?php echo $classcodeUrl; ?>/wp-content/plugins/class_code/message/trailer.png" />
</body>
</html>
