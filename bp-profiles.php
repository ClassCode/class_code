<?php

function display_classcode_profile($user_id){
  $argsFirstname = array(
    'field' => 'Prénom',
    'user_id' =>  $user_id
  );
  $Firstname = bp_get_profile_field_data($argsFirstname);
  $argsLastname = array(
    'field' => 'Nom',
    'user_id' =>  $user_id
  );
  $Lastname = bp_get_profile_field_data($argsLastname);
  $displayName = $Firstname." ".$Lastname;
  
  if($displayName == ' '){
    $displayName = get_user_by('id', $user_id)->display_name;   
  }
  $argsFacilitateur = array(
    'field' => 'Facilitateur',
    'user_id' => $user_id
  );
  $facilitateur = bp_get_profile_field_data($argsFacilitateur);
  if(isset($facilitateur[0]) && $facilitateur[0] != ''){
    $facilitateur = true;
  }else{
    $facilitateur = false;
  }
  $argsProfil = array(
    'field' => 'Profil',
    'user_id' => $user_id
  );
  $profil = bp_get_profile_field_data($argsProfil);
  
  $argsSkills = array(
    'field' => 'Mes compétences',
    'user_id' => $user_id
  );
  $skills = bp_get_profile_field_data($argsSkills);
  $skillsTab = explode(",",$skills);
  ob_start();
?>
  <!--div class="profilActionsView" ></div-->
  <div id="profilDisplay" class="contentDesc"> 
    <table>
      <tr>
        <td id="avatarTd">    
     <?php 
     class_code_user_avatar_img_display(bp_get_profile_field_data(array('field' => 'AvatarImg', 'user_id' => $user_id))); 
     ?>
        </td>
        <td id="hello">
          <span class="displayName"><?php echo $displayName  ; ?></span>	  <?php if ($user_id != wp_get_current_user()->ID) { 
    //echo '<a class="classCodeContactUserInProfile" href="mailto:'.get_user_by('ID', $user_id)->user_email.'?subject=À propos Class´Code"></a>';
    echo '<a class="classCodeContactUserInProfile" href="'.cc_mailto_shortcode(array("to" => get_user_by('ID', $user_id)->user_email, "who" => $Firstname." ".$Lastname, "subject" => "À propos de Class´Code")).'"></a>';
  } ?>
        <?php if($facilitateur){ ?>
          <div class="blueRectangle2">
            <div>Facilitateur ! </div>
          </div>
        <?php } ?>
        </td>
      </tr>
      <tr><td colspan="2">       
        <div class="blueRectangle1">
          <div>
          <?php echo $profil ;?>
          </div>
        </div>
      </td></tr>
      <tr><td colspan="2" >        
      <div id="profil_map_<?php echo $user_id;?>">
      <?php         
	      echo do_shortcode('[gmw_single_location item_type="member"  elements="map"  map_height="150px" map_width="100%" user_map_icon="0" item_id="'.$user_id.'"]');
      ?>
      </div>
      </td></tr>
      <tr><td colspan="2">
      <?php foreach($skillsTab as $skill){ 
          if(trim($skill) != ""){
      ?>
            <div class="greyRectangleSkills">
              <div>          
              <?php echo $skill ;?>
              </div>
            </div>  
      <?php 
          }
        } 
      ?>

      </td></tr>
    </table>

  </div>

<?php
 return ob_get_clean();
}

function customize_bp_user_profile(){
?>

<div id="profilHeader">
<?php
  global $wp;
  global $bp;
  if ( is_user_logged_in() ) {
    $current_url = home_url(add_query_arg(array(),$wp->request));
    $profil_url = bp_loggedin_user_domain()."profile/edit/group/1";
    
   
    if(bp_is_home() and  ($current_url == $profil_url )){  
      $argsFirstname = array(
      'field' => 'Prénom',
      'user_id' => bp_loggedin_user_id()
      );
      $Firstname = bp_get_profile_field_data($argsFirstname);
      $argsLastname = array(
        'field' => 'Nom',
        'user_id' => bp_loggedin_user_id()
      );
      $Lastname = bp_get_profile_field_data($argsLastname);
      $displayName = $Firstname." ".$Lastname;
      
      if($displayName == ' '){
        $displayName = wp_get_current_user()->display_name;   
      }
?>
      <div id="profilActions" class="contentMedia">
        <h2>Gerer votre profil</h2>
          <a href="#profilMain"><div id="profilAction1" class="whiteRectangle"><table><td><span class="bigNumber">1</span></td><td>Gérez<br/>vos donnees personnelles</td></table></div></a>
          <a href="#profilSecondary"><div id="profilAction2" class="whiteRectangle"><table><td><span class="bigNumber">2</span></td><td>Completez<br/> votre profil</td></table></div></a>
          <a href="#myLocation"><div id="profilAction3" class="whiteRectangle"><table><td><span class="bigNumber">3</span></td><td>Partagez<br/>votre localisation</td></table></div></a>
      </div>
      <div id="profilDisplay" class="contentDesc"> 
        <table>
          <tr>
            <td id="avatarTd">    
  <?php 
         class_code_user_avatar_img_display(bp_get_profile_field_data(array('field' => 'AvatarImg', 'user_id' => bp_loggedin_user_id()))); 
  ?>
            </td>
            <td id="hello">
              Bonjour<br>
              
              <span class="displayName"><?php echo $displayName  ; ?></span>
            </td>
          </tr>
          <tr><td colspan="2">       
            <div class="blueRectangle1">
  <?php
                $argsProfil = array(
                  'field' => 'Profil',
                  'user_id' => bp_loggedin_user_id()
                );
                $profil = bp_get_profile_field_data($argsProfil);
  ?>
              <div>
              <?php echo $profil ;?>
              </div>
            </div>
          </td></tr>
          <tr><td colspan="2">
            [gmw_single_location item_type="member"  elements="address,map"  map_height="150px" map_width="100%" user_map_icon="0"]
          </td></tr>
        </table>
      </div>
<?php
    }else if(bp_is_user()){
      echo display_classcode_profile(bp_displayed_user_id());
 
    }
?>
</div>

<div id="profilFooter">
  <?php if(bp_is_home() && ($current_url == $profil_url )){ ?>
    <div id="profilActionsFooter" class="contentMedia"></div>
  <?php }else if(bp_is_user()){ ?>
    <div class="profilActionsView" ></div>
  <?php } ?>
  <div id="profilSubmit" class="contentDesc">
    <?php 
   
    if($current_url == $profil_url ){
    ?>
      <a onclick="submitProfilForm();">Valider mon profil</a>
    <?php 
    }else{
    ?>
      <a href="<?php echo $profil_url;?>">Retourner à mon profil</a>
    <?php
    }
    ?>
  </div>
</div>
<script>

  function submitProfilForm(){
    
    window.onbeforeunload = null;
  //  document.getElementById('profile-edit-form').submit();
    var $myForm = document.getElementById('profile-edit-form');
    jQuery('<input type="submit">').hide().appendTo($myForm).click().remove();
    return false;
    
  }
  
</script>

<?php
  }else{
    echo "<div id='guidedTour'><h2>Vous devez être connecté pour accéder aux profils utilisateurs!</h2></div>";
  }
}  
add_action( 'bp_before_member_header', 'customize_bp_user_profile' );

?>