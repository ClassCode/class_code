<?php
include_once(plugin_dir_path( __FILE__ ).'../metabox/image_meta_box.php');
include_once(plugin_dir_path( __FILE__ ).'../metabox/bookmarks_meta_box.php');
/**
 * Defines an image rectangular selections for a meta box post.
 *
 * <b>Warning</b>: This meta-box can not be used in a front-end page, $post_type must be defined.
 *
 * @see clicarea_post_type
 *
 * \ingroup meta_box
 */
class image_selection_meta_box extends image_meta_box {
  /** Defines an image rectangular selections new meta-box.
   * Typical usage is:<pre>
   * new image_meta_box('name', array('title' => ../..));
   *</pre>
   * @param $name The field name.
   * @param $arguments An associative array of arguments with <a href="classbasic__meta__box.html#arguments">basic_meta_box</a> arguments.
   */
  public function __construct($name, $arguments) {
    parent::__construct($name, wp_parse_args($arguments, array(
							       'save_content_as_value_render' => true,
							       )));
    if ($arguments['post_type'] && is_admin()) {
      add_action('admin_enqueue_scripts', function() {
	  wp_enqueue_script('image_rectangle_selection', plugins_url()."/class_code/metabox/image_rectangle_selection.js");
	});
    }
    new bookmarks_meta_box($name.'_links', array('title' => 'Sélection dans l´image',
						 'post_type' => $this->arguments['post_type'],
						 'fields' => array("title", "area"),
						 'can-add' => false,
						 'can-sort' => false,
						 'can-export' => false,
						 'can-export-href' => false,
						 'can-import' => false,
						 'can-get' => false,
						 ));
  }
  function meta_box_render($value, $post_id) {    
    // Renders image_metabox
    parent::meta_box_render($value, $post_id);
    // Interfaces avec bookmarks_meta_box    
    if ($value && $value != '')
      echo '<script>
  new image_rectangle_selection("'.$this->name.'_div", "'.esc_attr($value).'", 800, 600, function(x1, y1, x2, y2) {
  '.$this->name.'_links_links.add_link("{ href: \"#\", title: \"éditer le lien\", area : {\"x1\":"+x1+", \"y1\":"+y1+", \"x2\":"+x2+", \"y2\":"+y2+"}}");
});
</script>';
  }
  function value_render($value, $post_id) {
    // Renders the image
    $html = '<img style="width:800px;height:600px" src="'.$value.'" usemap="#'.$this->name.'_map"/>';
    // Renders the image selection links if any
    {
      $links = get_post_meta($post_id, $this->name.'_links', true);
      $map = json_decode($links, true);
      $html .= '<map name="'.$this->name.'_map">';
      if ($map) 
	foreach($map as $area)
	  $html .= "  <area shape='rect' coords='".$area["x1"].",".$area["y1"].",".$area["x2"].",".$area["y2"]."' href='".$area["href"]."' alt='".$area["title"]."'/>\n";
      $html .= '</map>';
    }
    return $html;
  }
}
?>