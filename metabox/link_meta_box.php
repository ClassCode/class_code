<?php
include_once('basic_meta_box.php');
/**
 * Defines a link selection for a meta box post.
 * 
 * <b>Warning</b>: This meta-box can not be used in a front-end page, $post_type must be defined.
 *
 * <b>Warning</b>: Since it reuses the link chooser of the wordpress editor, it  must have been loaded (e.g., for comments or content edition) at the PHP level.
 *
 * @see link_chooser
 * 
 * \ingroup meta_box
 */
class link_meta_box extends basic_meta_box {
  /** Defines a link new meta-box.
   * Typical usage is:<pre>
   * new link_meta_box('name', array('title' => ../..));
   *</pre>
   * @param $name The field name.
   * @param $arguments An associative array of arguments with <a href="classbasic__meta__box.html#arguments">basic_meta_box</a> arguments.
   */
  public function __construct($name, $arguments) {
    parent::__construct($name, $arguments);
    if ($post_type && is_admin()) {
      add_action('admin_enqueue_scripts', function() {
	  wp_enqueue_script('link_chooser', plugins_url()."/class_code/metabox/link_chooser.js", 
			    array('jquery', 'jquery-ui-dialog'));
	});
    }
  }
  function meta_box_render($value, $post_id) {
    $icon_path = get_site_url().'/wp-content/plugins/class_code/basket/img';
    // Gets the stored or default value
    $link = json_decode($value, true);
    if (!$link) {
      $link['href'] = '#';
      $link['title'] = 'éditer le lien';
    }
    // Echoes the meta-box HTML code, with the related form field and JS link_chooser callback.
    echo '<div><a id="'.$this->name.'_link" href="'.$link['href'].'" target="_blank">'.$link['title'].'</a>
     <a href="#" title="Modifier le lien" onclick=\'new link_chooser(event, '.$this->name.'_links_update, "'.esc_attr($link['href']).'", "'.esc_attr($link['title']).'");return false;\'><img width="20" src="'.$icon_path.'/edit.png"/></a>
     <input type="hidden" id="'.$this->name.'_value" name="'.$this->name.'_value" value="'.esc_attr($value).'"/>
     <script>function '.$this->name.'_links_update(href, title) {
document.getElementById("'.$this->name.'_link").href = href;
document.getElementById("'.$this->name.'_link").innerHTML = title;
document.getElementById("'.$this->name.'_value").value = "{ \"href\":\"" + href + "\", \"title\":\"" + title + "\"}";
}</script>';
  }
  /** Returns an image of the QRcode associated to this link. 
   * - Uses the chart.googleapis.com Web service.
   * @param $value The current field value.
   * @param $post_id The current post ID.
   * @return A piece of HTML to display the value.
   */
  function value_render($value, $post_id) {
    return "<img src='https://chart.googleapis.com/chart?chs=400x400&cht=qr&chld=H|1&chl=$value' alt='$value'/>";
  }
}
?>