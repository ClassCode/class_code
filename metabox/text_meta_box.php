<?php
include_once('basic_meta_box.php');
/**
 * Defines a HTML text selection for a meta box post.
 *
 * #### Implementation details
 * @see https://codex.wordpress.org/Function_Reference/wp_editor
 *
 * \ingroup meta_box
 */
class text_meta_box extends basic_meta_box {
  /** Defines a text new meta-box.
   * Typical usage is:<pre>
   * new text_meta_box('name', array('title' => ../..));
   *</pre>
   * @param $name The field name.
   * @param $arguments An associative array of arguments with <a href="classbasic__meta__box.html#arguments">basic_meta_box</a> arguments and:
   * - <tt>textarea_rows</tt> The number of rows of the textarea. Default is 8.
   */
  public function __construct($name, $arguments) {
    parent::__construct($name, wp_parse_args($arguments, array(
							       'textarea_rows' => 8,
							       )));
  }
  function meta_box_render($value, $post_id) {
    wp_editor($value, $this->name.'_value', array('textarea_rows' => $this->arguments['textarea_rows']));
  }
  function value_render($value, $post_id) {
    return $value;
  }
  /** Renders the value as a wordpress content.
   * @param $postID the Post ID.
   * @param $name The field name.
   */
  public static function echo_value_as_content($postID, $name) {
    echo str_replace( ']]>', ']]&gt;', apply_filters('the_content', wpautop(get_post_meta($postID, $name, true))));
  }
}
?>