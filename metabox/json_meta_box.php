<?php
include_once('basic_meta_box.php');
/**
 * Defines a JSON structure edition for a meta box post.
 *
 * #### Implementation details
 * - Uses the ace JS editor
 * @see http://ace.c9.io/#nav=howto
 *
 * \ingroup meta_box
 */
class json_meta_box extends basic_meta_box {
  /** Defines a JSON structure new meta-box.
   * Typical usage is:<pre>
   * new json_meta_box('name', array('title' => ../..));
   *</pre>
   * @param $name The field name.
   * @param $arguments An associative array of arguments with <a href="classbasic__meta__box.html#arguments">basic_meta_box</a> arguments and:
   * - <tt>editor_width</tt> The width of the editor window. Default is 660.
   * - <tt>editor_height</tt> The height of the editor window. Default is 300.
   * - <tt>models</tt> An <tt>array(name => string, ...)</tt> of predefined JSON strings to help initializing the edition. Default is an empty array.
   */
  public function __construct($name, $arguments) {
    if ($arguments['post_type']) {
      if (is_admin())
	add_action('admin_enqueue_scripts', function() {
	    wp_enqueue_script('ace_editor', plugins_url()."/class_code/metabox/ace/ace.js");
	  });
    } else {
      if (!is_admin())
	echo "<script type='text/javascript' src='".plugins_url()."/class_code/metabox/ace/ace.js'></script>";
    }
    parent::__construct($name, wp_parse_args($arguments, array(
						       'editor_width' => 660,
						       'editor_height' => 300,
						       'models' => array(),
						       )));
  }
  function meta_box_render($value, $post_id) {
     $value = json_meta_box::json_prettify($value);

    // Displays the box HTML elements
    {
      echo '<div><p id="'.$this->name.'_menu">';
      if (count($this->arguments['models']) > 0)
	echo '<b>Initialiser à partir d´un modèle :</b>';
      foreach($this->arguments['models'] as $label => $string)
	echo '<a class="button" style="margin-left:20px" href="#" onClick="if (confirm(\'Remplacer le présent contenu par ce modèle ?\')) editor.setValue(\''.esc_js($string).'\'); return false;">'.$label.'</a>';
      echo '</p><div style="width:'.$this->arguments['editor_width'].'px;height:'.$this->arguments['editor_height'].'px" id="'.$this->name.'_pane">'.$value.'</div>';
      echo '<input type="hidden" id="'.$this->name.'_value" name="'.$this->name.'_value" value="'.esc_attr($value).'"/></div>';
    }

     // Integrates the javascript ace editor
     {
       $theme = 'monokai';
       $mode = 'json';
       $tab_size = 2;
       echo '<script>      
var editor = ace.edit("'.$this->name.'_pane");
editor.setTheme("ace/theme/'.$theme.'");
editor.getSession().setMode("ace/mode/'.$mode.'");
editor.getSession().setTabSize('.$tab_size.');
// The value is set directly in the div and get in the form hidden field at each change
editor.getSession().on("change", function(event) {
  document.getElementById(\''.$this->name.'_value\').value = editor.getValue();
});
</script>';
     }
  }
  /** Returns a syntax highlighted view of the JSON data structure. 
   * @param $value The current field value.
   * @param $post_id The current post ID.
   * @return A piece of HTML to display the value.
   */
  function value_render($value, $post_id) {
    $value = json_meta_box::json_prettify($value);
    $theme = 'monokai';
    $mode = 'json';
    $html = '<div style="width:600px;height:400px" id="'.$this->name.'_pane">'.esc_attr($value).'</div>
<script src="'.plugins_url().'/class_code/metabox/ace/ace.js" type="text/javascript" charset="utf-8"></script>
<script>      
var editor = ace.edit("'.$this->name.'_pane");
editor.setTheme("ace/theme/'.$theme.'");
editor.getSession().setMode("ace/mode/'.$mode.'");
editor.setReadOnly(true);
</script>';
    return $html;
  }
  /** Reformats the JSON string if the syntax is correct.
   * @param $value The JSON data structure as a string.
   * @return The JSON data structure as a beautified string.
   */
  public static function json_prettify($value) {
    return $value;
    // Cannot be used before 5.4.0 http://php.net/manual/fr/function.json-encode.php
    //$json = json_decode($value, true);
    //return $json ? json_encode($json, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK) : $value;
  } 
}
?>