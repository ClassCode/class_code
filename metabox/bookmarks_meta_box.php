<?php
include_once('basic_meta_box.php');
/**
 * Defines a link list selection for a meta-box post.
 *
 * - The bookmarks is a list link, defined as <tt>{"href": "../..", ../..}</tt>, where:
 *   - <tt>href</tt> is an uniform ressource identifier or location
 *   - <tt>../..</tt> stands for additionnal optional <tt>fields</tt> defined in the constructor.
 * 
 * - The Javascript function <tt>{$name}_add_link($link)</tt>, 
 * where <tt>$link</tt> is a JSON structure defining the link fields, 
 * is available to add a link from another meta-box or page section.
 *
 * <b>Warning</b>: Since it reuses in the back-office the link chooser of the wordpress editor when in back-office mode, it must have been loaded (e.g., for comments or content edition) at the PHP level.
 *
 * @see link_chooser
 * 
 * \ingroup meta_box
 */
class bookmarks_meta_box extends basic_meta_box {
  /** Defines a links list new meta-box.
   * Typical usage is:<pre>
   * new bookmarks_meta_box('name', array('title' => ../..));
   *</pre>
   * @param $name The field name.
   * @param $arguments An associative array of arguments with <a href="classbasic__meta__box.html#arguments">basic_meta_box</a> arguments and:
   * - <tt>fields</tt> An array of additional fields defining the link. Default is <tt>array("title")</tt>. Recognized fields are:
   *   - <tt>title</tt> An optional raw text (no HTML) title, provided by the user, or obtained from the URL.
   *   - <tt>icon</tt> An optional icon, provided by the user, or obtained from the URL.
   *   - <tt>area</tt> A rectangular clicable area, defined as a <tt>{"x1":../.., "y1":../.., "x2":../.., "y2":../..}</tt> array.
   * - <tt>can-add</tt> If true, the interface allows to add a new link. Default is true.
   * - <tt>can-delete</tt> If true, the interface allows to delete a new link. Default is true.
   * - <tt>can-sort</tt> If true, this is a user defined sorted sequence of links. If false, the set of links is unsorted. Default is true.
   * - <tt>can-export</tt> If true, the interface allows to export the link as a JSON data structure of the form <tt>{ href: ..}</tt> with all link fields. Default is true.
   * - <tt>can-export-href</tt> If true, the interface allows to export the link URL (without any another field). Default is true.
   * - <tt>can-import</tt> If true, the interface allows to import the link as a JSON data structure or the only the URL, depending on the string syntax. Default is true.
   * - <tt>can-get</tt> If true, allows in frontend pages the user, if (and only if) he/she is connected, to import a link via a Web service. Default is true.
   *   - The Web service URL writes:<pre>
   * http://...?...&bookmarks_meta_box_href=$href&bookmarks_meta_box_title=$title&bookmarks_meta_box_icon=$icon</pre>
   * where the URL <tt>href</tt> field is mandatory, while <tt>title</tt> and <tt>icon</tt> fields are optional.
   *     - Here <tt>http://...?...</tt> stands for the wordpress page URL.
   *   -It is possible to automatically add a link when visiting an external web page by adding a navigator bookmark, and clic on this bookmark when visiting a page, this is documented for the user.
   * - <tt>default</tt> If set, this string of the form <tt>[{href: ...}, ...]</tt> is used as default initial value.
   */
  public function __construct($name, $arguments) {
    $arguments = wp_parse_args($arguments, array(
						 'fields' => array("title"),
						 'can-add' => true,
						 'can-delete' => true,
						 'can-sort' => true,
						 'can-export' => true,
						 'can-export-href' => true,
						 'can-import' => true,
						 'can-get' => true,
						 ));
    if (function_exists('is_user_logged_in') && !is_user_logged_in()) {
      $arguments['can-add'] = $arguments['can-import'] = false;
      unset($arguments['default']);
    }
    parent::__construct($name, $arguments);
    if ($this->arguments['post_type'] && is_admin()) {
      add_action('admin_enqueue_scripts', function() {
	  wp_enqueue_script('link_chooser', plugins_url()."/class_code/metabox/link_chooser.js", 
			    array('jquery', 'jquery-ui-dialog'));
	});
    }
  }
  function meta_box_render($value, $post_id) {
    if ($value == "" && isset($this->arguments['default']))
      $value = "{ \"links\" : ".$this->arguments['default']."}";
    echo '<input type="hidden" id="'.$this->name.'_value"  name="'.$this->name.'_value" value="'.str_replace("\n", " ", esc_attr($value)).'"/>';
    echo '<div id="'.$this->name.'_popup" style="background-color:#EEEEEE;border:1px solid black;display:none;width:90%;height:auto;margin-left:auto;margin-right:auto;padding:10px;"></div>';
    echo '<div style="margin:20px;" id="'.$this->name.'_menu"></div>';
    $button_class = is_admin() ? "button" : "bookmarks_meta_box_button";
    echo '<script>
function bookmarks_meta_box() {
  var links = Array(); ; try { var value = JSON.parse(document.getElementById("'.$this->name.'_value").value); links = value.links; } catch(err) {}
  // Updates the display and the field value
  this.update = function(edit_index) {
    if (links == undefined) links = Array(); // This is an inunderstable patch 
    var html = "";
    html += "<style type=\"text/css\">a.bookmarks_meta_box_button{text-decoration:none;font-size:16px;padding:6px;background:#EEE;border:1px solid #AAA} a.bookmarks_meta_box_button:hover{background:#FFF;border:1px solid #333}</style>";
    html += "<table>";';
      if ($this->arguments['can-add'])
	echo '
    if (links.length == 0) html += "<tr><td><a onClick=\"'.$this->name.'_links.add(-1); return false;\" title=\"Ajouter un nouveau lien\" href=\"#\"><img width=\"25\" src=\"'.plugins_url().'/class_code/metabox/img/add.png\"/></a></td></tr>"; else ';
    echo'
    for(var i = 0; i < links.length; i++) 
      html += edit_index == i ? display_edit(i) : display(i);
    html += "</table>";
    document.getElementById("'.$this->name.'_menu").innerHTML = html;
    document.getElementById("'.$this->name.'_value").value = JSON.stringify({ links : links});
  }
  // Displays one link edition line
  function display(i) {
    var html = "<tr>";';
    // Displays the link fields
    if (in_array('icon', $this->arguments['fields'])) 
      echo '
    html += "<td>" + ((links[i].icon !== undefined && links[i].icon !== "") ? "<img src=\""+links[i].icon+"\" width=\"100\" height=\"80\"/>" : "") + "</td>";';
    echo '
    html += "<td>" + (links[i].href != undefined ? 
      "<a href=\""+links[i].href+"\" target=\"_blank\">"+
        (links[i].title != undefined ? links[i].title : links[i].href)+"</a>" :
      "") + "</td>";';
    if (in_array('area', $this->arguments['fields'])) 
      echo '
    if (links[i].area == undefined) links[i].area = { x1:0, y1:0, x2:0, y2:0 };
    html += "<td>("+links[i].area.x1+",</td><td>"+links[i].area.y1+")</td><td>("+links[i].area.x2+",</td><td>"+links[i].area.y2+")</td>";';
    // Displays the links buttons
    echo '
      html += "<td style=\"width:40px;\">:</td>";
      html += "<td><a onClick=\"'.$this->name.'_links.edit(event, "+i+"); return false;\" title=\"Éditer le lien\" href=\"#\"><img width=\"25\" src=\"'.plugins_url().'/class_code/metabox/img/edit.png\"/></a></td>";';
    if ($this->arguments['can-add'])
      echo '
      html += "<td><a onClick=\"'.$this->name.'_links.add("+i+"); return false;\" title=\"Ajouter un nouveau lien en dessous\" href=\"#\"><img width=\"25\" src=\"'.plugins_url().'/class_code/metabox/img/add.png\"/></a></td>";';
    if ($this->arguments['can-delete'])
      echo '
      html += "<td><a onClick=\"'.$this->name.'_links.del("+i+"); return false;\" title=\"Détruire le lien\" href=\"#\"><img width=\"25\" src=\"'.plugins_url().'/class_code/metabox/img/delete.png\"/></a></td>";';
     if ($this->arguments['can-sort'])
       echo '
      html += "<td>" + (i > 0 ? "<a onClick=\"'.$this->name.'_links.up("+i+"); return false;\" title=\"Déplacer vers le haut\" href=\"#\"><img width=\"25\" src=\"'.plugins_url().'/class_code/metabox/img/up.png\"/></a>" : "") + "</td>";
      html += "<td>" + (i < links.length - 1 ? "<a onClick=\"'.$this->name.'_links.down("+i+"); return false;\" title=\"Déplacer vers le bas\" href=\"#\"><img width=\"25\" src=\"'.plugins_url().'/class_code/metabox/img/down.png\"/></a>" : "") + "</td>";';
    if ($this->arguments['can-export'])
      echo '
      html += "<td><a onClick=\"'.$this->name.'_links.export("+i+", true); return false;\" title=\"Exporter le lien\" href=\"#\"><img width=\"25\" src=\"'.plugins_url().'/class_code/metabox/img/export.png\"/></a></td>";';
    if ($this->arguments['can-export-href'])
      echo '
      html += "<td><a onClick=\"'.$this->name.'_links.export("+i+", false); return false;\" title=\"Exporter l´URL\" href=\"#\"><img width=\"25\" src=\"'.plugins_url().'/class_code/metabox/img/export2.png\"/></a></td>";';
    echo '
   html += "</tr>";
   return html;
 }
 // Link edition callback';
    if (is_admin()) {
      echo '
 this.edit = function(event, i) { index = i; new link_chooser(event, edit_update, links[i].href, links[i].title); }
 function edit_update(href, title) { links[index].href = href; links[index].title = title; this.update(-1); }
 var index;';
    } else {
      echo '
 this.edit = function(event, i) { this.update(i); }
 function display_edit(i) { 
   var html = "<tr><td colspan=\"7\"><div style=\"background-color:#EEEEEE;border:black solid 1px;padding:20px\"><ul>";';
      if (in_array('title', $this->arguments['fields'])) 
	echo '
   html +=   "<li>Titre&nbsp;&nbsp;: <input type=\"text\" id=\"'.$this->name.'_edit_title\" size=\"48\" value=\""+links[i].title+"\"/></li>";';
      echo '
   html +=   "<li>Lien&nbsp;&nbsp;: <input type=\"text\" id=\"'.$this->name.'_edit_href\" size=\"48\" value=\""+links[i].href+"\"/></li>";';
      if (in_array('icon', $this->arguments['fields'])) 
	echo '   html +=   "<li>Icône&nbsp;: <input type=\"text\" id=\"'.$this->name.'_edit_icon\" size=\"48\" value=\""+links[i].icon+"\"/></li>";';
      echo '
   html += "</ul>";
   html += "<a style=\"float:left;\" class =\"bookmarks_meta_box_button\" onClick=\"'.$this->name.'_links.update(-1); return false;\" title=\"Annuler l´édition\" href=\"#\">Annuler l´édition</a>&nbsp;&nbsp;&nbsp;";
   html += "<a style=\"float:right;\" class =\"bookmarks_meta_box_button\" onClick=\"'.$this->name.'_links.edit_update("+i+"); return false;\" title=\"Valider l´édition\" href=\"#\">Valider l´édition</a>";
   html += "</div></td></tr>";
   return html;
 }
 this.edit_update = function(i) { 
   links[i].href = document.getElementById("'.$this->name.'_edit_href").value;';
      if (in_array('title', $this->arguments['fields'])) 
	echo '
   links[i].title = document.getElementById("'.$this->name.'_edit_title").value;';
      if (in_array('icon', $this->arguments['fields'])) 
	echo '
   links[i].icon = document.getElementById("'.$this->name.'_edit_icon").value;';
      echo '
   this.update(-1);
 }';
    }
    echo '
 // Adds a new link from external source, the link is a struct of the form { href : ../.. }
 this.add_link = function(link) { 
   if (typeof link == "string") {
     try { link = JSON.parse(link); } catch(err) { try { eval("link = " + link); } catch(err) { link = { href : link }; } }
   }
   links.splice(0, 0, link); this.update(-1); 
 };';
    if ($this->arguments['can-add'])
      echo '
 this.add = function(i) { links.splice(i+1, 0, { href : "#" '.
	(in_array('title', $this->arguments['fields']) ? ', title : "titre du lien"' : '').
	(in_array('icon', $this->arguments['fields']) ? ', icon : ""' : '').
	(in_array('area', $this->arguments['fields']) ? ', area : { x1:0, y1:0, x2:0, y2:0 }' :''). '}); this.update(-1); }';
    if ($this->arguments['can-delete'])
      echo '
 this.del = function(i) { if (window.confirm("Vous voulez vraiment détruire le lien ?")) links.splice(i, 1); this.update(-1); }'; 
    if ($this->arguments['can-sort'])
      echo '
 this.up = function(i) { var tmp = links[i-1]; links[i-1] = links[i]; links[i] = tmp; this.update(-1); }
 this.down = function(i) { var tmp = links[i+1]; links[i+1] = links[i]; links[i] = tmp; this.update(-1); }';
    if ($this->arguments['can-export'] || $this->arguments['can-export-href'])
      echo '
 this.export = function(i, json) { 
   this.popup("Votre lien: <div style=\"width:90%;margin-left:auto;margin-right:auto;margin-top:20px;\"><tt>"+(json ? JSON.stringify(links[i]) : links[i].href)+"</tt></div>");
 }'; 
    echo '
 // Displays a HTML message as inner-page popup
 this.popup = function(html) {
   document.getElementById("'.$this->name.'_popup").innerHTML = html + "<div style=\"text-align:right;\"><a class=\"'.$button_class.'\" href=\"#\" onClick=\"document.getElementById(\''.$this->name.'_popup\').style.display = \'none\'; return false;\">Fermer</a></div>";
   document.getElementById("'.$this->name.'_popup").style.display = "block";
 };
  this.update(-1);
};
var '.$this->name.'_links = new bookmarks_meta_box();';
     // Implements the frontend page user connected web service.
    $may_get = is_user_logged_in() && !is_admin();
    if ($this->arguments['can-get'] && isset($_REQUEST['bookmarks_meta_box_href'])) {
      $get_data = '{href:"'.$_REQUEST['bookmarks_meta_box_href'].'", title:"'.$_REQUEST['bookmarks_meta_box_title'].'", icon:"'.$_REQUEST['bookmarks_meta_box_icon'].'"}';
      if ($may_get) {
	if (!isset($_REQUEST['meta_box_action_todo']))
	  echo '
'.$this->name.'_links.add_link('.$get_data.');';
      } else {
	$get_help = '<div>Pour ajouter ce lien, il suffit de copier:<pre>'.$get_data.'</pre> dans la fenêtre d´import de l´interface des liens.</div>';
	echo $this->name.'_links.popup(\''.$get_help.'\');';
      }
    }
     echo '
</script>'; 
     if ($this->arguments['can-import'] || $this->arguments['can-get']) {
       echo '<div>';
       if ($this->arguments['can-import']) {
	 $edit_help = '<div>Pour ajouter directement un lien dans la fenêtre d´édition, il suffit d´y copier:<ul>
<li>Soit les méta-données du lien selon la syntaxe: <pre>{ href: "l´URL du lien", title : "le titre du lien" ...}</pre> où <tt>...</tt> peut correspondre à des champs supplémentaires comme <tt>icon</tt>.</li><br/>
<li>Soit le lien lui-même par exemple de syntaxe: <pre>http://classcode.fr</pre> et éditer ensuite ces méta-données dans l´interface.</li>
</ul></div>';
	 echo '<input onKeypress="return event.keyCode != 13;" type="text" id="'.$this->name.'_import" size="48"/> <a onClick="'.$this->name.'_links.popup(\''.esc_attr(str_replace("\n", " ", $edit_help)).'\'); return false;" title="Aide à l´édition" href="#"><img width="25" src="'.plugins_url().'/class_code/metabox/img/help.png"/></a> <a class="'.$button_class.'" href="#" onClick="'.$this->name.'_links.add_link(document.getElementById(\''.$this->name.'_import\').value); return false;">Importer</a>';
       }
       if ($this->arguments['can-get']) {
	 $page_link = get_permalink() != "" ? get_permalink() : (empty($_SERVER["HTTPS"]) ? "http://" : "https://").$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
	 $page_title = strtolower(get_the_title() != "" ? get_the_title() : $this->arguments['title']);
	 $import_link = 'javascript:location.href=\''.$page_link.'&bookmarks_meta_box_href=\'+encodeURIComponent(location.href)+\'&bookmarks_meta_box_title=\'+encodeURIComponent(document.title)+\'&bookmarks_meta_box_icon=\'+encodeURIComponent(document.images[0].src);';
	 $import_help = esc_js('
<div>Pour ajouter des liens à cette page lors de la visite d´un site il suffit d´avoir le lien ci-dessous en marque-page:</div>
<div align="center" style="height:40px;margin:40px;"><a class="bookmarklet_button" href="'.$import_link.'" onClick="return false;">Import de lien vers « '.esc_html($page_title).' »</a></div>
<ol>
 <li>Ouvrir le menu des marque-pages, si il n´est pas visible.</li>
 <li>Faire glisser l´élément ci-dessus dans le menu des marque-pages, pour l´y ajouter.</li>
 <li>Lors de la navigation sur une page, cliquer sur le lien du marque-page pour que cette page vienne s´ajouter.</li>
</ol>
<div><b>Note</b>: pour ajouter un lien, l´utilisateur doit être préalablement connecté.</div>');
	 $import_css = '
<style type="text/css">
  a.bookmarklet_button{text-decoration:none;cursor:move;font-size:16px;padding:6px;background:#F3F3F3;border:1px solid #CCC;-webkit-border-radius:9px;-moz-border-radius:9px;border-radius:9px}
  a.bookmarklet_button:hover{text-decoration:none;border:1px solid #333}
</style>';
	 echo $import_css.'<div style="float:right;"><a onClick="'.$this->name.'_links.popup(\''.$import_help.'\'); return false;" title="Ajouter un raccourci d´import" href="#"><img width="25" src="'.plugins_url().'/class_code/metabox/img/import.png"/></a></div>';
       }
       echo '</div>';
     }
  }
}
?>