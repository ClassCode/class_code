/** \class link_chooser 
 * Defines and opens the wordpress editor link chooser wrapper.
 * Typical usage:
 * <pre>&lt;a href="#" onClick="new link_chooser(event, update_callback, current_url, current_title); return false;">...</pre>
 *
 * <b>Warning</b>: Since it reuses the link chooser of the wordpress editor, it  must have been loaded (e.g., for comments or content edition) at the PHP level.
 *
 * \ingroup Javascript
 * \extends Javascript
 */
/** \fn link_chooser(event, update_callback, current_url, current_title)
 * @param event The onClick event at the origin of the link editor popup open.
 * @param update_callback A function of the form doingLink(href, title) called when the link is selected.
 * @param current_url The current link URL.
 * @param current_title The current link title.
 *
 * \memberof link_chooser
 */
function link_chooser(event, update_callback, current_url, current_title) {
  // The wpLink default values to be saved/restored
  var wplink_previous_default_value = false;
  // Prevents spurious events
  trap_events(event);

  // Opens the wp_editor link selector after initializing the related values
  if (typeof wpLink !== 'undefined') {
    wplink_previous_default_value = wpLink.setDefaultValues;
    wpLink.setDefaultValues = function () { 
      var $inputs = jQuery('#wp-link').find('input[type=text]');
      jQuery($inputs[0]).val(current_url);
      jQuery($inputs[1]).val(current_title);
    };
    wpLink.open();
  } else
    alert("L'éditeur de lien n'est pas accessible");
  
  // Binds the link save action to the wp_editor link selection
  jQuery('body').on('click', '#wp-link-submit', function(event) {
      trap_events(event);
      // Gets the wpLink href and title (but not the target)
      var linkAtts = wpLink.getAttrs();
      if (!('title' in linkAtts)) {
	linkAtts.title = jQuery("#wp-link-text").val();
      }
      // Updates and close
      update_callback(linkAtts.href, linkAtts.title);
      wpLink.close();
      wpLink.setDefaultValues = wplink_previous_default_value;
      return false;
    });
    
  // Traps any events to avoid spurious behavior
  function trap_events(event) {
    if (typeof event.preventDefault != 'undefined') {
      event.preventDefault();
    } else {
      event.returnValue = false;
    }
    event.stopPropagation();
  }
}  
