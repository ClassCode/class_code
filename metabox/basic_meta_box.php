<?php
/**
 * Defines a minimal post meta box.
 *
 * - The validation button is of the form <tt>&lt; input type="submit" class="button" ..</tt> 
 * and the connection link is of the form <tt>&lt; a class="button" ..</tt> allowing to customize the related CSS.
 *
 * #### Implementation details
 * @see https://codex.wordpress.org/Function_Reference/add_meta_box
 * @see https://codex.wordpress.org/Function_Reference/update_post_meta
 * @see https://codex.wordpress.org/Function_Reference/update_user_meta
 *
 * \ingroup meta_box
 */
class basic_meta_box
{
  /** Defines a generic new meta-box.
   * Typical usage tp use this meta-box is:<pre>
   * new basic_meta_box('name', array('title' => ../..));
   *</pre> and to derive a new meta-box:<pre>
   * class my_meta_box extends basic_meta_box {
   *   function __construct() {
   *     parent::__construct('name', 'title', 'post_type');
   *   }
   *   function meta_box_render($value, $post_id) {
   *     // Here implement the field edition HTML code echo.
   *   }
   * }
   *</pre>
   * @param $name The field name.
   * - The field name value is retrieved via : 
   *   - <tt>get_post_meta($post_id, $name, true)</tt> for a back-office post meta-box;
   *   - <tt>get_user_meta($user_id, $name, true)</tt> for a frontpage user meta-box.
   *   - while its value is defined by a form field of name <tt>${name}_value</tt>, 
   *     - e.g., of the form: <tt>&lt input type='text' name='${name}_value' .../></tt>
   *     - thus passed by a <tt>http://...&${name}_value=$value</tt> URL query.
   * @param $arguments An associative array of arguments with:
   *  \anchor arguments 
   * - <tt>title</tt> The field title. Default is ''.
   * - <tt>style</tt> The field frontend page <tt>&lt;div></tt> style. Default is a 90% centered bordered div.
   * - <tt>post_type</tt> The post type on which this meta box applies. Mandatory.
   *   - If false applies do not generate a back-office custom post type meta-box but a front-page user interaction box.
   * - <tt>priority</tt> The priority within the context where the boxes should show ('high', 'core', 'default' or 'low'), default is 'core'.
   * - <tt>unconnected_frontend_replacement_message</tt> The HTML text message to display instead of the connection message. If false nothing is displayed. To, e.g., ask for a connection you can use <p><tt>&lt;h3 style='float:left'>Pour répondre à cette question il faut se connecter&lt;/h3>&lt;a style='padding:0px 20px 0px 20px;min-height:0px;min-width:0px;margin:0px 10px 5px 0px;float:right;' class='button' href='".wp_login_url(get_permalink())."'>Je me connecte&lt;/a>&lt;div style='clear:both;'>&lt;/div></tt></p>
   * - <tt>save_frontend_value_history</tt> If set to a strictly positive number N, records the last N value updates history. Default is false.
   * - <tt>save_frontend_value_trials</tt> If set to a strictly positive number N, allows only N updates (after the field can not be changed). Default is false (no update limit).
   *   - @see meta_box_history for documentation and details.
   * - <tt>save_additionnal_suffixed_fields</tt> An array of suffixes (e.g., <tt>array('_more')</tt>), that corresponds to the fact that the meta-box has to save an additional field (e.g., of name <tt>{$name}_more</tt>), defined in a form field (e.g., of name <tt>{$name}_more_value</tt>). Default is an empty array.
   * - <tt>save_content_as_short_code</tt> If true saves the post content as a short code of the form <tt>[$post_type id='$post_id']</tt>. Default is false.
   * - <tt>save_content_as_value_render</tt> If true saves the post content using the <tt>value_render()</tt> method. Default is false.
   */
  public function __construct($name, $arguments = array()) {
    // The field name is sanitized 
    $this->name = preg_replace('/[^a-z0-9\-]/', '_', strtolower($name));
    // Manage spurious post_type definition
    if (!isset($arguments['post_type'])) {
      debug_print_backtrace();
      trigger_error("Error: a ".get_class($this)." with a undefined post_type is in use, this is class_code middleware use bug", E_USER_ERROR);
    }
    // Default arguments
    $this->arguments = wp_parse_args($arguments, array(
					     'title' => '',
					     'style' => '',
					     'priority' => 'core',
					     'unconnected_frontend_replacement_message' => false,
					     'save_frontend_value_history' => false,
					     'save_frontend_value_trials' => false,
					     'save_additionnal_suffixed_fields' => array(),
					     'save_content_as_short_code' => false,
					     'save_content_as_value_render' => false,
					    ));
    // [A] Back-office implementation
    if ($this->arguments['post_type']) {
      // Adds a meta-box in the edition panel
      if (is_admin()) {
	add_action('add_meta_boxes', array($this, 'add_meta_boxes'));
	// .. with the save action
	if(get_class($this) != 'help_meta_box')
	  add_action('save_post', array($this, 'save_post'));
      }
    // [B] Front-end page implementation
    } else {
      // Echoes a meta-box in the front page
      if (!is_admin() && (is_user_logged_in() || $this->arguments['unconnected_frontend_replacement_message'])) {
	// Renders a standard meta-box in the front page
	echo "<div class='meta_box' style='".$this->arguments['style']."' id='".$name."_div'>\n";
	// ... with title if any,
	if ($this->arguments['title'] != "")
	  echo "<h4>".$this->arguments['title']."</h4>";
	// ... with a simple echeos if a passive help meta-box.
	if(get_class($this) == 'help_meta_box') {
	  $this->meta_box_render("", get_the_ID());
	} else if (is_user_logged_in()) {
	  // ... with a form otherwise.
	  echo "<form action='#".$name."_div' method='post' style='width:100%;padding:0px;border:0px'>";
	  // Protects the save operation
	  wp_nonce_field($this->name.'_id_nonce', $this->name.'_data_nonce');
	  // Detects if a user is connected
	  $user_id = get_current_user_id();
	  // Defines the initial value from the form or data-base previous value
	  $value = 
	    isset($_REQUEST[$name.'_value']) ? preg_replace('/\\\\"/', '"', $_REQUEST[$name.'_value']) : get_user_meta($user_id, $name, true);
	  $_REQUEST[$name.'_value'] = $value;
	  // Updates the value if the user is connected
	  if (isset($_REQUEST[$name.'_value']) && 
	      isset($_POST[$this->name.'_data_nonce']) && wp_verify_nonce($_POST[$this->name.'_data_nonce'], $this->name.'_id_nonce')) {
	    update_user_meta($user_id, $name, $value);
	    // Manages the history if required
	    if (intval($this->arguments['save_frontend_value_history']) > 0) {
	      $history = json_decode(get_user_meta($user_id, $name.'_history', true), true);
	      if (!$history) $history = array();
	      $history[time()] = $value;
	      $history = array_slice($history, 0, $this->arguments['save_frontend_value_history']);
	      krsort($history);
	      update_user_meta($user_id, $name.'_history', 
			       json_encode($history, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK));
	    }
	    // Updates additional suffixed field values
	    foreach ($this->arguments['save_additionnal_suffixed_fields'] as $suffix)
	      if (isset($_POST[$this->name.$suffix.'_value']))
		update_user_meta($user_id, $this->name.$suffix,
				 preg_replace('/\\\\"/', '"', $_POST[$this->name.$suffix.'_value']));
	  }
	  // Renders the meta-box form content
	  $this->meta_box_render($value, get_the_ID());
	  // Renders the validation button
	  {
	    echo "<br/>";
	    // Manages the number of trials if appropriate
	    if (intval($this->arguments['save_frontend_value_trials']) > 0) {
	      $count = 1 + get_user_meta($user_id, $name.'_count', true); 
	      update_user_meta($user_id, $name.'_count', $count);
	      if ($count <= $this->arguments['save_frontend_value_trials'])
		echo "<div align='right'><input class='button' type='submit' name='meta_box_action_todo' value='Valider ($count essais sur ".$this->arguments['save_frontend_value_trials'].")'/></div>";
	      else
		echo "<div align='right'><input class='button' type='reset' name='meta_box_action_todo' value='Validation bloquée (".$this->arguments['save_frontend_value_trials']." essais utilisés)'/></div>";
	    } else 
	      echo "<div style='margin:0px;' align='right'><input class='button' type='submit' name='meta_box_action_todo' value='Valider'/></div>";
	  }
	  echo "</form>";
	} else if ($this->arguments['unconnected_frontend_replacement_message']) {
	  echo '<p>'.$this->arguments['unconnected_frontend_replacement_message'].'</p>';
	}
	echo "</div>\n";
      }
    }
  }
  /** Implements the add_meta_boxes hook. \private */
  function add_meta_boxes () {
    add_meta_box($this->name, $this->arguments['title'], array($this, 'add_meta_boxes_function'), $this->arguments['post_type'], 'normal', $this->arguments['priority'], null);
  }  
  /** Implements the add_meta_boxes hook function. \private */
  function add_meta_boxes_function ($post) {
    // Protects the save operation
    wp_nonce_field($this->name.'_id_nonce', $this->name.'_data_nonce');
    // Echoes the HTML meta-box render, using a predefined value
    $this->meta_box_render(get_post_meta($post->ID, $this->name, true), $post->ID);
  }
  /** Implements the save_meta_box hook. \private */
  function save_post($post_id) {
    // Checks that the save action is save and valid
    if (isset($_POST[$this->name.'_data_nonce']) && wp_verify_nonce($_POST[$this->name.'_data_nonce'], $this->name.'_id_nonce') &&
	(!(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)) && 
	isset($_POST[$this->name.'_value'])) {
      // Updates the value
      $value = $_POST[$this->name.'_value']; // with sanitize_text_field() ?
      update_post_meta($post_id, $this->name, $value);
      // Saves additional fields
      foreach ($this->arguments['save_additionnal_suffixed_fields'] as $suffix)
	if (isset($_POST[$this->name.$suffix.'_value']))
	  update_post_meta($post_id, $this->name.$suffix, 
			   preg_replace('/\\\\"/', '"', $_POST[$this->name.$suffix.'_value']));
      // Saves the post content using predefined option
      if ($this->arguments['save_content_as_short_code'] || $this->arguments['save_content_as_value_render']) {
	// This mandatory hook supression avoids infinite loops
	remove_action('save_post', array($this, 'save_post'));
	// Defines the content depending on the option
	{
	  if ($this->arguments['save_content_as_short_code'])
	    $post_content = "[".$this->arguments['post_type']." id='$post_id']";
	  if ($this->arguments['save_content_as_value_render'])
	    $post_content = $this->value_render($value, $post_id);
	}
	// Updates the post content
	wp_update_post(array(
			     'ID'           => $post_id,
			     'post_content' => $post_content));
	// Restores this hook
	add_action('save_post', array($this, 'save_post'));
      }
    }
  }
  /** Implements the field edition HTML code echo.
   * - The field edition value must be stored in a input form field with attributes
   *  <tt>name="'.$this->name.'_value" value="'.esc_attr($value).'"</tt>
   * @param $value The current field value.
   * @param $post_id The post ID 
   * - The post_this this meta-box field belongs to, if used in back-office edition
   * - The user_id this meta-box is presented to, if used in a front-page, 0 undefined.
   */
  function meta_box_render($value, $post_id) {
    echo '<div><b>Valeur:</b> <input type="text" name="'.$this->name.'_value" value="'.esc_attr($value).'" size="32"/></div>';
  }
  /** Returns a piece of HTML to display the value.
   * @param $value The current field value.
   * @param $post_id The current post ID.
   * @return A piece of HTML to display the value.
   */
  function value_render($value, $post_id) {
    return "<pre>$value</pre>";
  }
  /** Returns true if the edition of the field is allowed and false otherwise.
   * @param $name The field name.
   */
  public static function can_edit($name) {
    return is_user_logged_in() && 
      isset($_POST[$name.'_data_nonce']) && wp_verify_nonce($_POST[$name.'_data_nonce'], $name.'_id_nonce');
  }
}
?>