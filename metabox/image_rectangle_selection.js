/** \class image_rectangle_selection
 * Defines, within a div HTML element, an image with rectangle selection.
 * Typical usage:
 * <pre>new image_rectangle_selection(id, image_src, width, height);</pre>
 *
 * <b>Warning</b>: Their is a caveat when the user clics more that two times during the selection.
 *
 * \ingroup Javascript
 * \extends Javascript
 */
/**
 * \fn image_rectangle_selection(id, image_src, width, height, update_callback)
 * @param id The div HTML ID element which is going to contain the img HTML element with the selection mechanism.
 * @param image_src The image src, i.e., the image file location.
 * @param width The selection width.
 * @param height The selection height.
 * @param update_callback A function of the form <tt>update(x1, y1, x2, y2)</tt> called when the selection is done.
 * 
 * 
 * \memberof image_rectangle_selection
 */
function image_rectangle_selection(id, image_src, width, height, update_callback) {
  // Defines the DOM elements and attaches their callbacks.
  {
    document.getElementById(id).style = "width:"+width+"px;height:"+height+"px;position:relative";
    document.getElementById(id).innerHTML =
      "<img id=\""+id+"_img\" src=\""+image_src+"\"/>" +
      "<div id=\""+id+"_rect\"></div>";
    var img = document.getElementById(id+"_img");
    var rect = document.getElementById(id+"_rect");
    img.style = "width:"+width+"px;height:"+height+"px;position:absolute;top:0;left:0;border:solid blue 1px;";
    rect.style = "position:absolute;top:0px;left:0px;height:0px;width:0px;border:solid yellow 1px;";
    img.onmousedown = onClick;
    rect.onmousedown = onClick;
    img.onmousemove = onMoveImg;
    rect.onmousedown = onMoveRect;
  }

  // Current mouse position
  var x, y;
  // Current selection position
  var x1, y1, x2, y2;
  // Current selection interaction state
  var state;
  
  // Clears the selection
  function clear() {
    x1 = "";
    y1 = "";
    x2 = "";
    y2 = "";
    state = "";
    trace(false);
  }
  clear();
  
  // Gets the mouse coordinates
  function mouse(evt) {
    if (typeof evt.offsetX != "undefined") {
      x = evt.offsetX;
      y = evt.offsetY;
    } else if (typeof evt.layerX != "undefined") {
      x = evt.layerX;
      y = evt.layerY;
    }
  }

  // Updates the current trace
  function trace(trace) {
    if(trace && x1 != "" && y1 != "" && x2 != "" && y2 != "") {
      rect.style.left = Math.min(x1, x2)+ "px";
      rect.style.top = Math.min(y1, y2)+ "px";
      rect.style.width = (Math.abs(x2 - x1) - 1) + "px";
      rect.style.height = (Math.abs(y2 - y1) - 1) + "px"; 
    } else {
      rect.style.left = 0;
      rect.style.top = 0;
      rect.style.width = 0;
      rect.style.height = 0;
    }
  }
  
  // Manages the start/stop selection clic
  function onClick(evt) {
    // Prevents from drag and drop
    if (evt.preventDefault)
      evt.preventDefault();
    evt.returnValue = false;
    // Saves the 1st selection corner
    if (state != "start") {
      state = "start";
      mouse(evt);
      x1 = x;
      y1 = y;
    } else {
      // Runs the callback on selection
      update_callback(x1, y1, x2, y2);
      clear();
      state = "stop";
    }
  }

  // Manages the selection drag 
  function onMoveImg(evt) {
    if (state == "start") {
      mouse(evt);
      x2 = x;
      y2 = y;
      trace(true);
    }
  }
  // When on rectangle avoid selection jitter
  function onMoveRect(evt) { 
    if (state == "start") {
      mouse(evt);
      x2 = Math.max(x-2,x1) + Math.min(x+2,x1)-x1;
      y2 = Math.max(y-2,y1) + Math.min(y+2,y1)-y1;
      trace(false);
    }
  }
}
