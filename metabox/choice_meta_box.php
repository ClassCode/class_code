<?php
include_once('basic_meta_box.php');
/**
 * Defines a choice selection for a meta box post.
 *
 * - The interface is defined by a data-structure of the form <pre>{
 * "choices" : [ "Oui", "Non", "Ne sait pas" ],
 * "multiple" : false,
 * "text" : 40
 *}</pre>, as detailled below.
 *
 * \ingroup meta_box
 */
class choice_meta_box extends basic_meta_box {
  /** Defines a choice new meta-box.
   * @param $name The field name.
   * @param $arguments An associative array of arguments with <a href="classbasic__meta__box.html#arguments">basic_meta_box</a> arguments and:
   * - <tt>choices</tt>  An optional list of possible answers, a JSON array of the form <tt>[ "item_1", "item_2", ...]</tt>. Default is <tt>[]</tt>.
   * - <tt>multiple</tt> If true, multiple choices can be made. Default is <tt>false</tt>.
   *   - A multiple answer is stored in a semi-colum separated list (with no space) of the form <tt>answer_1;answer_2;...</tt>.
   *   - As a consquence answers, if multiple, can NOT contains a semi-column ';'.
   * - <tt>text</tt>  If not <tt>false</tt> 
   *   - but a number of chars (of the text area width), the answer can be input as one-line text.
   *   - but a number of columns and lines separated by a «,», the answer is completed by a commentary.
   *  Default is <tt>false</tt>.
   *   - Multiple answers are input as a semi-colum separated list.
   * - <tt>unconnected_frontend_replacement_message</tt> If equal to 'true' generates this message, from the choices, as a fixed item list.
   */
  public function __construct($name, $arguments) {
    if ($arguments['unconnected_frontend_replacement_message'] == true && is_array($arguments['choices'])) {
      $arguments['unconnected_frontend_replacement_message'] = '<ul>';
      foreach($arguments['choices'] as $choice) 
	$arguments['unconnected_frontend_replacement_message'] .= '<li>'.$choice.'</li>';
      $arguments['unconnected_frontend_replacement_message'] .= '</ul>';
    }
    parent::__construct($name, wp_parse_args($arguments, array(
							       'choices' => array(),
							       'multiple' => false,
							       'text' => false,
							       )));
  }
  function meta_box_render($value, $post_id) {
    // Defines the field HTML name
    $name = $this->name.'_value';
    // Gets the answers (mutiple choices)
    $answers = explode(";", preg_replace("/\\\\'/", "'", $value));
    // Renders the HTML code
    $html = "<div id='".$name."_div'>\n";
    $html .= "  <script>var ".$name."_update_count = 0;</script>\n";
    if (count($this->arguments['choices']) > 0) {
      if ($this->arguments['multiple']) {
	// Implements multiple choices with checkbox-button
	$nn = 0;
	foreach($this->arguments['choices'] as $choice) {
	  $nn++;
	  $html .= "  <input type='checkbox' id='$name-$nn' name='$name-$nn' value='".esc_attr($choice)."' ".(in_array($choice, $answers) ? " checked='true'" : "")." onChange='".$name."_update(false);'/>$choice<br/>\n";
	}
	$html .= "  <script>".$name."_update_count = $nn;</script>\n";
      } else {
	// Implements unique choice with radio-button
	foreach($this->arguments['choices'] as $choice) {
	  $html .= "  <input type='radio' name='$name-0' value='".esc_attr($choice)."' ".(in_array($choice, $answers) ? " checked='true'" : "")." onChange='".$name."_update(false);'/>$choice<br/>\n";
	}
      }
      foreach($this->arguments['choices'] as $choice)
	while(in_array($choice, $answers))
	  unset($answers[array_search($choice, $answers)]);
    }
    // Implements the value open field if any
    if ($this->arguments['text']) {
      $textvalue = implode(";", $answers);
      if (strpos($this->arguments['text'], ","))
	$html .= " <br/><label style='vertical-align:top;' for='$name-x'>Commentaire:</label> <textarea id='$name-x' name='$name-x' cols='".preg_replace('/\s*([0-9])\s*,.*/', "$1", $this->arguments['text'])."' rows='".preg_replace('/[^,]*,\s*([0-9])\s*/', "$1", $this->arguments['text'])."' onChange='".$name."_update(true);'>".$textvalue."</textarea>\n";
      else 
	$html .= " <label for='$name-x'>Autre:</label> <input type='text' id='$name-x' name='$name-x' size='".$this->arguments['text']."' value='".esc_attr($textvalue)."' onChange='".$name."_update(true);' onKeypress='return event.keyCode != 13;'/>\n";
    }
    // Implements the hidden metabox field 
    $html .= "  <input type='hidden' id='$name' name='$name'/>\n";
    // Implements the update function when a value is changed
    $html .= '
<script>
function '.$name.'_update(fromText) {
  var answer = "";
'.($this->arguments['multiple'] ? '
  // Multiple choice update
  for(var n = 1; n <= '.$name.'_update_count; n++)
    if (document.getElementById("'.$name.'-"+n).checked)
      answer += (answer == "" ? "" : ";") + document.getElementById("'.$name.'-"+n).value;
'.($this->arguments['text'] ? '
  if (document.getElementById("'.$name.'-x").value != "")
    answer += (answer == "" ? "" : ";") + document.getElementById("'.$name.'-x").value
' :'') : '
  // Exclusive choice update
  if(fromText) {
    answer = document.getElementById("'.$name.'-x").value;
    var answers = document.getElementsByName("'.$name.'-0");
    for(var n = 0; n < answers.length; n++)
      answers[n].checked = false;
  } else {
    var answers = document.getElementsByName("'.$name.'-0");
    for(var n = 0; n < answers.length; n++)
      if (answers[n].checked) 
        answer = answers[n].value;
    if (document.getElementById("'.$name.'-x") != undefined)
      document.getElementById("'.$name.'-x").value = answer;
  }
').'
  document.getElementById("'.$name.'").value = answer;
}
'.$name.'_update('.(($this->arguments['text'] && count($answers) > 0) ? 'true' : 'false').');
</script>';
    $html .= "</div>";
    echo $html;
  }
}
?>