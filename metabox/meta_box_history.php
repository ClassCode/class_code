<?php
/**
 * This factory defines utilities regarding the meta-box value history.
 *
 * - Any page frontend meta-box may define the <a href="classbasic__meta__box.html#arguments">basic_meta_box</a> <tt>save_frontend_value_history</tt> argument.
 *   - If true records not only the last value update but all value updates.
 *   - The history is recorded as an array of the form <tt>array("time" => "value", ..)</tt>.
 *   - The <tt>time</tt> format is the current time, measured in second since <tt>1970, January the 1st, at 00:00</tt>; 
 *   i.e., as provided by the PHP <tt><a href="http://php.net/manual/en/function.time.php">time()</a></tt>.
 *   - The history is retrieved as a JSON string in the using <tt>get_user_meta($user_id, $name.'_history', true)</tt>.
 *   - This option only applies for frontend meta-box (the wordpress revision mechanism is used in the backoffice).
 *   - This option only applies for the field value, not additional suffixed fields.
 *
 * \ingroup meta_box
 * \extends Utility
 */
class meta_box_history
{
  /** Clears or garbles the history before a given time.
   * @param $name The field name (without the <tt>_history</tt> suffix).
   * @param $user_id The user ID for which history has to be cleared. If false, clears of equal to 0, clears for all users. Default is false.
   * @param $time The history is cleared before this time (measured in second since <tt>1970, January the 1st, at 00:00</tt>), included. Default is now.
   * @return The history value avec being cleared or garbled.
   */
  static function clear_history($name, $user_id = false, $time = false) {
    if (!$time)
      $time = time();
    if ($user_id && $user_id != 0) {
      $history = json_decode(get_user_meta($user_id, $name.'_history', true), true);
      if ($history)
	foreach($history as $date => $value) 
	  if ($date <= $time)
	    unset($history[$date]);
      update_user_meta($user_id, $name.'_history', 
		       json_encode($history, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK));
    } else {
      // All users implementation 
      $users = get_users(array(
			       'meta_key' => $name."_history",
			       ));
      foreach($users as $user)
	meta_box_history::clear_history($name, $user->ID, $time);
    }
  }
  /** Returns a count of history values.
   * @param $name The field name (without the <tt>_history</tt> suffix).
   * @param $user_id The user ID for which history has to be cleared.
   * @param $format The output $format. Default is array.
   *   - <tt>array</tt> A PHP array.
   *   - <tt>JSON</tt>  A JSON string.
   *   - <tt>HTML</tt>  A HTML table.
   * @return The result in the required format.
   */
  static function get_history_count($name, $user_id, $format = "array") {
    $result = array();
    $history = json_decode(get_user_meta($user_id, $name.'_history', true), true);
    if ($history)
      foreach($history as $date => $value) 
	if ($value != '')
	  $result[$value] = isset($result[$value]) ? $result[$value] + 1 : 1;
    switch($format) {
    case 'HTML' :
      $html = "<table style='margin-left:auto;margin-right:auto;'>";
      foreach($result as $value => $count)
	$html .= "  <tr><td>$value</td><td align='center'>$count</td></tr>";
      $html .= "</table>";
      return $html;
    case 'JSON' :
      return json_encode($format, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK);
    default :
      return $result;
    }
  }
}
?>