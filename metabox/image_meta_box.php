<?php
include_once('basic_meta_box.php');
/**
 * Defines an image selector for a meta box post.
 *
 * <b>Warning</b>: This meta-box can not be used in a front-end page, $post_type must be defined.
 *
 * \ingroup meta_box
 */
class image_meta_box extends basic_meta_box {
  /** Defines an image new meta-box.
   * Typical usage is:<pre>
   * new image_meta_box('name', array('title' => ../..)):
   *</pre>
   * @param $name The field name.
   * @param $arguments An associative array of arguments with <a href="classbasic__meta__box.html#arguments">basic_meta_box</a> arguments.
   */
  public function __construct($name, $arguments) {
    parent::__construct($name, $arguments);
    // This is required to use the media panel JS API
    if (is_admin ())
      add_action('admin_enqueue_scripts', function () { wp_enqueue_media();  });
  }
  function meta_box_render($value, $post_id) {
    // Wordpress dialog to select an image
    // Ref: http://codex.wordpress.org/Javascript_Reference/wp.media
    echo '
<p align="right"><a id="'.$this->name.'_media_input" onClick="mb_wp_media_input();" href="#" class="button">Choisir une image</a></p>
<div id="'.$this->name.'_div"><img id="'.$this->name.'_div_img" src="'.esc_attr($value).'"/></div>
<p>URL: <input type="text" id="'.$this->name.'_value" name="'.$this->name.'_value" value="'.esc_attr($value).'" size="64"/></p>
<script language="javascript"> 
var mb_wp_media_frame = false;
// JS call back to open the dialog
function mb_wp_media_input() {
  if (!mb_wp_media_frame) { 
    mb_wp_media_frame = wp.media({
      title: "Sélecteur d´image",
      button: {
        text: "Choisir une image"
      },
      multiple: false
    });
    mb_wp_media_frame.on("select", function() {
      var result = mb_wp_media_frame.state().get("selection").first().toJSON();
      document.getElementById("'.$this->name.'_div_img").src = result.url;
      document.getElementById("'.$this->name.'_value").value = result.url;
    });
  }
  mb_wp_media_frame.open();
  return false;
}
</script>';
  }  
  function value_render($value, $post_id) {
    return "<img alt='".preg_replace("/\\.[a-zA-Z0-9]*$/", "", basename($value))." src='$value'/>";
  }
}
?>