<?php
include_once('basic_meta_box.php');
/**
 * Defines a non-editable meta-box that simply displays information.
 *
 * \ingroup meta_box
 */
class help_meta_box extends basic_meta_box {
  /** Defines a non-editable meta-box.
   * Typical usage is:<pre>
   * new help_meta_box('name', array('title' => ../.. , 'meta_box_render' => function($value, $post_id) {
   *     // Here echoes the HTML help text.
   * }));
   *</pre>
   * @param $name The field name.
   * @param $arguments An associative array of arguments with <a href="classbasic__meta__box.html#arguments">basic_meta_box</a> arguments and:
   * - <tt>meta_box_render</tt> A callback function of the form <tt>meta_box_render($value, $post_id)</tt> which echoes the HTML help text.
   */
  public function __construct($name, $arguments) {
    parent::__construct($name, wp_parse_args($arguments, array(
							       'meta_box_render' => function($value, $post_id) { },
							       )));
  }
  function meta_box_render($value, $post_id) {
    call_user_func($this->arguments['meta_box_render'], $value, $post_id);
  }
}
?>