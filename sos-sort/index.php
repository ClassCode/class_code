<?php 
  define('WP_USE_THEMES', false); 
  require_once("../../../../wp-load.php");

  $classcodeUrl='';
  if(empty($_SERVER["HTTPS"])){
    $classcodeUrl='http://';
  }else{
    $classcodeUrl='https://';
  }
  $classcodeUrl.=$_SERVER["SERVER_NAME"];
  
// https://pixees.fr/wp-content/plugins/class_code/sos-sort/index.php
// https://pixees.fr/wp-admin/edit.php?category_name=a-la-carte
// https://sig-bd.inria.fr

// Defines the category order given the modules choice 
function get_cat_order($categories) {
  $cats_order = array(
    "iai-formation-citoyenne-a-lintelligence-artificielle-intelligente" => 1,
    "module-1" => 5, 
    "module-2" => 6, 
    "module-3" => 7, 
    "module-4" => 8, 
    "module-5" => 9, 
    "1-2-3-codez" => 10, 
    "1-2-3-codez-tome-1" => 11, 
    "1-2-3-codez-tome-2" => 12, 
    "activite-multi-disciplinaire" => 13,
    "presentation-du-cours" => 19, 
    "t-thematiques-en-sciences-du-numerique" => 20, 
    "i-l-informatique-et-ses-fondements" => 21, 
    "n-le-numerique-et-ses-sciences-dans-le-reel" => 22, 
    "c-creer-des-projets-pour-l-icn" => 23,
    "parcours-mgistere" => 24,
    "informatics-and-digital-creation" =>25,
    "n-informatics-and-digital-creation" =>26,
    "i-informatics-and-digital-creation" =>27,
    "c-informatics-and-digital-creation" =>28,
    "iai-modules-avec-blocs-de-liens" => 29);
  $cats_cycle_order = array(
    "cycle-1" => 11, 
    "cycle-2" => 12, 
    "cycle-3" => 13, 
    "cycle-4" => 14);
  $p_order = 0;
  foreach($cats_order as $cat => $c_order) 
    if (in_array($cat, $categories) && $c_order > $p_order)
      $p_order = $c_order;
  return $p_order == 0 ? 99 : $p_order;
}

$all_categories = array('iai-formation-citoyenne-a-lintelligence-artificielle-intelligente', 'module-1', 'module-2', 'module-3', 'module-4', 'module-5', '1-2-3-codez', '1-2-3-codez-tome-1', '1-2-3-codez-tome-2', 'activite-multi-disciplinaire', 'cycle-1', 'cycle-2', 'cycle-3', 'cycle-4', 'presentation-du-cours', 't-thematiques-en-sciences-du-numerique', 'n-le-numerique-et-ses-sciences-dans-le-reel', 'i-l-informatique-et-ses-fondements', 'c-creer-des-projets-pour-l-icn', 'parcours-mgistere', 'texte', 'video', 'livre', 'activite', 'fiche-d-activite', 'activite-debranchee', 'activite-branchee', 'cours-en-ligne', 'scratch', 'thymio', 'vpl', 'vpl-avance', 'arduino', 'blocky', 'intelligence-artificielle', 'prendre-du-recul', 'histoire-de-linformatique', 'pedagogie', 'ressources-et-supports-scolaires', 'educateurs', 'professeurs-des-ecoles', 'professeurs-du-secondaire', 'parents', 'initiation-algorithme', 'initiation-a-la-programmation', 'representation-information', 'informagique', 'primaire', 'college', 'lycee', 'superieur', 'informatics-and-digital-creation', 'n-informatics-and-digital-creation', 'i-informatics-and-digital-creation', 'c-informatics-and-digital-creation', 'modules', 'iai-modules-avec-blocs-de-liens');
$bad_categories = array('a-la-carte', 'mooc-icn-informatique-et-creation-numerique', 'cycles-scolaires', 'epi-enseignement-pratique-et-interdisciplinaire', 'les-parcours', 'ressources-a-la-une', '2014-10-octobre', 'bonne-feuille', 'a-decouvrir', 'plateforme', 'septembre-2014');

// Gets all a-la-carte posts and builds the related meta-data
$posts = array();
foreach(get_posts(array(
	'posts_per_page'   => -1,
	'offset'           => 0,
	'category'         => '',
	'category_name'    => 'a-la-carte',
	'orderby'          => 'menu_order',
	'order'            => 'ASC',
	'include'          => '',
	'exclude'          => '',
	'meta_key'         => '',
	'meta_value'       => '',
	'post_type'        => 'post',
	'post_mime_type'   => '',
	'post_parent'      => '',
	'author'	   => '',
	'author_name'	   => '',
	'post_status'      => 'publish',
	'suppress_filters' => true)) as $post) {
  $categories = array_map(function($cat) { return $cat->slug; }, get_the_category($post->ID));
  $posts[$post->ID] = array(
    'ID' => $post->ID, 
    'title' => $post->post_title, 
    'menu_order' => $post->menu_order, 
    'cat_order' => get_cat_order($categories), 
    'categories' => $categories);
}

// Verifies categories
foreach($posts as $post) 
  foreach($post['categories'] as $cat)
    if(!(in_array($cat, $all_categories) || in_array($cat, $bad_categories)))
      echo "<p><b>Unexpected category $cat for post <a target='_blank' href='".get_site_url()."?p=".$post['ID']."'>".$post['ID']."</a></b></p>";

// Sorts according to category order preserving the menu order of equivalent posts
uasort($posts, function($p1, $p2) { 
    $cat_order = $p1['cat_order'] - $p2['cat_order']; 
    $menu_order = $p1['menu_order'] - $p2['menu_order']; 
    return $cat_order == 0 ? $menu_order : $cat_order;
  });
$order = 0; foreach($posts as &$post) $post['new_menu_order'] = ++$order;
$sorted = true; for($index = 1; $sorted && $index < count($posts); $index++) if ($posts[$index-1]['menu_order'] > $posts[$index]['menu_order']) $sorted = false;

// Displays all posts as a table with the related links
function display_posts() {
  global $posts, $bad_categories, $sorted;
  echo "<table style='font-size:12px;color:black;border:1px;'><tr><th>ID</th><th style='width:400px;'>Title</th><th style='width:300px;'>The <a target='_blank' href='".$GLOBALS['classcodeUrl']."/wp-admin/edit.php?category_name=a-la-carte'>a-la-carte</a> sub-categories</th><th>menu_order</th><th>cat_order</th><th>new_order</th></tr>\n";
  foreach($posts as $post)
    echo "<tr><td><a target='_blank' href='".$GLOBALS['classcodeUrl']."?p=".$post['ID']."'>".$post['ID']."</a></td><td>".$post['title']."</td><td>".implode(" ; ", array_map(function($cat) use ($bad_categories) { return "<a target='_blank' href='".$GLOBALS['classcodeUrl']."/wp-admin/edit.php?category_name=$cat'>".(in_array($cat, $bad_categories) ? "<i>$cat</i>" : "$cat")."</a>"; }, $post['categories']))."</td><td align='right'>".$post['menu_order']."</td><td align='right'>".$post['cat_order']."</td><td align='right'>".$post['new_menu_order']."</td></tr>";
  echo "</table>\n";
  echo "<p><b>Les posts sont ".($sorted ? "bien triés" : "à retrier").".</p></b>";
}

// Now we dare to update the menu order of all posts !! 
// Be sure the data base has been saved before 
// https://pixees.fr/wp-content/plugins/class_code/sos-sort/index.php?update=JU098y9Ljkjshqwqr66yu
if ($_REQUEST['update'] == "JU098y9Ljkjshqwqr66yu") {
  foreach($posts as $post) {
    $id = wp_update_post(array(
			       'ID'           => $post['ID'],
			       'menu_order'   => $post['new_menu_order']));
    if ($id != $post['ID']) echo "<pre>AHH ! update error #$id for ".print_r($post, true)."</pre>";
  }
}

?>

<?php header('X-Frame-Options: GOFORIT'); ?>
<?php include_once(get_template_directory().'/_inc/display-functions.php'); ?>
<!DOCTYPE html>
<html>
 <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>"/>    
  <title>Tableau de bord des a-la-carte de Class'Code</title>
  <link href="<?php the_theme_file('/classcode.css');?>" type="text/css" rel="stylesheet" />
  <?php wp_head(); ?>
</head>
<body id="body" style="padding:0px;margin-left:auto;margin-right:auto;background-color:white">
<img style="width:100%" src="<?php echo $classcodeUrl; ?>/wp-content/plugins/class_code/message/header.png" />
<div style="padding:0px 10px;">

<?php display_posts(); ?>

</div>
<img style="display:block;width:90%;margin-left:auto;margin-right:auto;" src="<?php echo $classcodeUrl; ?>/wp-content/plugins/class_code/message/trailer.png" />
</body>
</html>