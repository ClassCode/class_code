<?php 
define('WP_USE_THEMES', false);
require_once("../../../../wp-load.php");

$classcodeUrl='';
if(empty($_SERVER["HTTPS"])){
  $classcodeUrl='http://';
}else{
  $classcodeUrl='https://';
}
$classcodeUrl.=$_SERVER["SERVER_NAME"];

?>
<?php header('X-Frame-Options: GOFORIT'); ?>
<!DOCTYPE html>
<html>
 <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>"/>    
  <title>Données ouvertes de Class'Code pour OpenPACA</title>
  <link href="<?php the_theme_file('/classcode.css');?>" type="text/css" rel="stylesheet" />
  <?php wp_head(); ?>
  <style>
  #profilDisplay { min-height: auto; }
  .moocsBloc { min-height:180px; height: auto; }
  #gmw-sl-wrapper-364, #profil_map_6 { display : none; }
.content-category-title {
	color:#3f6476;
	font-size: 1.25rem;
	font-weight:bold;
	text-transform: uppercase;
        border-bottom : 2px solid #27cfd0;
}
 </style>
 </head>
 <body id="body" style="width:1000px;background-color:white;padding:40px;">
<div>
  <img style="width:100%;" src="<?php echo get_site_url(); ?>/wp-content/plugins/class_code_v2/assets/images/slider/accueil/Home-01-narrow.png"/>
</div>
<p class="content-category-title">MISE EN PARTAGE DE DONNÉES D´APPRENTISSAGE DE CLASS´CODE</p>
 <a target="_blank" style="float:right;" title="Mise à dispositon de données ouvertes en collaboration avec OpenPACA" href="http://opendata.regionpaca.fr"><img width="300px;" src="./logo-openpaca.png"/></a><ul>
 <li><a target="_blank" href="#">Accès aux données sur le portail OpenPACA</a> (en cours de mise en place).</li>
 <li><a href="./description.pdf">Description des données mises en partage</a>.</li>
 <li><a href="<?php echo $classcodeUrl; ?>/classcode-v2/faq/#plateforme">Explication sur l'utilisation des données</a> (ouvrir "Qu'en est-il de mes données ?") et <a href="<?php echo $classcodeUrl; ?>/classcode-v2/credits/">mentions légales générales</a>.</li> <li><a href="<?php echo $classcodeUrl; ?>/wp-content/plugins/class_code/attestation/?raw=true">Accès à vos données personnelles</a> (si vous êtes inscrit·e et connecté·e) et accès à l´<a href="<?php echo $classcodeUrl; ?>/wp-content/plugins/class_code/attestation">attestation</a> correspondante.</li>

</ul>
<p class="content-category-title">Jeux de données disponibles</p>
<?php
  if (file_exists('../../../../wq-opendata/index_datas.php'))
    require_once('../../../../wq-opendata/index_datas.php');
  else
    echo "<h1>en cours de finalisation</h1>";
?>
<p class="content-category-title"></p>
</body>
</html>
