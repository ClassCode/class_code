<?php 
  // Exemple: https://pixees.fr/wp-content/plugins/class_code/attestation/?raw=true
  // Refs: https://drive.google.com/drive/folders/1Hj0jqcvP1czTNmL3L8r0A9-Cr7PWwcaM

if(!isset($argv[1]) || $argv[1] != 'please-do-it') exit(0);

define('WP_USE_THEMES', false);
require_once("../../../../wp-load.php");
require_once("../attestation/get_user_learning_data.php");

$raw_datas = array();

echo "[1] Retrieving all user learning data\n";

foreach(get_users() as $user) {
  $raw_data = get_user_learning_data($user->ID, false);
  $raw_datas[$user->ID] = $raw_data['user_local_profile'];
  foreach(array("ID", "Prénom", "Nom", "Login", "Email") as $item) unset($raw_datas[$user->ID][$item]);
  // Used for preliminary test ::: if (count($raw_datas) > 10) break;
}

echo "\t ... #".count($raw_datas)." records\n";

echo "[2] Extracting marginal statistics\n";

$datas = array(
  // title => array(array(...$fields...), array(...data...), array(...data...), ...);
);

function remove_accent($str) {
  $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
  $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
  return str_replace($a, $b, $str);
} 

function normalize_value($field) {
  return preg_replace("/\\\\/", "", preg_replace("/-/", " ", remove_accent(strtolower(trim($field)))));
}

$fields = array('Profil de l´apprenant',
		'Discipline informatique',
		'Matière enseignée en tant que professeur',
		'Matière suivie en tant qu´étudiant',
		'Facilitateur',
		'Compétences partagées ici',
		'Structure d’appartenance',
		'Cadre de la formation',
		'Localisation régionale',	
		'Pourcentage de participation au module #1',	
		'Pourcentage de participation au module #2',	
		'Pourcentage de participation au module #3',	
		'Pourcentage de participation au module #4',	
		'Pourcentage de participation au module #5',	
		'Nombre de participation à une rencontre',	 
		'Nombre d´organisation de rencontre',
		);

$counts = array();
foreach(array('Profil de l´apprenant', 'Discipline informatique', 'Matière enseignée en tant que professeur', 'Matière suivie en tant qu´étudiant', 'Facilitateur', 'Compétences partagées ici', 'Structure d’appartenance', 'Cadre de la formation', 'Localisation régionale') as $what) {
  $count = 0; $counts[$what] = array();
  foreach($raw_datas as $raw_data) {
    $values = $what == 'Compétences partagées ici' ? array_map("trim", explode(",", $raw_data[$what])) : array($raw_data[$what]);
    $tocount = false;
    foreach($values as $value) {
      $value = normalize_value($value);
      if($value != '') {
	$counts[$what][$value] = isset($counts[$what][$value]) ? $counts[$what][$value] + 1 : 1;
	$tocount = true;
      }
    }
    if ($tocount)
      $count++;
  }
  arsort($counts[$what]);
  $datas["Statistique sur «".strtolower($what)."» (pourcentage sur un sous-échantillon de #$count personnes)"] = array_map(function($th, $cc) use ($count) { return array($th, round(100 * $cc / $count)."%"); }, array_keys($counts[$what]), $counts[$what]);
}

$what = 'Profil de l´apprenant';
foreach(array('Pourcentage de participation au module #1', 'Pourcentage de participation au module #2', 'Pourcentage de participation au module #3', 'Pourcentage de participation au module #4', 'Pourcentage de participation au module #5', 'Nombre de participation à une rencontre', 'Nombre d´organisation de rencontre') as $result) {
  $mean = array(); $stdev = array();
  $count = 0;
  foreach(array_keys($counts[$what]) as $value) {
    $m0 = 0; $m1 = 0; $m2 = 0;
    foreach($raw_datas as $raw_data) 
      if (normalize_value($raw_data[$what]) == normalize_value($value) && $raw_data[$result] > 0) {
	$m0++;
	$m1 += $raw_data[$result];
	$m2 += $raw_data[$result] * $raw_data[$result];
      }
    $count += $m0;
    $mean[] = $m0 == 0 ? 0 : round($m1 / $m0);
    $stdev[] = $m0 == 0 ? 0 : round(sqrt(($m2 - $m1 * $m1 / $m0) / $m0));
  }
  $datas["Statistique par «".strtolower($what)."» de «".strtolower($result)."» (moyenne colonne 2 et écart type colonne 3 sur un sous-échantillon de #$count personnes)"] = array_map(function($th, $td1, $td2) { return array($th, $td1, $td2); }, array_keys($counts[$what]), $mean, $stdev);
}

echo "[3] Saving in data files\n";

// sudo su ; cd /var/www/pixees ; mkdir -m a+rwx wq-opendata
$folder_data = "../../../../wq-opendata/";

$index_datas = ""; $count = 0;
foreach($datas as $title => $data) {
  $count++; $filename = "data-$count";
  $index_datas .= "<li>[ <a href='$folder_data/$filename.csv'>CVS</a> <a href='$folder_data/$filename.json'>JSON</a> <a href='$folder_data/$filename.html'>HTML</a> ] : $title</li>\n";
  $csv_string = ""; $html_string = "<html><head><title>$title</title><meta charset='UTF-8'/></head><body><h4>$title<h4><table>\n";
  foreach($data as $ii => $row) {
    $html_string.= "  <tr>"; 
    foreach($row as $jj => $cell) {
      $tag = $jj == 0 ? "th" : "td"; 
      $value = preg_replace("/\s+/", " ", trim($cell));
      $cvs_string .= ($jj == 0 ? "" : "\t").$value;
      $html_string.= "<$tag>$value</$tag>";
    }
    $cvs_string .= "\n"; $html_string.= "</tr>\n";
  }
  $html_string.= "</table></body></html>\n";
  file_put_contents($folder_data.$filename.'.csv', $cvs_string);
  file_put_contents($folder_data.$filename.'.json', json_encode($data, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK));
  file_put_contents($folder_data.$filename.'.html', $html_string);
}

if ($index_datas != "")
  file_put_contents($folder_data.'index_datas.php', "<ul>\n".$index_datas."</ul>\n");

?>