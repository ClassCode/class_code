<?php
/**
 * Defines the functionnalities to access to the Discourse API.
 * @see http://learndiscourse.org/discourse-api-documentation
 */
 
$classcodeUrl='';
if(empty($_SERVER["HTTPS"])){
  $classcodeUrl='http://';
}else{
  $classcodeUrl='https://';
}
$classcodeUrl.=$_SERVER["SERVER_NAME"];

class DiscourseAPI {
  /** Creates or retrives a rencontre topic and returns its URL.
   * @param $rencontre_id The rencontre id.
   */
  public static function get_rencontre_topic($rencontre_id) {
    if (get_post_meta($rencontre_id, "discourse/url", true) == '') {
      // Creates the rencontre topic on discourse
      $result1 = self::api_post("POST", 
				"posts",
				array(
				      "title" => "Échange à propos de la rencontre #$rencontre_id",
				      "raw" => "Bienvenue sur le fil d´échange de la rencontre ".$GLOBALS['classcodeUrl']."/rencontre/$rencontre_id\n- Nous nous y entraidons, partageons, dialoguons, ...",
				      ));
      if ($result1) {
	// Sets the category
	$result2 = self::api_post("PUT", 
				  "t/".$result1['topic_id'], 
				  array(
					"topic_id" => $result1['topic_id'],
					"category_id" => 5, // Obtained via curl https://pixees.fr/forum/categories.json
					));
	
	// Builds the URL
	$url = $GLOBALS['classcodeUrl']."/forum/t/".$result1['topic_slug']."/".$result1['topic_id'];
	update_post_meta($rencontre_id, "discourse/url", $url);
      } else {
	update_post_meta($rencontre_id, "discourse/url", $GLOBALS['classcodeUrl']."/forum");
      }
    }
    return get_post_meta($rencontre_id, "discourse/url", true);
  }
  
  /** Performs a API post and return the obtained JSON structure as an array.
   * @param $method The http method "POST" or "PUT"
   * @param $what The API request past.
   * @param $data The API request parameters.
   */
  private static function api_post($method, $what, $data) {
    $curl_request = curl_init();
    if ($method == "POST") {
      curl_setopt($curl_request, CURLOPT_POST, true);
    } else if ($method == "PUT") {
      curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, "PUT");
    } else 
      return false;
    curl_setopt_array($curl_request, 
		      array(
			    CURLOPT_URL => $GLOBALS['classcodeUrl']."/forum/$what",
			    CURLOPT_HEADER => false,
			    CURLOPT_RETURNTRANSFER => true,
			    CURLOPT_POSTFIELDS => http_build_query($data + 
								   array(
									 "api_key" => self::$api_key,
									 "api_username" => self::$api_username)) 
			    ));
    $response = curl_exec($curl_request);
    $code = curl_getinfo($curl_request, CURLINFO_HTTP_CODE);
    if ($code == 200 && $response)
      $response = json_decode($response, true);
    curl_close($curl_request);
    return $response;
  }
  private static $api_key = "b2b2fb4fa043cc7de80aade92f410a7c40eac922e31ebcfceaed74763a6e4d83";
  private static $api_username = "system";
}
?>
