<?php  
  $flashMessage = '';  
  include_once(plugin_dir_path( __FILE__ ).'/../ClassCode_config.php');
  global $wpdb;
  
  $profil_url = bp_loggedin_user_domain()."profile/edit/group/1";
  $visitorId = bp_loggedin_user_id();   
  
  if(isset($_REQUEST['action'])){
    $action = $_REQUEST['action'];
    if($action=="delete"){
      $flashMessage = "La Rencontre a été supprimée avec succès !";
    }
  }
  if($flashMessage!=""){
    echo '<div id="flashMessage">';
      echo $flashMessage;
    echo '</div>';
  }
  
  $classcodeUrl='';
  if(empty($_SERVER["HTTPS"])){
    $classcodeUrl='http://';
  }else{
    $classcodeUrl='https://';
  }
  $classcodeUrl.=$_SERVER["SERVER_NAME"];
  
?>
<div id="classCodeMeetingHeader">
  <div id="classCodeMeetingActions" class="contentMedia">
    <h2>Toutes les rencontres</h2>
<?php 
  if(is_user_logged_in()) {
?>
    <a href="#classCodeMeetingMain"><div id="classCodeMeetingAction1" class="whiteRectangle"><table><td><span class="bigNumber">1</span></td><td>Chercher<br/>les rencontres</td></table></div></a>
    <a href="#classCodeMeetingSecondary"><div id="classCodeMeetingAction2" class="whiteRectangle"><table><td><span class="bigNumber">2</span></td><td>Lister<br/>les rencontres</td></table></div>
    </a>
    <a href="#classCodeMeetingTierce"><div id="classCodeMeetingCreate" class="whiteRectangle"><table><td><span class="bigNumber">3</span></td><td>Créer<br/>un temps de rencontre</td></table></div>
    </a>
<?php 
  } else {
?>
    <a href="#classCodeMeetingMain"><div id="classCodeMeetingAction1" class="whiteRectangle"><table><td><span class="bigNumber"></span></td><td>Chercher<br/>les rencontres</td></table></div></a>

<?php 
  }
?>


  </div>
  <div id="profilDisplay" class="contentDesc"> 
<?php
  if (is_user_logged_in()) {
    echo display_classcode_profile($visitorId);
  } else {
    echo '<div style="width:80%" id="contentTitleResume" class="contentDesc"><h2>Bienvenu(e)s</h2>
<p>Class´Code propose un programme de formation, basé sur une formation en ligne d’une dizaine d’heures chacun, couplé à des temps de rencontre présentielle pour partager, expérimenter et échanger entre apprenants.</p>
<p>En cliquant sur Rechercher vous avez un apperçu (cliquer sur la carte) des rencontres en cours ou prévues.</p>
<p>Pour en savoir plus ou participer il faut s´<a href="'.$classcodeUrl.'/wp-login.php?action=register">inscrire</a> et se <a href="'.$classcodeUrl.'/wp-login.php">connecter</a>.</p>
</div>';
  }
?>
  </div>
</div>
<div id="profilFooter">
  <div id="classCodeMeetingActionsFooter" class="contentMedia"></div>
  <div id="profilSubmit" class="contentDesc">
<?php 
  if(is_user_logged_in()) {
?>
    <a href="<?php echo $profil_url;?>">Modifier mon profil</a>
<?php 
  }else{
?>
    <a href="<?php echo wp_login_url(get_permalink());?>">Se Connecter</a></div>    
<?php
  }
?>
  </div>
</div>
<div id="anchorSearchform"></div>
<div id="classCodeMeetingMain">
  <div id="classCodeMeetingMainHeader">
    <h2>1 | Chercher les rencontres</h2>
    Sélectionner les rencontres que vous souhaitez visualiser ou modifier. <?php echo_help_tooltip('Tutoriel pour s´inscrire à une rencontre', 'gray', 'https://www.youtube.com/watch?v=tyoVNSE89Bs'); ?> <p>La formation Class'Code repose sur deux aspects : une partie en ligne, et une partie présentielle. Ces temps de rencontre sont essentiels pour tester les activités avant de les proposer aux jeunes, bénéficier de temps d’échanges avec des professionnels de l’éducation et de l’informatique, ainsi que rester en lien ensuite pour être accompagné.</p>
  </div>

  <?php echo cc_meeting_search_shortcode(array()) ;?>
  
</div>

<?php 
  if(is_user_logged_in()) {
?>

<div id="classCodeMeetingSecondary">
    <div id="classCodeMeetingSecondaryHeader" class="classCodeMeetingSecondaryHeaderSearch">
      <h2>2 | Lister les rencontres</h2><?php echo cc_help_shortcode(array("picto" => "gray", "title" => "Besoin d´aide pour trouver une rencontre ?", "subject" => "Je ne trouve pas de rencontres", "body" => "&#13;&#10;Le module (1,2,3,4 ou 5) : &#13;&#10;Ma ville: ")); ?>
<?php 
    global $meetings;   
    $postNearby=false;
    if(isset($_POST['where'])){
      if($_POST['where']!='everywhere'){
        $postNearby=true;  
      }
    }
   $postModule=false;
    if(isset($_POST['module']) && $_POST['module'] ){
      $postModule=$_POST['module'];       
    }
    $postWhen = false;
    if(isset($_POST['when']) && $_POST['when'] ){
      $postWhen = $_POST['when'];       
    }
    $postMyMeeting = false;
    if(isset($_POST['mymeeting']) && $_POST['mymeeting'] ){
      $postMyMeeting = true;       
    }
    $postStructure = false;
    if(isset($_POST['structure']) && $_POST['structure'] ){
      $postStructure=$_POST['structure'];       
    }
      
    if(count($meetings)==0){            
      echo "Aucune rencontre correspondant aux critères ci-dessus n'a été trouvée ! Relancer une recherche en modifiant les critères ou créez une rencontre dans la section suivante.<br/>";
    }else{
      echo "<b>".count($meetings)."</b> rencontre".(count($meetings) > 1 ? "s" : "")." correspondant aux critères ci-dessus ont été trouvées ! ";
      echo "Les rencontres marquées en bleu ciel dans la colonne de droite sont celles où vous êtes inscrit-e-s.<br/>";
    }
?>

 <br/><i>Vous ne trouvez pas de temps de rencontre ?</i> Vous avez trois solutions:  <ol>
   <li>Devenir organisateur en <a href="https://pixees.fr/classcode/rencontres/creer-une-rencontre/">créant un temps de rencontre</a> vous même <br/> (il y a sûrement des personnes près de chez vous (collègues, partenaires) qui ont le même besoin).<?php echo_help_tooltip('En savoir plus sur le rôle d´organisateur', 'gray', $classcodeUrl.'/classcode/accueil/documentation/classcode-la-documentation-organisateur/'); ?></li>
    <li>Contacter un coordinateur pour lui demander de l'aide <?php echo_help_tooltip('Contacter un coordinateur', 'gray', $classcodeUrl.'/classcode/accueil/aide/#aideAction3'); ?></li>
<li>Vous <?php echo rencontre_alert::get_user_alert($postModule, $postNearby, $text = 'inscrire à une alerte'); ?> par mail (qui correspond à votre dernière recherche) <br/> pour être informé dès qu'un temps de rencontre est organisé, comme expliqué ici : <?php echo_help_tooltip('<ul>
     <li>Faire la recherche sur <br/>- le module (1, 2, 3, 4, ou 5) <br/> de votre choix, <br/> - partout ou près de chez vous.</li>
     <li>Aucune rencontre ? Un lien permet d´ajouter une alerte.</li>
    </ul>', 'gray'); ?></li>
  </ol>

<?php
    echo "</div>";
    // Affiche les alertes
    {
      $myAlertsHtml = rencontre_alert::get_user_alerts();
      if($myAlertsHtml != ''){
	echo $myAlertsHtml;  
      }
    }

    if(count($meetings)!=0){     
          
?>      
    </div>
  <table id="meetingList"  class="tablesorter"  border="0" cellspacing="0" cellpadding="0">
    <thead id="meetingListHeader">
      <tr>
        <th id="meetingListHeaderStructure" class="meetingListHeaderDefault sortable">Structure <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></th>
        <th id="meetingListHeaderLieu" class="meetingListHeaderDefault sortable">Lieu <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></th>
        <th id="meetingListHeaderDistance" class="meetingListHeaderDefault sortable">Distance (km)<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></th>
        <th id="meetingListHeaderModule" class="meetingListHeaderDefault sortable">Module <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></th>
        <th id="meetingListHeaderDate1" class="meetingListHeaderDefault sortable">1ère date <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></th>
        <th id="meetingListHeaderDate2" class="meetingListHeaderDefault sortable">2ème date <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></th>
        <th class="meetingListHeaderDefault">Plus de détails</th>
      </tr>
    </thead>
    <tbody>
<?php  

    $meeting_inc = 0;
    foreach($meetings as $meeting){
?>
      <tr>
      <td class='meetingListStructure'><?php echo $meeting['structure']; ?></td>
      <td class='meetingListLieu'><a onclick="map.setCenter(new google.maps.LatLng(<?php echo $meeting['latlong']; ?>));focusOnMap();"><?php echo $meeting['lieu']; ?></a></td>
      <td><?php echo $meeting['distance']; ?></td> 
      <td><?php echo $meeting['module']; ?></td>  
      <td><?php echo $meeting['date1']; ?></td>
      <td><?php echo $meeting['date2']; ?></td> 
      
<?php
      if($meetings[$meeting_inc]['userStatut'] =='organisateur'){
?>      
        <td class='meetingShow '>
          <span class="meetingShowOrganizer"><a href='<?php echo $meeting['url'];?>&edit=true'>Modifier #<?php echo $meetings[$meeting_inc]['post_id']; ?></a></span>
          <span class=""><a href='<?php echo $meeting['url'];?>'>Voir #<?php echo $meetings[$meeting_inc]['post_id']; ?></a></span>
<?php
      }else if($meetings[$meeting_inc]['userStatut'] =='participant'){
?>
        <td class='meetingShow meetingShowAttendee'><a href='<?php echo $meeting['url'];?>'>
        Voir la rencontre #<?php echo $meetings[$meeting_inc]['post_id']; ?></a>
<?php
      }else{
?>
        <td class='meetingShow'><a href='<?php echo $meeting['url'];?>'>
        Voir la rencontre #<?php echo $meetings[$meeting_inc]['post_id']; ?></a>
<?php
      }
?>
        </td>
      </tr>
<?php
      $meeting_inc++;
    }
?>
    </tbody>
  </table>
<?php
  }
?>

<div id="postMeetingList">

</div>

</div>
<script type="text/javascript">
  function focusOnMap(){
    
    jQuery('html,body').animate({scrollTop: jQuery("#classCodeMeetingMainHeader").offset().top}, 'slow'      );
  }
  jQuery(document).ready(function(){ 

<?php 
    if(count($meetings) > 0){
?> 
      jQuery.tablesorter.addParser({ 
        // set a unique id 
        id: 'classCodeDates', 
        is: function(s) { 
            // return false so this parser is not auto detected 
            return false; 
        }, 
        format: function(s) { 
            // format your data for normalization 
            sDateReformated = s;
            if(s.length>0){
              var sTmp = s.split("à");
              sDate = sTmp[0];
              sDateArray = sDate.split('/');
              sDateReformated = sDateArray[2].trim()+"/"+sDateArray[1].trim()+"/"+sDateArray[0].trim();
            }
            return sDateReformated;
        }, 
        // set type, either numeric or text 
        type: 'text' 
      });
      jQuery("#meetingList").tablesorter( {
                                          sortList: [[0,0]],
                                          widgets: ['zebra'],
                                          dateFormat: "uk",
                                          headers: {4: { sorter:'classCodeDates'}, 5: { sorter:'classCodeDates'},6:{sorter: false}},
                                        } ); 
<?php
    }
?>                                      
  }); 

</script>



<div id="meetingListFooter"></div>

<div id="classCodeMeetingTierce">
  <div id="classCodeMeetingTierceHeader">
    <h2>3 | Créer un temps de rencontre</h2>
    Pas de rencontre correspondant à votre besoin ? Vous pouvez vous même créer une rencontre Class'Code sur un module spécifique ou sur un sujet connexe, 
    et d'autres participants viendront vous rejoindre. <?php echo_help_tooltip('Tutoriel pour créer une rencontre', 'gray', 'https://www.youtube.com/watch?v=3z3nIzK0IrU'); ?>

      <table align="center"><tr>

        <td align="center"><a href="<?php echo get_site_url(); ?>/classcode/rencontres/creer-une-rencontre/"><img src="<?php echo get_site_url(); ?>/wp-content/themes/pixees-theme/_img/classcode_pictos/meeting/Img-rencontre-15_green.png" title="Créer une rencontre" alt="Créer une rencontre"/></a></br><h4>Créer une rencontre</h4></td>

        <td align="center"><a href="<?php echo get_site_url(); ?>/classcode/rencontres/trouver-des-participants/"><img src="<?php echo get_site_url(); ?>/wp-content/themes/pixees-theme/_img/classcode_pictos/meeting/Img-rencontre-15_blue.png" title="Trouver des participants" alt="Trouver des participants"/></a></br><h4>Contacter des participant-e-s</h4></td>

      </tr></table>


    <div>Tout le monde peut devenir organisateur de Class'Code. <?php echo_help_tooltip('En savoir plus sur le rôle d´organisateur', 'gray', $classcodeUrl.'/classcode/accueil/documentation/classcode-la-documentation-organisateur/'); ?></div>

  </div>
</div>

<?php

  } //fin if connecté
?>