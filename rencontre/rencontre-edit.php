<?php // Implements the edition or display of a rencontre post
  
// Usage: link?post_id=$id 		to display a rencontre
// Usage: link?post_id=$id&edit=true 	to edit a rencontre
// Usage: link				to create edit a rencontre
  // Creates the post if not yet done
 
include_once('rencontre_post_type.php');
//include_once GMW_PT_PATH . 'includes/admin/gmw-pt-metaboxes.php';
//include_once GMW_PATH . '/includes/admin/geo-my-wp-admin-functions.php' ;
include_once(plugin_dir_path( __FILE__ ).'/../ClassCode_config.php');
include_once('rencontre_discourse.php');
wp_enqueue_script('jquery-ui-datepicker');
wp_enqueue_style('jquery-style', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');

$classcodeUrl='';
if(empty($_SERVER["HTTPS"])){
  $classcodeUrl='http://';
}else{
  $classcodeUrl='https://';
}
$classcodeUrl.=$_SERVER["SERVER_NAME"];

// Redirection if the user is not connected
if (!is_user_logged_in()) {
  echo "<div id='guidedTour'><h2>Vous devez être connecté pour accéder à cette page !</h2>";
  echo "<h2><a href='".get_site_url()."/classcode/accueil/'' >Retour à l'accueil</h2></a></div>";
  //header('Location: '.get_site_url().'/classcode/accueil/');
  return;
}

$flashMessage = '';

// Visualization du logo de la structure organisatrice de la rencontre
function get_cc_logo_HTML($data_structures, $structure) {
  if (isset($data_structures[$structure]) &&  is_array($data_structures[$structure])) {
    $data = $data_structures[$structure];
    return '<a class="structureLogo" style="display:block;float:right;" title="'.$structure.'" href="'.$data['url'].'"><img width="180" src="'.$data['logo'].'" alt="'.$structure.'"/></a>';
  } else {
    return '';
  }
}

$visualization='create';
global $wpdb;    
      
// Determination du type de visualisation : edition, creation ou visualisation

if (isset($_REQUEST['post_id'])){
  if ( FALSE === get_post_status( $_REQUEST['post_id'] ) ) {
    //le post n'existe pas
    $visualization='create';
    unset($_REQUEST['post_id']) ;
  }else{ // le poste existe
    if(get_post_type($_REQUEST['post_id']) != 'rencontre'){
      // le post n'est pas de type rencontre
      $visualization='create';
      unset($_REQUEST['post_id']) ;
    }else{ // le poste existe et est de type rencontre
      if(isset($_REQUEST['action'])){
        $action = $_REQUEST['action'];
        if($action=="update"){
        $flashMessage = "Bravo, Rencontre modifiée avec succès !";
        }else if($action=="create"){
          $flashMessage = "Bravo, Rencontre créée avec succès !";
        }else if($action=="duplicate"){
          $flashMessage = "Bravo, Rencontre dupliquée avec succès !";
        }
      }
      
      $post = get_post($_REQUEST['post_id']);
      $organizerId = $post->post_author;
      $organizerdisplayName = getDisplayName($organizerId);
     
      $argsStructure = array(
        'field' => 'Structure',
        'user_id' => $organizerId
      );
      $organizerStructure = bp_get_profile_field_data($argsStructure);
      
      $organizerLocation = $wpdb->get_row($wpdb->prepare( "SELECT * FROM wppl_friends_locator WHERE member_id = %s", $organizerId ) );
      $organizerLat = $organizerLocation->lat;
      $organizerLong = $organizerLocation->long;
      $organizerFullAdress = $organizerLocation->formatted_address;
      $organizerStreet = $organizerLocation->street;
      $organizerCity = $organizerLocation->city;
      $organizerPostalCode = $organizerLocation->zipcode;
      $organizerCountry = $organizerLocation->country_long;
      $postLat = $organizerLat;
      $postLong = $organizerLong;
      
      $postLocation = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM wp_places_locator WHERE post_id = %d", array( $_REQUEST['post_id'] ) ) );        
      $postAddress = $postLocation->street ;
      $postAddress = str_replace("\\", "",$postAddress);
      $postCity = $postLocation->city ;
      $postState = $postLocation->state ;
      $postPostalCode = $postLocation->zipcode ;
      $postLat = $postLocation->lat ;
      $postLong = $postLocation->long ;      
      $postFormattedAddress = $postLocation->formatted_address  ;
      $postFormattedAddress = str_replace("\\", "",$postFormattedAddress);
      $postStructure = get_post_meta($_REQUEST['post_id'], 'structure', true);
      $postCapacity = get_post_meta($_REQUEST['post_id'], 'capacity', true);
      $postPrecisions = get_post_meta($_REQUEST['post_id'], 'precisions', true);
      $postModule = get_post_meta($_REQUEST['post_id'], 'rencontre_module', true);
      $module_id = 0; 
      foreach($title_module as $id => $title){
        if ($postModule == $title){
          $module_id = $id;  
        } 
      }
      //existance des kits modules
      if (isset($kit1_module[$module_id])){
        $kit1_module_display_link = $kit1_module[$module_id];
      }
      if (isset($kit2_module[$module_id])){
        $kit2_module_display_link = $kit2_module[$module_id];
      }
      //existance de l'url de description du module
      if(isset($url_module_array[$module_id])){
        $url_module_link = $url_module_array[$module_id];
      }
        
      $postDate1 = get_post_meta($_REQUEST['post_id'], 'rencontre_date_1', true);
      $postTime1 = get_post_meta($_REQUEST['post_id'], 'rencontre_heure_1', true);
      $postDate2 = get_post_meta($_REQUEST['post_id'], 'rencontre_date_2', true);
      $postTime2 = get_post_meta($_REQUEST['post_id'], 'rencontre_heure_2', true);
      // $postWebpad = get_post_meta($_REQUEST['post_id'], 'rencontre_pad', true);  
      
      if(isset($_REQUEST['edit'])){
        if((bp_loggedin_user_id()!=$organizerId) && !in_array('administrator', wp_get_current_user()->roles)){
          //le visiteur n'est pas l'auteur, on le repasse en visualisation
          $visualization='visualize';    
        }else{
          $visualization='edit';    
        }
      }else{
        $visualization='visualize';  
      }      
    }
  }
}

if($visualization=='create'){
  $organizerId = bp_loggedin_user_id();  
  $organizerLocation = $wpdb->get_row($wpdb->prepare( "SELECT * FROM wppl_friends_locator WHERE member_id = %s", $organizerId ) );
  $organizerLat = $organizerLocation->lat;
  $organizerLong = $organizerLocation->long;
  $postLat = $organizerLat;
  $postLong = $organizerLong;
  $organizerFullAdress = $organizerLocation->formatted_address;
  $organizerStreet = $organizerLocation->street;
  $organizerCity = $organizerLocation->city;
  $organizerPostalCode = $organizerLocation->zipcode;
  $organizerCountry = $organizerLocation->country_long;
  $organizerState = $organizerLocation->state;
  $postAddress = $organizerStreet;
  $postCity = $organizerCity;
  $postPostalCode = $organizerPostalCode;   
  $postState = $organizerState;
  $postFormattedAddress = $organizerFullAdress ;
}
$noloc = false;
if(!$postLat or ($postLat == '') or !$postLong or ($postLong == '')){
  $postLat =  '46.52863469527167';
  $postLong = '2.43896484375'; 
  $noloc = true;
}  

$current_url = home_url(add_query_arg(array(),$wp->request));
$profil_url = bp_loggedin_user_domain()."profile/edit/group/1";

if($flashMessage!=""){
  echo '<div id="flashMessage">';
    echo $flashMessage;
  echo '</div>';
}
  
if($visualization!='visualize'){ 
?>

<div id="classCodeMeetingHeader">
  <div id="classCodeMeetingActions" class="contentMedia">
    <h2>
<?php  
  if($visualization=='edit'){
    echo "Modifier la rencontre " ; 
    if(isset($_REQUEST['post_id'])){ 
      echo '#'.$_REQUEST['post_id']; 
    }     
  }else{
    echo "Créer une rencontre";  
  }
?>
    </h2>
    <a href="#classCodeMeetingMain">
      <div id="classCodeMeetingAction1" class="whiteRectangle"><table><td><span class="bigNumber">1</span></td><td>Déclarer<br/>un lieu de rencontre</td></table></div>
    </a>
    <a href="#classCodeMeetingSecondary"><div id="classCodeMeetingAction2" class="whiteRectangle"><table><td><span class="bigNumber">2</span></td><td>Définir<br/>les temps de rencontre</td></table></div>
    </a>
    <a href="#classCodeMeetingTierce"><div id="classCodeMeetingAction3" class="whiteRectangle"><table><td><span class="bigNumber">3</span></td><td>Gérer<br/>la participation</td></table></div>
    </a>
  </div>
  <div id="profilDisplay" class="contentDesc"> 
<?php
    echo display_classcode_profile(bp_loggedin_user_id());
?>
  </div>
</div>
<div id="profilFooter">
  <div id="classCodeMeetingActionsFooter" class="contentMedia"></div>
  <div id="profilSubmit" class="contentDesc">
    <a href="<?php echo $profil_url;?>">Modifier mon profil</a>
  </div>
</div>
<?php

  // Encapsulates the interaction in a form
  echo '<form action="'.get_site_url().'/wp-content/plugins/class_code/rencontre/rencontre_creation.php" method="post" id="meetingForm" class="rencontres-edit">';
  // Protects the save operation
  wp_nonce_field('rencontres_edit_id_nonce', 'rencontres_edit_data_nonce');
  echo "<input type='hidden' value='rencontre' id='post_type' name='post_type'>";   
  // Propagates the post_id
  if (isset($_REQUEST['post_id'])){
    echo '<input type="hidden" name="post_id" value="'.$_REQUEST['post_id'].'"/>';
  }
?>
 <h3 style="float:right;font-weight:bold;padding-right:80px;">Documentation : <?php echo_help_tooltip('En savoir plus sur le rôle d´organisateur', 'gray', $classcodeUrl.'/classcode/accueil/documentation/classcode-la-documentation-organisateur/'); ?></h3>

  <div id="classCodeMeetingMain">
    <div id="classCodeMeetingMainHeader">
     <h2>1 | Déclarez le lieu de la rencontre</h2> 
     Définir le lieu précis de rencontre et les informations pour s'y rendre. Il sera ensuite facile de dupliquer cettte rencontre sans devoir re-renseigner le lieu. <br/> <br/> Si votre structure n'est pas dans le menu déroulant <a href="<?php echo cc_mailto_shortcode(array("subject" => "Merci de rajouter ma structure pour les temps de rencontre", "body" => "Nom exact de la structue :&#13;&#10;URL du site de la structure :&#13;&#10;URL du logo de la structure :&#13;&#10;Adresse postale (pour localisation sur la cartographie) :&#13;&#10;merci !")); ?>">demandez-le</a>.
     <br/><i>Attention:</i> si vous changez de lieu ou de date ... prévenez les participants (bouton «contacter le groupe», ci-dessous) !
    </div>
    <div id="classCodeMeetingMainDisplay">  
     <div id="classCodeMeetingMap" class="contentMedia"></div>
      <div id="classCodeMeetingAddress" class="contentDesc">
<?php        

     echo "<label>Structure"; echo_help_tooltip($helpTab['structure'], 'gray'); echo "</label><br/>";
        $postStructure=htmlentities($postStructure);
        echo do_completion_field("structure", array_keys($data_structures), $postStructure, "classCodeMeetingAddressInput", false).'<br/>';

        echo "<label>Adresse"; echo_help_tooltip($helpTab['adresse'], 'gray'); echo "</label><br/>";      
        echo '<input class="classCodeMeetingAddressInput" id="_wppl_street" type="text" name="_wppl_street" value="'.$postAddress.'"/><br/>';
        echo '<br/>';
        echo "<label>Ville</label><br/>";
        echo '<input class="classCodeMeetingAddressInput" id="_wppl_city" type="text" name="_wppl_city" value="'.$postCity.'"/><br/>';
        echo '<br/>';
        echo "<label>CP</label><br/>";
        echo '<input class="classCodeMeetingAddressInput" id="_wppl_zipcode" type="text" name="_wppl_zipcode" value="'.$postPostalCode.'"/><br/>';
        echo '<br/>';
        echo "<label>Capacité d'accueil</label><br/>";
        echo '<input class="classCodeMeetingAddressInput" id="capacity" type="text" name="capacity" value="'.$postCapacity.'"/><br/>';
        echo '<br/>';
        echo "<label>Précisions éventuelles</label><br/>";
        echo '<textarea rows="40" cols="50" class="classCodeMeetingAddressInput" id="precisions" name="precisions">'.$postPrecisions.'</textarea><br/>';
       
        // input for geomywp
        //nonce
        echo '<input type="hidden" name="this->meta_boxes_nonce" value="'.wp_create_nonce('gmw-pt-metaboxes.php').'" />';

        //fields
        echo '<input id="_wppl_lat" type="hidden" name="_wppl_lat" value="'.$postLat.'"/>';      
        echo '<input id="_wppl_long" type="hidden" name="_wppl_long" value="'.$postLong.'"/>';  
        echo '<input id="_wppl_formatted_address" type="hidden" name="_wppl_formatted_address" value="'.$postFormattedAddress.'"/>';  
        
        echo '<input id="post_title" type="hidden" name="post_title" value="'.$postFormattedAddress.'"/>';  
        echo '<input id="post_status" type="hidden" name="post_status" value=""/>';  
        echo '<input id="_wppl_street_number" type="hidden" name="_wppl_street_number" value=""/>';  
        echo '<input id="_wppl_street_name" type="hidden" name="_wppl_street_name" value=""/>';   
        echo '<input id="_wppl_apt" type="hidden" name="_wppl_apt" value=""/>'; 
        echo '<input id="_wppl_state" type="hidden" name="_wppl_state" value="'.$postState.'"/>'; 
        echo '<input id="_wppl_state_long" type="hidden" name="_wppl_state_long" value=""/>'; 
        echo '<input id="_wppl_country" type="hidden" name="_wppl_country" value="FR"/>'; 
        echo '<input id="_wppl_country_long" type="hidden" name="_wppl_country_long" value=""/>'; 
        echo '<input id="_wppl_address" type="hidden" name="_wppl_address" value=""/>'; 
        echo '<input id="_wppl_phone" type="hidden" name="_wppl_phone" value=""/>'; 
        echo '<input id="_wppl_fax" type="hidden" name="_wppl_fax" value=""/>'; 
        echo '<input id="_wppl_email" type="hidden" name="_wppl_email" value=""/>'; 
        echo '<input id="_wppl_website" type="hidden" name="_wppl_website" value=""/>'; 

        ?>        

      </div>
    </div>
    <div id="classCodeMeetingMainFooter">
      <div id="classCodeMeetingMapFooter" class="contentMedia"></div>
      <div id="classCodeMeetingMapUpdate" class="contentDesc"><a onclick="updateGMap();">Valider la position</a></div>
    </div>
  
  </div>
  <?php   
   // wp_enqueue_script('gmap', "https://maps.google.com/maps/api/js?sensor=false");
?>
  <script type="text/javascript">   
  var geocoder;
  var map;
  var marker;
  <?php echo "var markerImage = '".get_template_directory_uri()."/_img/classcode_pictos/meeting/Img-rencontre-07.png';"; ?>
  jQuery( document ).ready(function() {
<?php
    echo "var latlng = new google.maps.LatLng(".$postLat.", ".$postLong.");";
?>
    
    loadGmap(latlng);
    
    //when dragging the marker on the map
    google.maps.event.addListener( marker, 'dragend', function(evt){
      jQuery("#_wppl_lat").val( evt.latLng.lat() );
      jQuery("#_wppl_long").val( evt.latLng.lng() );
      returnAddress( evt.latLng.lat(), evt.latLng.lng(), false );  
    });
    
    
  });        
  
  /* main function to conver lat/long to address */
  function returnAddress( gotLat, gotLng, updateMap ) {
    
    //remove all address fields
    jQuery('#_wppl_street, #_wppl_city, #_wppl_zipcode').val('');
    
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(gotLat ,gotLng);
  
    //geocode lat/lng to address
    geocoder.geocode( {'latLng': latlng }, function(results, status) {		
      if (status == google.maps.GeocoderStatus.OK) {
            if ( results[0] ) {
          breakAddress(results[0]);
          if ( updateMap == true ) update_map();
            }
          } else {
            alert("Geocoder failed due to: " + status);
          }
      });
  }
  function dump(obj) {
    var out = '';
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }

    alert(out);
  }
  //address components
  function breakAddress(location) {  
    var addressComponent = location.address_components;
    var addressComponentTab = [];
    for(i = 0; i < addressComponent.length; i++){
      component = addressComponent[i];
      addressComponentTab[component['types'][0]] = component['long_name'];        
    }
    var streetAdress = "";
    if(addressComponentTab['street_number']){
      streetAdress = addressComponentTab['street_number']+" ";
    }
    if(addressComponentTab['route']){
      streetAdress += addressComponentTab['route'];
    }
    jQuery("#_wppl_street").val(streetAdress);
    jQuery("#_wppl_city").val(addressComponentTab['locality']);
    jQuery("#_wppl_zipcode").val(addressComponentTab['postal_code']);
    //region
    jQuery("#_wppl_state").val(addressComponentTab['administrative_area_level_1']);
    //departement
    //jQuery("#_wppl_state").val(addressComponentTab['administrative_area_level_2']);
    jQuery("#_wppl_formatted_address").val(location.formatted_address);
    jQuery("#post_title").val(location.formatted_address);
  }
  
  function loadGmap(latlng) {
<?php
  if($noloc === true){
    echo "var mapZoom = 5";
  }else{
    echo "var mapZoom = 11";
  }
?>
    //objet contenant des propriétés avec des identificateurs prédéfinis dans Google Maps permettant de définir des options d'affichage de notre carte
    var options = {
        center: latlng,
        zoom: mapZoom,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
    //constructeur de la carte qui prend en paramêtre le conteneur HTML dans lequel la carte doit s'afficher et les options
    map = new google.maps.Map(document.getElementById("classCodeMeetingMap"), options);

    geocoder = new google.maps.Geocoder();

    marker = new google.maps.Marker({
      map: map,
      icon: markerImage,
      position: latlng,
      title: 'Temps de rencontre',
      draggable: true
    });

  }
  
  function updateGMap(formSubmit){
    formSubmit = typeof formSubmit !== 'undefined' ? formSubmit : false;
    var location = document.getElementById("structure").value;
    var address = document.getElementById("_wppl_street").value;
    var city = document.getElementById("_wppl_city").value;
    var postalCode = document.getElementById("_wppl_zipcode").value;
    var country = "France";

    var completeAddress = address+","+postalCode+","+city+","+country;
    geocoder.geocode( 
      { 'address': completeAddress,
      }, 
      
      function(results, status) {    
      if (status == google.maps.GeocoderStatus.OK) {
        latlong = results[0].geometry.location ;
        newlat=latlong.lat();
        newlong=latlong.lng();
        jQuery("#_wppl_lat").val(newlat);
        jQuery("#_wppl_long").val(newlong);
        jQuery("#_wppl_formatted_address").val(results[0].formatted_address);      
        var addressComponentTab = [];
        for(var i = 0; i < results[0].address_components.length; i++){
          var component = results[0].address_components[i];
          addressComponentTab[component['types'][0]] = component['long_name'];        
        }
        var state = addressComponentTab['administrative_area_level_1'];
        if(state == 'Brittany'){
          state = 'Bretagne';
        }
        jQuery("#_wppl_state").val(state);       
        jQuery("#post_title").val(results[0].formatted_address);
        map.setCenter(latlong);
        marker.setPosition(latlong);
        if(formSubmit == true){
          window.onbeforeunload = null;
          var meetingForm = document.getElementById('meetingForm');
          jQuery('<input type="submit">').hide().appendTo(meetingForm).click().remove();
          return false;
        }
      } else {
        if(status=='ZERO_RESULTS'){        
          if(postalCode != ''){
            customComponentRestrictions = {
              country: country,              
              locality:city,
              postalCode:postalCode
            };
          }else{
            customComponentRestrictions = {
              country: country,              
              locality:city              
            };
          }     
          geocoder.geocode( 
            { componentRestrictions: customComponentRestrictions},
            function(results, status) {    
              if (status == google.maps.GeocoderStatus.OK) {
                latlong = results[0].geometry.location ;
                newlat=latlong.lat();
                newlong=latlong.lng();
                jQuery("#_wppl_lat").val(newlat);
                jQuery("#_wppl_long").val(newlong);
                jQuery("#_wppl_formatted_address").val(results[0].formatted_address);
                var addressComponentTab = [];
                for(var i = 0; i < results[0].address_components.length; i++){
                  var component = results[0].address_components[i];
                  addressComponentTab[component['types'][0]] = component['long_name'];        
                }
                var state = addressComponentTab['administrative_area_level_1'];
                if(state == 'Brittany'){
                  state = 'Bretagne';
                }
                jQuery("#_wppl_state").val(state);       
                jQuery("#post_title").val(results[0].formatted_address);
                map.setCenter(latlong);
                marker.setPosition(latlong);
                if(formSubmit == true){
                  window.onbeforeunload = null;
                  var meetingForm = document.getElementById('meetingForm');
                  jQuery('<input type="submit">').hide().appendTo(meetingForm).click().remove();
                  return false;
                }
              }else{                
                alert("La localisation n'a pu être géocodée, merci de corriger le lieu de rencontre");
              }
          });
        }
      }
    });

    
  }


  function selectModule(htmlObj,moduleTitle){  
    document.getElementById('rencontre_module').value = moduleTitle;
    var moduleDivList = Array.filter( document.getElementsByClassName('moduleChoiceChecked'), function(elem){
                                                                                                   return elem.nodeName == 'DIV';
                                                                                                 });

    for (i = 0; i < moduleDivList.length; i++){
      moduleDivList[i].className = "moduleChoice";
    }
    htmlObj.className ='moduleChoiceChecked';
    if(moduleTitle == ''){
      jQuery('#rencontre_module').val('Autre');
    }
    return true;
  }
  </script>
  <div id="classCodeMeetingSecondary">
    <div id="classCodeMeetingSecondaryHeader">
      <h2>2 | Définir les temps de rencontre</h2>
      Choisir le module qui correspond à ce temps de rencontre ou définir le thème sinon. <br/> <br/>Un pad et un forum permettra au groupe de partager des liens. des ressources des commentaires, faire remonter des avis, etc. <br/> On peut aussi y ajouter une proposition de <a target="_blank" href="https://framadate.org/create_poll.php?type=date">choix collectif de date</a> ou faire participer des personnes à distance <a target="_blank" href="<?php echo get_site_url(); ?>/classcode/accueil/documentation/classcode-la-documentation-participant/comment-utiliser-hangout-dans-le-cadre-de-ce-projet/">par hangout</a> ou avec <a href="https://talky.io">talky</a>.<br/> <br/> <b>À la fin de la rencontre penser à faire un retour <a href="<?php echo cc_mailto_shortcode(array("subject" => "À propos de la rencontre numéro ".$_REQUEST['post_id']."", "body" => "Combien de personnes :&#13;&#10;Avis sur la participation :&#13;&#10;Avis sur la facilitation :&#13;&#10;Commentaires et suggestions :&#13;&#10;merci !")); ?>">à la coordination de Class´Code</a>.</b>

    </div>
    <div id="classCodeMeetingSecondaryDisplay">
      <div id="classCodeMeetingModule">
        <div id="classCodeMeetingModuleHeader">
          Module
        </div>
        <div id="classCodeMeetingModuleDisplay">
<?php
      // Edits the rencontre metatags: generates the proper radio + text input

    
       $checked = false;
    for($ii = 1; $ii <= 5; $ii++) {
      $checked_ii = $postModule == $title_module[$ii];
      $checked = $checked || $checked_ii;
      //echo '<span><input type="radio" name="rencontre_module_n" value="'.$ii.'" onClick="document.getElementById(\'rencontre_module\').value = \''.$title_module[$ii].'\'"'.($checked_ii ? ' checked' : '').'/>'.$ii.'</span>';
      echo '<div class="'.($checked_ii ? 'moduleChoiceChecked' : 'moduleChoice').'" onClick="selectModule(this,\''.$title_module[$ii].'\');">#'.$ii.'</div>';
    }
    echo '<div class="'.($checked ? 'moduleChoice' : ' moduleChoiceChecked').'" onClick="selectModule(this,\'\');">Autre</div>';
  //  echo '<span><input type="radio" name="rencontre_module_n" value="0"'.($checked ? '' : ' checked').'/>'.Autre.'</span>';
    echo ' <input id="rencontre_module" type="text" size="40" name="rencontre_module" value="'.$postModule.'"/>';    

?> 
        </div>
      </div>
       
      <div id="classCodeMeetingTimes">
        <div id="classCodeMeetingTime1" class="classCodeMeetingTime">
          <div class="classCodeMeetingTimeHeader">
            Temps 1
          </div>
          <div class="classCodeMeetingTimeDisplay">
<?php
            echo '<label for="rencontre_date_1">Date<span class="required">*</span></label><input class="classCodeMeetingDateInput calendrier" id="rencontre_date_1" type="text" onchange="seeDate2()" onNothing="cc_checkDate(this.value);" name="rencontre_date_1" value="'.$postDate1.'"/>';
            echo '<label for="rencontre_heure_1">Heure<span class="required">*</span></label><input placeholder="00:00" class="classCodeMeetingTimeInput" id="rencontre_heure_1" type="text" onchange="seeTime2()" onNothing="cc_checkTime(this.value);" name="rencontre_heure_1" value="'.$postTime1.'"/>';
?>    
          </div>
        </div>
        <div id="classCodeMeetingTime2" class="classCodeMeetingTime">
          <div class="classCodeMeetingTimeHeader">
            Temps 2
          </div>
          <div class="classCodeMeetingTimeDisplay">
<?php     
            echo '<label for="rencontre_date_2">Date<span class="required">*</span></label><input class="classCodeMeetingDateInput" id="rencontre_date_2" type="text" onNothing="cc_checkDate(this.value);" name="rencontre_date_2" value="'.$postDate2.'"/>';
            echo '<label for="rencontre_heure_2">Heure<span class="required">*</span></label><input placeholder="00:00" class="classCodeMeetingTimeInput" id="rencontre_heure_2" type="text" onNothing="cc_checkTime(this.value);" name="rencontre_heure_2" value="'.$postTime2.'"/>';
?>  
          </div>
        </div>
      </div>
       

<script type="text/javascript">
// Fonctions de remplissage par défaut de la 2eme date
function seeDate2() {
/*
  if (document.getElementById("rencontre_date_2").value == "") {
    var date = document.getElementById("rencontre_date_1").value;
    date = new Date(date.substr(6), date.substr(3, 2), date.substr(0, 2));
    date = new Date(date.getTime() + 1000 * 3600 * 24 * 14);
    document.getElementById("rencontre_date_2").value = date.getDate()+"/"+date.getMonth()+"/"+date.getFullYear();
  }
*/
}
function seeTime2() {
/*
  if (document.getElementById("rencontre_heure_2").value == "")
    document.getElementById("rencontre_heure_2").value = document.getElementById("rencontre_heure_1").value;
*/
}
// Fonctions de hurlement si les dates/heures ne sont pas au bon format, mais ca cree un conflit avec le calendrier
function cc_checkDate(value) {
  if (!value.match(/\d{2}\/\d{2}\/\d{4}/))
    alert("Le format de date ``"+value+"´´ n'est pas correct: utiliser JJ/MM/YYYY (jour, mois, année), par exemple 01/04/2016 (1er avril 2016)");
}
function cc_checkTime(value) {
  if (!value.match(/\d{2}:\d{2}/))
    alert("Le format d'heure ``"+value+"´´ n'est pas correct: utiliser HH:MM (heures, minutes), par exemple 12:00 (midi)");
}
</script>

<script type="text/javascript">

  jQuery(document).ready(function() {
    jQuery('#rencontre_date_1').datepicker({
        closeText: 'Fermer',
        prevText: 'Précédent',
        nextText: 'Suivant',
        currentText: 'Aujourd\'hui',
        monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
        dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
        dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        weekHeader: 'Sem.',
        dateFormat : 'dd/mm/yy'
    });
    jQuery('#rencontre_date_2').datepicker({
        closeText: 'Fermer',
        prevText: 'Précédent',
        nextText: 'Suivant',
        currentText: 'Aujourd\'hui',
        monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
        dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
        dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        weekHeader: 'Sem.',
        dateFormat : 'dd/mm/yy'
    });
    
  });
  jQuery('#rencontre_heure_1').focus(function(){
    jQuery('#rencontre_heure_1').attr('placeholder', '');
  });
  jQuery('#rencontre_heure_1').focusout(function(){
    jQuery('#rencontre_heure_1').attr('placeholder', '00:00');
  });
  jQuery('#rencontre_heure_2').focus(function(){
    jQuery('#rencontre_heure_2').attr('placeholder', '');
  });
  jQuery('#rencontre_heure_2').focusout(function(){
    jQuery('#rencontre_heure_2').attr('placeholder', '00:00');
  });
</script>
       
    <!--div id="classCodeMeetingResources"> 
<?php
    // Inutile d'afficher le kit predefini dans l edition et le pad de defini en fait automatiquement

        if($kit1_module_display_link !=''){
                 
?>      
        <div class="classCodeMeetingResource">
          <div class="classCodeMeetingResourceHeader">
            Ressources
          </div>
          <div class="classCodeMeetingResourceDisplay">
            <label>Kit Presentiel 1</label>
            <br/>
            <br/>
            <a href="<?php echo $kit1_module_display_link; ?>"><img src="<?php echo get_template_directory_uri()."/_img/classcode_pictos/meeting/Img-rencontre-08.png"; ?>" alt="download" /></a>           
          </div>
        </div>
<?php 

        }
        if($kit2_module_display_link !=''){
?>          
        <div class="classCodeMeetingResource">
          <div class="classCodeMeetingResourceHeader">
            Ressources
          </div>
          <div class="classCodeMeetingResourceDisplay">
            <label>Kit Presentiel 2</label>
            <br/>
            <br/>
            <a href="<?php echo $kit2_module_display_link; ?>"><img src="<?php echo get_template_directory_uri()."/_img/classcode_pictos/meeting/Img-rencontre-08.png"; ?>" alt="download" /></a>
          </div>         
        </div>
<?php
        }
?>
        <div class="classCodeMeetingResource" id="classCodeMeetingPad">
          <div class="classCodeMeetingResourceHeader">
            Ressources
          </div>
          <div class="classCodeMeetingResourceDisplay">
<?php
	   echo '<label for="rencontre_pad">Note pad</label><br/>';
	   echo '<input placeholder="Un lien vers un Note Pad est ajouté ici" id="rencontre_pad" type="hidden" name="rencontre_pad" value="'.$postWebpad.'"/><br/>';
           echo ' <a target="_blank" href="https://framapad.fr">Créer un pad et recopier son lien ici</a>';
?>

          </div>
        </div>
      </div-->
    </div>
  </div>
<?php
  if(isset($_REQUEST['post_id'])){
    echo '<div id="classCodeMeetingFooterMultiple">';
    echo '<div id="classCodeMeetingSubmitFooterMultiple"> ';
    echo '<a onclick="submitMeetingForm();"><img src="'.get_template_directory_uri().'/_img/classcode_pictos/profil/i-modifier.png" alt="Valider la rencontre" />Valider la rencontre</a>';
    echo '<a href="'.get_site_url().'/?p='.$_REQUEST['post_id'].'">Visualiser la rencontre</a>';
    echo '<a onclick="submitMeetingForm(\'duplicate\');">Dupliquer la rencontre</a>';
    echo '<a onclick="submitMeetingForm(\'delete\');">Supprimer la rencontre</a>';
  }else{
    echo '<div id="classCodeMeetingFooter">';
    echo '<div id="classCodeMeetingSubmitFooter"> ';
    echo '<a onclick="submitMeetingForm();">Créer la rencontre</a>';    
  }
?>
      
    </div>
  </div>
  <script type="text/javascript">

    function submitMeetingForm(action){
      if(action == 'duplicate'){
        var meetingForm = document.getElementById('meetingForm');
        var input = document.createElement('input');
        input.type = 'hidden';
        input.name = 'duplicate';
        input.value = 'true';
        meetingForm.appendChild(input);
      }else if(action == 'delete'){
        var meetingForm = document.getElementById('meetingForm');
        var input = document.createElement('input');
        input.type = 'hidden';
        input.name = 'delete';
        input.value = 'true';
        meetingForm.appendChild(input);
      }
      updateGMap(true);              
    }
  </script>
<?php
  if(isset($_REQUEST['post_id'])){
?>
  <div id="classCodeMeetingTierce">
    <div id="classCodeMeetingTierceHeader" class="classCodeMeetingTierceHeader">
      <h2>3 | Gérez la participation</h2>
      Les participants vont s'inscrire à travers l'interface de recherche des rencontres. Ils vont aussi recevoir une alerte si besoin.

<?php // Gestion de la liste des facilitateurs potentiels
      if(rencontre_post_type::has_facilitateur(get_post()->ID))
	echo '<div><br/>Vous avez un facilitateur inscrit pour cette rencontre.</div>';
      {
	$facilitators = rencontre_post_type::get_facilitators_around_a_rencontre(get_post()->ID, 20);
	if (count($facilitators) == 0)
	  $facilitators = rencontre_post_type::get_facilitators_around_a_rencontre(get_post()->ID, 100);
	$count_ok = count($facilitators);
	if ($count_ok > 0)
	  echo '<div style=padding-top:10px;"><b>Voici '.$count_ok.' facilitateur-e'.($count_ok > 1 ? '-s' : '').' à proximité : <table style="padding-left:30px;">'.join(' ' ,array_map(function($user) {
		return '<tr><td><a href="'.$classcodeUrl.'/members/'.$user['login'].'">'.$user['login']
		  .'</a></td><td>à '.round($user['distance']).' km</b></td></tr>';
	      }, $facilitators)).'</table>cliquer sur l´icône <img style="background-color:#266d83;padding:5px;" src="'.$classcodeUrl.'/wp-content/themes/pixees-theme/_img/classcode_pictos/meeting/Img-rencontre-17.png"/> de leur profil pour les contacter</div>';
	else
	  echo '<div style="padding-left:30px;padding-top:5px;">Pas de facilitateur à proximité, désolé !, mais n´hésitez pas à <a href="'.$classcodeUrl.'/classcode/accueil/aide/">nous contacter</a> on peut aider.</div>';
      }
?>
    </div>
    <div id="classCodeMeetingTierceDisplay">
      <div id="classCodeMeetingAttendeeList" class="contentMedia">
<?php 
      echo do_shortcode( '[cc_profile_mini user_id="'.$organizerId.'" color_class="classCodeMiniGreen"]' );
      $participants = get_post_meta($_REQUEST['post_id'], 'rencontre_participants', true);
      $participants_array = array_values(array_filter(explode('|',$participants )));
      foreach($participants_array as $id){
        if ($id != ''){
          $user = get_user_by('id', $id);
          $colorClass = "";
          $argsFacilitateur = array(
            'field' => 'Facilitateur',
            'user_id' => $id
          );
          $facilitateur = bp_get_profile_field_data($argsFacilitateur);
          if(isset($facilitateur[0]) && $facilitateur[0] != ''){
            //$facilitateur = true;
            $colorClass = "classCodeMiniBlue";
          }else{
            //$facilitateur = false;
            $colorClass = "classCodeMiniDarkBlue";
          }          
          echo do_shortcode( '[cc_profile_mini user_id="'.$id.'" color_class="'.$colorClass.'"]' );
        }
      }
        
?>        
      
      </div>
      <!--div id="classCodeMeetingAttendeeCheck">Voir les participants</div-->
      <div id="classCodeMeetingAttendeeProfilDisplay" class="contentDesc">
        
      </div>
    </div>
    <div id="classCodeMeetingTierceFooter">
      <div id="classCodeMeetingTierceActionsFooter" class="contentMedia invitation">
          <input type="text" id="emailInvit" name="emailInvit" placeholder="Mail d'une personne que vous souhaitez inviter dans Class'Code">
           <a onClick="sendClassCodeInvit();"><img src="<?php echo get_template_directory_uri();?>/_img/classcode_pictos/meeting/Img-rencontre-16bis.png"></a>
      </div>
      <div id="classCodeMeetingContactGroupe" class="contentDesc">
	<a href="<?php echo cc_mailto_shortcode(array("who" => "Groupe du temps de rencontre #".get_post()->ID, "to" => rencontre_post_type::get_users_emails(get_post()->ID), "subject" => "À propos de la rencontre #".get_post()->ID, "body" => "Relativement à la rencontre ".get_site_url()."/?p=".get_post()->ID)); ?>">Contacter le Groupe</a>
      </div>
    </div>
  </div>
  <script type="text/javascript">

    function sendClassCodeInvit(){
      var email = encodeURIComponent(jQuery('#emailInvit').val());
      window.open("https://classcode.fr/accueil/aide?cc_mailer_shortcode_subject="+encodeURIComponent("Bienvenue à une rencontre Class´Code")+"&cc_mailer_shortcode_to="+email+"&cc_mailer_shortcode_who="+email+"&cc_mailer_shortcode_body="+encodeURIComponent("Bonjour,&#13;&#10;&#13;&#10;La rencontre <?php echo get_post()->ID?> pourrait vous intéresser, vous pouvez vous inscrire d'un simple clic ici <?php echo get_site_url().'/?p='.get_post()->ID; ?>&#13;&#10;&#13;&#10;Bien Cordialement.")+"#aideAction2");
      }
    
    jQuery('#emailInvit').focus(function(){
      jQuery('#emailInvit').attr('placeholder', '');
    });
    jQuery('#emailInvit').focusout(function(){
      jQuery('#emailInvit').attr('placeholder', "Mail d'une personne que vous souhaitez inviter dans Class'Code");
    });
  </script>
<?php       
  }
?>
  
<?php

echo '</form>';

}else{
?>
  <div id="classCodeMeetingHeader">
  <div id="classCodeMeetingActions" class="contentMedia">
    <h2>Participez à la rencontre <?php if (isset($_REQUEST['post_id'])){ echo '#'.$_REQUEST['post_id']; }?> </h2>
    <a href="#classCodeMeetingMain">
      <div id="classCodeMeetingVisualizeAction1" class="whiteRectangle"><table><td><span class="bigNumber">1</span></td><td>Gérez votre inscription<br/> à la rencontre</td></table></div>
    </a>
    <a href="#classCodeMeetingResourcesConsultation">
      <div id="classCodeMeetingVisualizeAction2" class="whiteRectangle"><table><td><span class="bigNumber">2</span></td><td>Consulter<br/> les ressources liées</td></table></div>
    </a>
    <a href="#classCodeMeetingTierce">
      <div id="classCodeMeetingVisualizeAction3" class="whiteRectangle"><table><td><span class="bigNumber">3</span></td><td>Rencontrez<br/> les participants</td></table></div>
    </a>
  </div>
  <div id="profilDisplay" class="contentDesc"> 
<?php
    echo display_classcode_profile(bp_loggedin_user_id());
?>
  </div>
</div>
<div id="profilFooter">
  <div id="classCodeMeetingActionsFooter" class="contentMedia"></div>
  <div id="profilSubmit" class="contentDesc">
    <a href="<?php echo $profil_url;?>">Modifier mon profil</a>
  </div>
</div>

 <div id="classCodeMeetingMain">
    <div id="classCodeMeetingMainHeader">
      <h2>1 | Gérez votre inscription à la rencontre <?php if (isset($_REQUEST['post_id'])){ echo '#'.$_REQUEST['post_id']; }?> </h2> 
      Si la rencontre correspond à votre besoin, inscrivez vous d'un simple clic.
      <?php
        if(in_array('administrator', wp_get_current_user()->roles)){
      ?>
        <br><br><span style="color:red;">En tant qu'administrateur vous pouvez aussi 
      <?php
        echo '<a href="'.get_site_url().'?post_type=rencontre&p='.$_REQUEST['post_id'].'&edit=true">Modifier la rencontre</a></span>';
        }
      ?>
        
    </div>
    <div id="classCodeMeetingMainDisplay">  
      <div id="classCodeMeetingMap" class="contentMedia"></div>

      <div id="classCodeMeetingDescModule">
        <?php 
          $descTitle = 'Module ';
          if($module_id == 0){
            $descTitle .= 'Autre';
          }else{
            $descTitle .= '#'.$module_id;
          }
          if($postPostalCode != ''){
            $descTitle .= ", ".$postPostalCode;   
          }          
          echo $descTitle;
        ?>
      </div>
      <div id="classCodeMeetingAddressDesc" class="contentDesc">
        <?php echo get_cc_logo_HTML($data_structures, $postStructure); ?>
        <h3><?php echo $postStructure; ?></h3>
        <div id="classCodeMeetingDescAddress"><?php echo $postFormattedAddress; ?></div>
        <h3>Organisateur</h3>
        <div id="classCodeMeetingDescOrg"><?php echo $organizerdisplayName ; ?> </div>
        <div id="classCodeMeetingDescStruc"><?php echo $organizerStructure ; ?> </div>
        <h3><?php echo $postModule; ?></h3>
        <div id="classCodeMeetingDescPrec"><?php echo $postPrecisions ; ?> </div>
        
      </div>
      <div id="classCodeMeetingDescTime">
          <div class="classCodeMeetingDescTime">
            <div class="classCodeMeetingDescTimeTitle">Temps 1</div>
            <div class="classCodeMeetingDescTimecontent">
              <?php echo $postDate1." à ".$postTime1; ?>
            </div>
          </div>
<?php
  if(isset($postDate2) && $postDate2!=""){
?>          
          <div class="classCodeMeetingDescTime">
            <div class="classCodeMeetingDescTimeTitle">Temps 2</div>
            <div class="classCodeMeetingDescTimecontent">
              <?php echo $postDate2." à ".$postTime2; ?>
            </div> 
          </div>           
        </div>
<?php
  }
?>
    </div>
    <div id="classCodeMeetingMainFooter">
      <div id="classCodeMeetingMapFooter" class="contentMedia"></div>
      <div id="classCodeMeetingRegister" class="contentDesc">
      
<?php
    // Encapsulates the interaction in a form
    echo '<form action="'.get_site_url().'/wp-content/plugins/class_code/rencontre/rencontre_register.php" method="post" id="meetingForm">';
      // Protects the save operation
      wp_nonce_field('rencontres_edit_id_nonce', 'rencontres_edit_data_nonce');
      echo "<input type='hidden' value='rencontre' id='post_type' name='post_type'>";   
      // Propagates the post_id
      echo '<input type="hidden" name="post_id" value="'.$_REQUEST['post_id'].'"/>';
      $mode = rencontre_post_type::get_participant($_REQUEST['post_id'], wp_get_current_user()->ID);      
      if(bp_loggedin_user_id()==$organizerId){
        echo '<a href="'.get_site_url().'?post_type=rencontre&p='.$_REQUEST['post_id'].'&edit=true">Modifier la rencontre</a>';
      }else if($mode){
        echo '<input type="hidden" name="meetingUnregister" value="meetingUnregister"/>';
        echo '<a onclick="register();">Se désinscrire</a>';
      }else{
        echo '<input type="hidden" name="meetingRegister" value="meetingRegister"/>';
        echo "<a onclick='register();'>S'inscrire</a>";
      }
    echo '</form>';
?>        
      </div>
    </div>
  
  </div>

      <?php if($mode) echo '<div style="float:right;padding:20px;font-weight:bold;font-size:16px;">Obtenir une <a target="_blank" href="'.get_site_url().'/wp-content/plugins/class_code/attestation/inscription.php?rencontre_id='.$_REQUEST['post_id'].'">attestation d´inscription</a>.</div>'; ?>
  
  <script type="text/javascript">   
    var meetingMap;
    var meetingMarker;
    <?php echo "var markerImage = '".get_template_directory_uri()."/_img/classcode_pictos/meeting/Img-rencontre-07.png';"; ?>
    jQuery( document ).ready(function() {
  <?php
      echo "var latlng = new google.maps.LatLng(".$postLat.", ".$postLong.");";
  ?>  
      loadGmap(latlng);

    });        
    
    function loadGmap(latlng) {
      
<?php
  if($noloc === true){
    echo "var mapZoom = 5";
  }else{
    echo "var mapZoom = 11";
  }
?>
      //objet contenant des propriétés avec des identificateurs prédéfinis dans Google Maps permettant de définir des options d'affichage de notre carte
      var options = {
          center: latlng,
          zoom: mapZoom,
          mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      
      //constructeur de la carte qui prend en paramêtre le conteneur HTML dans lequel la carte doit s'afficher et les options
      meetingMap = new google.maps.Map(document.getElementById("classCodeMeetingMap"), options);

      var meetingContentString = '<div class="meetingMapInfoTitle"><?php echo addslashes($postModule);?></div><?php echo $postStructure ? '<div>'.addslashes($postStructure).'</div>' : '';?><div><?php echo addslashes($postFormattedAddress);?></div>';
      var meetingInfowindow = new google.maps.InfoWindow({
        content: meetingContentString
      });
     
      meetingMarker = new google.maps.Marker({
        map: meetingMap,
        icon: markerImage,
        position: latlng,
        title: 'Temps de rencontre',
      });
      
      meetingMarker.addListener('click', function() {
        meetingInfowindow.open(meetingMap, meetingMarker);
      });
      
    }
    function register(){
      document.getElementById("meetingForm").submit();
    }
  </script>
  <div id="classCodeMeetingResourcesConsultation">
    <div id="classCodeMeetingResourcesConsultationHeader">
      <h2>2 | Consultez les ressources</h2>

      <?php echo '<div style="float:right;padding:20px;font-weight:bold;font-size:16px;"><a href="'.cc_mailto_shortcode(array("who" => "Organisateur-e du temps de rencontre #".get_post()->ID, "to" => get_user_by('ID', get_post()->post_author)->user_email, "subject" => "À propos de la rencontre #".get_post()->ID, "body" => "Relativement à la rencontre ".get_site_url()."/?p=".get_post()->ID)).'">Contacter l´organisateur-e</a>.</div><div style="clear:both;"></div>'; ?>

      Les documents de la rencontre et le pad pour partager entre les participants, poser une question, proposer un retour ou une ressource, donner un avis.
    </div>
<?php
    if(isset($postModule)){
      $existingModuleClass ='';
      if(isset($url_module_link)){
        $existingModuleClass = 'existingModule';
        echo '<a href="'.$url_module_link.'">';
      }    
?>    
    <span  id="classCodeMeetingResourcesConsultationModule" class="<?php echo $existingModuleClass; ?>">  
      <?php echo $postModule; ?></a>
    </span >
<?php
      if(isset($url_module_link)){
        echo '</a>';
      }
    }
?>
    <div id="classCodeMeetingResourcesConsultationDisplay">  
<?php
      $postWebpad = "https://annuel.framapad.org/p/classcode_rencontre_".$_REQUEST['post_id'];

      if(isset($postWebpad) && ($postWebpad!='')){
        echo '<a href="'.$postWebpad.'" target="_blank">';
?>
          <div class="classCodeMeetingResourcesGreenBox">
            <label>Le note pad <br/>de la rencontre</label>
	     <div class="classCodeMeetingResourcesGreenBoxText">Intégré à cette page, pour garder une trace de nos échanges et collaborer.</div>
            <br/>
            <br/>
<?php                
            echo '<span><img src="'.get_template_directory_uri().'/_img/classcode_pictos/meeting/Img-rencontre-27.png" alt="download" /></span>';
?>        
          </div>
        </a>    
<?php
      }
      if($kit1_module_display_link !=''){
?>
        <a href="<?php echo $kit1_module_display_link; ?>">
          <div class="classCodeMeetingResourcesGreenBox">
            <label>Le kit <br/>présentiel #1</label>
            <div class="classCodeMeetingResourcesGreenBoxText">Des indications sur les possibilités d'activités pendant cette rencontre</div>
            <br/>
            <br/>
            <span>
              <img src="<?php echo get_template_directory_uri()."/_img/classcode_pictos/meeting/Img-rencontre-26.png"; ?>" alt="download" />
            </span>  
          </div>
        </a>
<?php
      }
      if($kit2_module_display_link !=''){
?>
      <a href="<?php echo $kit2_module_display_link; ?>">
        <div class="classCodeMeetingResourcesGreenBox">
            <label>Le kit <br/>présentiel #2</label>
            <div class="classCodeMeetingResourcesGreenBoxText">Des indications sur les possibilités d'activités pendant cette rencontre</div>
            <br/>
            <br/>
            <span>
              <img src="<?php echo get_template_directory_uri()."/_img/classcode_pictos/meeting/Img-rencontre-26.png"; ?>" alt="download" />
            </span>  
          </div>
        </a>  
<?php    
      }
?>
    </div>
  </div>

<?php  
    // Displays the forum always defined
    echo '<div id="classCodeEmbededNotepad">';
$postDiscourseForum = DiscourseAPI::get_rencontre_topic(get_post()->ID);
    echo '<center><embed width="900" height="600" src="'.$postDiscourseForum.'"></embed></center><div style="font-weight:bold;font-size:16px;"><a target="_blank" href="'.$postDiscourseForum.'">Ouvrir le forum de la rencontre dans une autre fenêtre.</a></div>';     
    echo '</div>';
?>
  
  <div id="classCodeMeetingTierce">
    <div id="classCodeMeetingTierceHeader" class="classCodeMeetingTierceHeader">
      <h2>3 | Rencontrez les participants</h2>
<div style="margin-right:120px">
      <?php echo '<div style="float:right;padding:20px;font-weight:bold;font-size:16px;"><a href="'.cc_mailto_shortcode(array("who" => "Organisateur-e du temps de rencontre #".get_post()->ID, "to" => get_user_by('ID', get_post()->post_author)->user_email, "subject" => "À propos de la rencontre #".get_post()->ID, "body" => "Relativement à la rencontre ".get_site_url()."/?p=".get_post()->ID)).'">Contacter l´organisateur-e</a>.</div><div style="clear:both;"></div>'; ?>

      Ici on voit et on contacte les participants. L'<span style="color:#54ca90">organisateur-e est dans un cadre vert</span>, la ou le <span style="color:#ff90f3">facilitateur-e en rose</span> et les autres <span style="color:#38aef7;">participant-e-s en bleu</a>.
</div>
    </div>
    <div id="classCodeMeetingTierceDisplay">
      <div id="classCodeMeetingAttendeeList" class="contentMedia">
<?php 
      echo do_shortcode( '[cc_profile_mini user_id="'.$organizerId.'" color_class="classCodeMiniGreen"]' );
      $participants = get_post_meta($_REQUEST['post_id'], 'rencontre_participants', true);
      $participants_array = array_values(array_filter(explode('|',$participants )));
      foreach($participants_array as $id){
        if ($id != ''){
          $user = get_user_by('id', $id);
          $colorClass = "";
          $argsFacilitateur = array(
            'field' => 'Facilitateur',
            'user_id' => $id
          );
          $facilitateur = bp_get_profile_field_data($argsFacilitateur);
          if(isset($facilitateur[0]) && $facilitateur[0] != ''){
            //$facilitateur = true;
            $colorClass = "classCodeMiniPink";
          }else{
            //$facilitateur = false;
            $colorClass = "classCodeMiniBlue";
          }          
          echo do_shortcode( '[cc_profile_mini user_id="'.$id.'" color_class="'.$colorClass.'"]' );
        }
      }
        
?>        
      
      </div>
      <div id="classCodeMeetingAttendeeProfilDisplay" class="contentDesc">

      </div>
    </div>
    <div id="classCodeMeetingTierceFooter">
      <div id="classCodeMeetingTierceActionsFooter" class="contentMedia"></div>
      <div id="classCodeMeetingContactGroupe" class="contentDesc">
	<a href="<?php echo cc_mailto_shortcode(array("who" => "Groupe du temps de rencontre #".get_post()->ID, "to" => rencontre_post_type::get_users_emails(get_post()->ID), "subject" => "À propos de la rencontre #".get_post()->ID, "body" => "Relativement à la rencontre ".get_site_url()."/?p=".get_post()->ID)); ?>">Contacter le Groupe</a>
      </div>
    </div>
  </div>
<?php
 }
?>

<script type="text/javascript">
  jQuery(function() {
    jQuery('.classCodeMiniProfil').click(function() {
      var avatarDiv = this;
      jQuery.ajax({
        type: 'GET',
        url: '<?php echo get_site_url() ?>'+'/wp-content/plugins/class_code/rencontre/rencontre_ajax_profil.php?user_id='+jQuery(avatarDiv).data('user_id')+'&ajax_profile=true',
        timeout: 3000,
        success: function(data) {
          jQuery('#classCodeMeetingAttendeeProfilDisplay').html(data);     
	        
          jQuery("#classCodeMeetingTierceDisplay").css({
            minHeight: jQuery("#classCodeMeetingAttendeeProfilDisplay").height()
          });
          jQuery("#classCodeMeetingAttendeeList").css({
            minHeight: jQuery("#classCodeMeetingAttendeeProfilDisplay").height()+40
          });
          
        },          
        error: function(request) {
          jQuery('#classCodeMeetingAttendeeProfilDisplay').html(
            "erreur ajax");    
        }
      });    
    });  
  });
  
  function loadProfilGmap(latlng, userName, userId, userAddress, userEmail) {
    //objet contenant des propriétés avec des identificateurs prédéfinis dans Google Maps permettant de définir des options d'affichage de notre carte
    var options = {
        center: latlng,
        zoom: 11,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
    //constructeur de la carte qui prend en paramêtre le conteneur HTML dans lequel la carte doit s'afficher et les options
    //  profilMap = new google.maps.Map(document.getElementById("#profil_map_<?php echo $user_id;?>"), options);
    
    var profilmMalsubdiv = document.querySelector("#classCodeMeetingTierce").querySelector("#profil_map_"+userId); 
    profilmMalsubdiv.style.height = '200px';
    profilMap = new google.maps.Map(profilmMalsubdiv, options);

    var profilContentString = '<div>'+userName+'</div><div>'+userAddress+'</div>';
    var profilInfowindow = new google.maps.InfoWindow({
      content: profilContentString
    });
       
    profilMarker = new google.maps.Marker({
      map: profilMap,
      position: latlng,
      title: userName,
    });
    profilMarker.addListener('click', function() {
      profilInfowindow.open(profilMap, profilMarker);
    });
  }
</script>
  