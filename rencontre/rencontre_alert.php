<?php
include_once(plugin_dir_path( __FILE__ ).'./rencontre_post_type.php'); 
include_once(plugin_dir_path( __FILE__ ).'../message/rencontre_messages.php'); 

/**
 * Defines the mechanism to manage user alerts regarding Class'Code hybrid formation hangout.
 * Usage: <pre>rencontre_alert::get_user_alert($module, $nearby); | rencontre_alert::get_user_alerts(); | rencontre_alert::alert_users($post_id);</pre>
 */
class rencontre_alert {
  function __construct() {
    // Loads the alert table as a PHP array
    {
      self::$alerts = get_site_option("ClassCode/rencontre/alerts");
      if (!self::$alerts) self::$alerts = array();
    }
    // Implements the redirection to the alert web service management
    add_filter('request', array($this, 'rencontre_alert_request'), 1, 1);
  }
  function rencontre_alert_request($request) {
    if (isset($_REQUEST['rencontre_alert'])) {
      $user_id = wp_get_current_user()->ID;
      if ($user_id != 0) {
	if (wp_verify_nonce($_REQUEST['rencontre_alert'], 'alert_add_'.$user_id)) {
	  self::add_alert($user_id, $_REQUEST['module'], $_REQUEST['nearby']);
	} else if (wp_verify_nonce($_REQUEST['rencontre_alert'], 'alert_del_'.$user_id)) {
	  self::del_alert($user_id, $_REQUEST['alert_id']);
	} else {
	  echo '<h2>Oh, il y a un souci, vous avez probablement du cliquer deux fois sur le lien du mail. <br/> Retournons sur <a href="'.get_site_url().'/classcode/rencontres/#classCodeMeetingSecondary">la page des rencontres</a> (on vous redirige dans 5 secondes...).</h2><script>setTimeout(function() { window.location = "'.get_site_url().'/classcode/rencontres/#classCodeMeetingSecondary"; }, 5 * 1000);</script>';
	  exit(0);
	}
	header('Location: '.get_site_url().'/classcode/rencontres/#classCodeMeetingSecondary');
	exit(0);
      } else {
	// Redirects towards login
	header('Location: '.wp_login_url(empty($_SERVER["HTTPS"]) ? "http://" : "https://").$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]);
	exit(0);
      }
    } else 
      return $request;
  }
  /** Returns a HTML link with adds an alert when the user click on it.
   * @param $module The module the user wants to be alerted about: 1, 2, 3, 4 or 5.
   * @param $nearby Whether only nearby hangouts are to be considered.
   * @param $text The link text.
   * @return The alert HTML <tt>&lt;a.. class="alertAdd"..</tt> element.
   */
  public static function get_user_alert($module, $nearby = true, $text = 'ajouter une alerte') {
    $user_id = wp_get_current_user()->ID;
    $add_link = get_site_url().'?rencontre_alert='.wp_create_nonce('alert_add_'.$user_id).'&module='.urlencode($module).'&nearby='.($nearby ? 1 : 0);
    return '<a class="alertAdd" href="'.$add_link.'">'.$text.'</a>';
  }
  /** Returns a HTML piece of text with all user alerts and links to cancel them. 
   * @return The alert HTML <tt>&lt;table class="alertList"..</tt> element with an <tt>&lt; div..</tt> if any, else the empty string.
   */
  public static function get_user_alerts() {
    $user_id = wp_get_current_user()->ID;
    $html = '';
    $count = 0;
    if (isset(self::$alerts[$user_id])){
      foreach(self::$alerts[$user_id] as $alert_id => $alert) {
        $del_link = get_site_url().'?rencontre_alert='.wp_create_nonce('alert_del_'.$user_id).'&alert_id='.$alert_id;
        $alerteModule = $alert['module'];
        if($alert['module'] == '' || $alert['module'] == 'all' ){
          $alerteModule = 'Tous';
        }elseif($alert['module'] == 'other'){
          $alerteModule = 'Autre';
        }
        $html .= '<tr><td>'.preg_replace("/\\\\*'/", "´", $alerteModule).'</td><td>'.($alert['nearby'] ? 'près de chez moi' : 'partout').'</td><td><a class="alertDel" href="'.$del_link.'">annuler cette alerte</a></td></tr>';
        $count++;
      }
    }
    return $html == '' ? '' : '<div style="padding-left:30px;padding-top:5px;"><i>Voici '.($count > 1 ? 'vos' : 'votre').' alerte'.($count > 1 ? 's' : '').' en attente :</i><table style="padding-left:20px" class="alertList"><th>Module</th><th>Où ?</th><th></th>'.$html.'</table></div>';
  }
  /** Manages the alerts of a new hangout.  
   * - This is to be called the 1st time a hangout has a location and dates.
   */
  public static function alert_users($post_id) {
    $module = get_post_meta($post_id, 'rencontre_module', true);
    $where = rencontre_post_type::get_location(true, "members", "coordinates");
    $nearby = $where && rencontre_post_type::get_location_distance(rencontre_post_type::get_location($post_id, "posts", "coordinates"), $where) <= 20;
    self::alert_users_2($post_id, $module, $nearby);
  }
  public static function alert_users_2($post_id, $module, $nearby) {
    global $title_module;
    $other_module = true; foreach($title_module as $title) if ($module == $title) $other_module = false;
    foreach(self::$alerts as $user_id => $useralerts)
      foreach($useralerts as $alert_id => $alert)
      if ((($alert['module'] == $module) || ($alert['module'] == 'all') || ($alert['module'] == 'other' && $other_module)) && 
	  ((!$alert['nearby']) || $alert['nearby'] == $nearby)) {
	  self::mail_alert($post_id, $module, $nearby);
	  self::del_alert($user_id, $alert_id);
	}
  }
  // Mails to a user regarding her/his alert
  private static function mail_alert($post_id, $module, $nearby) {
    $message = '<p>Vous recevez ce courriel car vous avez demandé une alerte pour un temps de rencontre de Class´Code, pour le module «<i>'.$module.'</i>», '.($nearby ? 'à proximité de chez vous' : 'partout').'.</p>
<p>La rencontre <a href="https://pixees.fr/rencontre/'.$post_id.'">'.$post_id.'</a> vient d´être crée si elle vous intéresse n´hésitez pas à vous inscrire en allant sur la page <a href="https://pixees.fr/rencontre/'.$post_id.'/#classCodeMeetingMain">ICI</a>.</p>
<p>Sinon cliquer '.self::get_user_alert($module, $nearby, 'LA').' pour renouveller votre alerte.</p>';
    rencontre_messages::send_message(wp_get_current_user()->ID, "Votre alerte à propos d'une rencontre de Class´Code", $message);
    //mail(wp_get_current_user()->user_email, "Class´Code: le point sur votre activité",  'Bonjour,'.$message.'Bien Cordialement.</div>', "From: classcode-accueil@inria.fr\r\nContent-type: text/html; charset=utf-8\r\nContent-Transfer-Encoding: 8bit\r\n");
  }
  // Adds an alert
  private static function add_alert($user_id, $module, $nearby) {
    if (!isset(self::$alerts[$user_id]))
      self::$alerts[$user_id] = array();
    self::$alerts[$user_id][] = array('module' => $module, 'nearby' => $nearby);
    update_site_option("ClassCode/rencontre/alerts", self::$alerts);
  }
  // Deletes an alert
  private static function del_alert($user_id, $alert_id) {
    if (isset(self::$alerts[$user_id]) && isset(self::$alerts[$user_id][$alert_id]))
      unset(self::$alerts[$user_id][$alert_id]);
    update_site_option("ClassCode/rencontre/alerts", self::$alerts);
  }
  // Defines the alert table as:
  // self::$alerts == array(user_id => array(array('module' => .., 'nearby' => ..), ..));
  public static $alerts;
}
new rencontre_alert();

/* Returns a piece of HTML to test the routine @todo a virer qd validé

function test_rencontre_alert() {
  echo "<div style='background-color:#e8e'><hr>";
  echo "<pre>".print_r(rencontre_alert::$alerts, true)."</pre>";
  $module = array(1 => '#1 Module fondamental : découvrez la programmation créative',
		  2 => '#2 Module thématique : manipulez l’information');
  foreach(array(1, 2) as $imodule)
    foreach(array(0, 1) as $nearby)
      echo "<div>Module ".$module[$imodule].", nearby = $nearby : ".rencontre_alert::get_user_alert($module[$imodule], $nearby)."</div>";
  if (isset($_REQUEST['the_m']) && isset($_REQUEST['the_m'])) {
    echo "<br/><div>Sending alerts for ".$module[$_REQUEST['the_m']]." nearby = ".$_REQUEST['the_n']."</div>";
    rencontre_alert::alert_users_2("007", $module[$_REQUEST['the_m']], $_REQUEST['the_n']);
  }
  $url = (empty($_SERVER["HTTPS"]) ? "http://" : "https://").$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
  foreach(array(1, 2) as $imodule)
    foreach(array(0, 1) as $nearby)
       echo "<div>Send alert for imodule = $imodule, nearby = $nearby <a href='$url&the_m=$imodule&the_n=$nearby'>ICI</a></div>";
  echo rencontre_alert::get_user_alerts();
  echo "<hr></div>";
}
*/

?>