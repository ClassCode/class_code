<?php // Implements the edition or display of a rencontre post

// Redirection if the user is not connected
if (!is_user_logged_in()) {
  echo "<div id='guidedTour'><h2>Vous devez &ecirc;tre connect&eacute; pour acc&eacute;der &agrave; cette page !</h2>";
  echo "<h2><a href='".get_site_url()."/classcode/accueil/'' >Retour &agrave; l'accueil</h2></a></div>";
  //header('Location: '.get_site_url().'/classcode/accueil/');
  return;
}

global $wpdb;
$visitorId = bp_loggedin_user_id();  

$visitorLocation = $wpdb->get_row($wpdb->prepare( "SELECT * FROM wppl_friends_locator WHERE member_id = %s", $visitorId ) );
$visitorLat = $visitorLocation->lat;
$visitorLong = $visitorLocation->long;
$mapZoom = '11';
if($visitorLat =='' || $visitorLong == ''){
  echo "<div id='guidedTour'><h2>Vous devez avoir une localisation de d&eacute;finie dans votre profil pour acc&eacute;der &agrave; cette page !</h2>";
  echo "<h2><a href='".get_site_url()."/classcode/accueil/'' >Retour &agrave; l'accueil</h2></a></div>";
  return;
}else{
  $visitorLatLong = strval($visitorLat).",".strval($visitorLong);
  $nearby = '20';
  if(isset($_POST['where']) && $_POST['where'] != '' ){
    $nearby = $_POST['where'];
  }
}
$visitorDisplayName = getDisplayName($visitorId);


?>
<div id="classCodeMeetingMain">
  <div id="classCodeMeetingMainHeader">
    <h2>Voir Class'Code pr&egrave;s de chez moi</h2>
 
  </div>
  <div id="classCodeMeetingMainDisplay">
    <div id="classCodeMeetingMap" class="contentMedia"></div>
    <div id="classCodeMeetingSearchDesc" class="contentDesc">
      <form method="post" id="userSearchForm" action="" class="auto_submit_form">
        <h3>Localisation</h3>
        Autour de chez moi : 
        <?php 
          $checked = '';
          if(isset($nearby) && ($nearby == '20')){
            $checked = 'checked';
          } 
        ?>   
          <input type="radio" name="where" value="20" <?php echo $checked;?>> 20 km 
        <?php 
          $checked = '';
          if(isset($nearby) && ($nearby == '100')){
            $checked = 'checked';
            
          } 
        ?>   
          <input type="radio" name="where" value="100" <?php echo $checked;?>> 100 km 
        <?php 
          $checked = '';
          if(isset($nearby) && ($nearby == '200')){
            $checked = 'checked';
          } 
        ?>   
          <input type="radio" name="where" value="200" <?php echo $checked;?>> 200 km 
        <?php 
          $checked = '';
          if(isset($nearby) && ($nearby == '2000')){
            $checked = 'checked';
          } 
        ?>   
          <input type="radio" name="where" value="2000" <?php echo $checked;?>> 2000 km 
          <br/>
      </form>
    </div>
  </div>
</div>
      
<div id="classCodeMeetingTierce">
  <div id="classCodeMeetingTierceHeader" class="classCodeMeetingTierceHeader">
    <h2>Contacter Class'Code autour de moi</h2>
  </div>
  <div id="classCodePixeesTierceDisplay">
    <div id="classCodePixeesList" class="contentMedia">
<?php 
      echo do_shortcode( '[cc_profile_mini user_id="'.$organizerId.'" color_class="classCodeMiniGreen"]' );
      $pixees_array_id = get_users_next_to(bp_loggedin_user_id(), intval($nearby));
      $emails_array = array();
     // var_dump($pixees_array_id);
      foreach($pixees_array_id as $id){
        if ($id != ''){
          $user = get_user_by('id', $id);
          $emails_array[]=$user->user_email;
          $colorClass = "";
          $argsFacilitateur = array(
            'field' => 'Facilitateur',
            'user_id' => $id
          );
          $facilitateur = bp_get_profile_field_data($argsFacilitateur);
          if(isset($facilitateur[0]) && $facilitateur[0] != ''){
            //$facilitateur = true;
            $colorClass = "classCodeMiniPink";
          }else{
            //$facilitateur = false;
            $colorClass = "classCodeMiniBlue";
          }          
          echo do_shortcode( '[cc_profile_mini user_id="'.$id.'" color_class="'.$colorClass.'"]' );
        }
      }
      
      $emails_string = implode(",", $emails_array);
        
?>        
      
    </div>  
    <div id="classCodeMeetingAttendeeProfilDisplay" class="contentDesc">
    </div>
  </div>
  <div id="classCodeMeetingTierceFooter">
    <div id="classCodeMeetingTierceActionsFooter" class="contentMedia"></div>
    
  </div>
</div>

<?php

function get_users_next_to($user_id,$distArg){
  $user_where =  rencontre_post_type::get_location($user_id, "members", "coordinates");
  $distance = isset($distArg) ? $distArg : 20;
  $pixees_near = array();
  
  global $wpdb;
  $users = $wpdb->get_results( "SELECT distinct user_id FROM $wpdb->usermeta WHERE TRUE" );  
  
  foreach($users as $user){
     $nearby = rencontre_post_type::get_location_distance(rencontre_post_type::get_location($user->user_id, "members", "coordinates"), $user_where) <= $distance ;
     $current_id = $user->user_id;
     if($nearby and $current_id != $user_id){
       $pixees_near[]=$current_id;
     }
  }
  return $pixees_near;
  
}
?>

<script type="text/javascript">
  jQuery(function() {
    jQuery('.classCodeMiniProfil').click(function() {
      var avatarDiv = this;
      jQuery.ajax({
        type: 'GET',
        url: '<?php echo get_site_url() ?>'+'/wp-content/plugins/class_code/rencontre/rencontre_ajax_profil.php?user_id='+jQuery(avatarDiv).data('user_id')+'&ajax_profile=true',
        timeout: 3000,
        success: function(data) {
          jQuery('#classCodeMeetingAttendeeProfilDisplay').html(data);     
	        
          jQuery("#classCodePixeesTierceDisplay").css({
            minHeight: jQuery("#classCodeMeetingAttendeeProfilDisplay").height()
          });
          jQuery("#classCodePixeesList").css({
            minHeight: jQuery("#classCodeMeetingAttendeeProfilDisplay").height()+40
          });
          
        },          
        error: function(request) {
          jQuery('#classCodeMeetingAttendeeProfilDisplay').html(
            "erreur ajax");    
        }
      });    
    }); 
    jQuery(".auto_submit_form").change(function() {
      this.submit();
    });
  });
  
  function loadProfilGmap(latlng, userName, userId, userAddress, userEmail) {
    //objet contenant des propri�t�s avec des identificateurs pr�d�finis dans Google Maps permettant de d�finir des options d'affichage de notre carte
    var options = {
        center: latlng,
        zoom: 11,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
    //constructeur de la carte qui prend en param�tre le conteneur HTML dans lequel la carte doit s'afficher et les options
    //  profilMap = new google.maps.Map(document.getElementById("#profil_map_<?php echo $user_id;?>"), options);
    
    var profilmMalsubdiv = document.querySelector("#classCodeMeetingTierce").querySelector("#profil_map_"+userId); 
    profilmMalsubdiv.style.height = '200px';
    profilMap = new google.maps.Map(profilmMalsubdiv, options);

    var profilContentString = '<div>'+userName+'</div><div>'+userAddress+'</div>';
    var profilInfowindow = new google.maps.InfoWindow({
      content: profilContentString
    });
       
    profilMarker = new google.maps.Marker({
      map: profilMap,
      position: latlng,
      title: userName,
    });
    profilMarker.addListener('click', function() {
      profilInfowindow.open(profilMap, profilMarker);
    });
  }
  
   var geocoder;
  var map;
  var marker;
  
  var meetingMarkerImage = '<?php echo get_template_directory_uri(); ?>/_img/classcode_pictos/meeting/Img-rencontre-07.png';
  var visitorMarkerImage = '<?php echo get_template_directory_uri(); ?>/_img/classcode_pictos/meeting/Img-rencontre-07-2.png';

  jQuery( document ).ready(function() {
    var latlng = new google.maps.LatLng(<?php echo $visitorLatLong;?>);
    loadGmap(latlng);
  });


  function loadGmap(latlng) {
    
    var options = {center: latlng,zoom: <?php echo $mapZoom;?>,mapTypeId: google.maps.MapTypeId.ROADMAP};
    map = new google.maps.Map(document.getElementById("classCodeMeetingMap"), options);
    var oms = new OverlappingMarkerSpiderfier(map);
    geocoder = new google.maps.Geocoder();
    var infowindow = new google.maps.InfoWindow();
    
    var visitorDisplayName ='<div class=\"meetingMapInfoTitle\"><?php echo $visitorDisplayName;?></div>';
    marker = new google.maps.Marker({map: map,icon: visitorMarkerImage,position: latlng,title: '<?php echo $visitorDisplayName;?>',});
    marker.desc=visitorDisplayName;
    oms.addMarker(marker); 
    var pixeesArrayMarker = [];
    var pixeesContentString = [];

         
    <?php
    $pixees_inc = 0;
    foreach($pixees_array_id as $id){
      $pixeeLocation = $wpdb->get_row($wpdb->prepare( "SELECT * FROM wppl_friends_locator WHERE member_id = %s", $id ) );
      $pixeeLat = $pixeeLocation->lat;
      $pixeeLong = $pixeeLocation->long;
      $pixeeLatLong = strval($pixeeLat).",".strval($pixeeLong);
      $pixeeDisplayName = getDisplayName($id);
?>
      var pixeelatlng = new google.maps.LatLng(<?php echo $pixeeLatLong;?>);
      pixeesContentString[<?php echo $pixees_inc;?>] ='<div class=\"meetingMapInfoTitle\"><?php echo $pixeeDisplayName;?></div>';
      pixeesArrayMarker[<?php echo $pixees_inc;?>] = new google.maps.Marker({map: map,icon: meetingMarkerImage,position: pixeelatlng,title: '<?php echo $pixeeDisplayName;?>',});
      pixeesArrayMarker[<?php echo $pixees_inc;?>].desc=pixeesContentString[<?php echo $pixees_inc;?>];
      oms.addMarker(pixeesArrayMarker[<?php echo $pixees_inc;?>]); 
      
<?php   
      $pixees_inc++;
    }
    ?> 
  
      
    oms.addListener('click',function(marker, event) {
      infowindow.setContent(marker.desc);
      infowindow.open(map, marker);
    });
          
    oms.addListener('spiderfy', function(meetingArrayMarker) {
      infowindow.close();
    });

  }

</script>
  