<?php
include_once(plugin_dir_path( __FILE__ ).'../metabox/basic_meta_box.php');
include_once(plugin_dir_path( __FILE__ ).'../metabox/text_meta_box.php');
include_once(plugin_dir_path( __FILE__ ).'../message/rencontre_messages.php');
/**
 * Defines a new post type called rencontre to manage Class'Code hybrid formation hangout.
 * \ingroup custom_post_type
 * \extends custom_post_type
 */
class rencontre_post_type {
  function __construct() {
    add_action('init', function() {
      register_post_type('rencontre',
        array(
          'labels' => array(
            'name' => 'Rencontre de la formation hybride',
            'singular_name' => 'Rencontre de la formation hybride',
            ),
          'description' => 'Ce contenu défini une rencontre de la formation hybride de Class´Code',
          'public'               => true,
          'exclude_from_search'  => true,
          'show_in_menu'         => false,
          'supports'             => array('author', 'revisions'),
          'taxonomies'           => array(),
          'can_export'           => true,
	  'capabilities' => array('edit_post' => 'edit_rencontre'),
          'register_meta_box_cb' => function() {
            // Removes a spurious meta-box
            remove_meta_box('slugdiv', 'rencontre', 'normal');
          }
        )
      );
      get_role('subscriber')->add_cap('edit_rencontre');
      get_role('editor')->add_cap('edit_rencontre');
      get_role('administrator')->add_cap('edit_rencontre');
      // Ref: https://codex.wordpress.org/Function_Reference/register_post_type
    });
    // Adds the post edition meta-boxes
    if (is_admin()) {
      // Lieu de rencontre : mystère complet de comment on y accède mais il semble defini pour tous les posts
      // A voir avec geo*

      // Organisateur de la rencontre: c'est l'auteur initial du post 
      // Ref: get_post()->post_author pour le récupérer

      // Autres méta-données spécifiques
      {
        new basic_meta_box('structure', 
			   array(
				 'title' => 'Etablissement',
				 'post_type' => 'rencontre',
				 ));
        new basic_meta_box('capacity', 
			   array(
				 'title' => "Capacité d'accueil",
				 'post_type' => 'rencontre',
				 ));
        new basic_meta_box('precisions', 
			   array(
				 'title' => 'Précisions éventuelles',
				 'post_type' => 'rencontre',
				 ));
	      new basic_meta_box('rencontre_module', 
			   array(
				 'title' => 'Module: 1 à 5 ou si autre intitulé',
				 'post_type' => 'rencontre',
				 ));
	      new basic_meta_box('rencontre_date_1', 
			   array(
				 'title' => 'Date au format : YYYY-MM-DD',
				 'post_type' => 'rencontre',
				 ));
        new basic_meta_box('rencontre_heure_1',
         array(
				 'title' => 'Heure au format : HH:MM',
				 'post_type' => 'rencontre',
				 ));
	      new basic_meta_box('rencontre_date_2', 
			   array(
				 'title' => 'Date au format : YYYY-MM-DD',
				 'post_type' => 'rencontre',
				 ));
        new basic_meta_box('rencontre_heure_2',
         array(
				 'title' => 'Heure au format : HH:MM',
				 'post_type' => 'rencontre',
				 ));
        new basic_meta_box('rencontre_participants', 
			   array(
				 'title' => 'Liste participants au format : |id_1|id_2|... id_N|',
				 'post_type' => 'rencontre',
				 ));
        new basic_meta_box('rencontre_pad', 
			   array(
				 'title' => 'Le pad de la rencontre',
				 'post_type' => 'rencontre',
				 ));
         new basic_meta_box('rencontre_hangout', 
			   array(
				 'title' => 'Le pad Hangout de la rencontre',
				 'post_type' => 'rencontre',
				 ));
         new basic_meta_box('sujet_precisions', 
			   array(
				 'title' => 'Précision sur le sujet de la rencontre',
				 'post_type' => 'rencontre',
				 ));
      }
      // Suppresses the title and status field dislpaly
      add_action('admin_footer-edit.php', function() {
	      if($_REQUEST['post_type'] == 'rencontre') {
	        echo "<script type='text/javascript'>
                jQuery(document).ready(function($) {
                  $('.row-title').remove();
                  $('.post-state').remove();
                });
              </script>";
        }
      });
    }
    add_shortcode("cc_rencontres_table", array($this, "cc_rencontres_table_shortcode"));
  }
  /** Creates or duplicates a new empty rencontre.
   * @param $default The source rencontre ID in case of rencontre duplication.
   * @return The rencontre post ID or 0 if it fails
   */
  public static function create_rencontre($default = false) {
    // Ref: https://developer.wordpress.org/reference/functions/wp_insert_post
    $post_id = 
      wp_insert_post(array(
			   'post_type' => 'rencontre',
         'post_status' => 'publish',
			   'comment_status' => 'open',
			   'ping_status' => 'open'));
    if ($post_id == 0 || is_wp_error($post_id)) {
      return 0;
    } else if ($default) {
      foreach(array('structure', 'capacity', 'precisions', 'rencontre_module', 'rencontre_date_1', 'rencontre_heure_1', 'rencontre_date_2', 'rencontre_heure_2', 'rencontre_participants', 'rencontre_hangout', 'sujet_precisions') as $name){
        update_post_meta($post_id, $name, get_post_meta($default, $name, true));
      }
      // Duplicates the location
      global $wpdb; 
      $postLocation = $wpdb->get_row($wpdb->prepare("SELECT * FROM wp_places_locator WHERE post_id = %d", $default));
      $wpdb->insert( 'wp_places_locator', 
          array( 
            'post_id' => $post_id, 
            'feature' => $postLocation->feature,
            'post_status' => $postLocation->post_status,
            'post_type' => $postLocation->post_type,
            'post_title' => $postLocation->post_title,
            'lat' => $postLocation->lat,
            'long' => $postLocation->long,
            'street_number' => $postLocation->street_number,
            'street_name' => $postLocation->street_name,
            'street' => $postLocation->street,
            'apt' => $postLocation->apt,
            'city' => $postLocation->city,
            'state' => $postLocation->state,
            'state_long' => $postLocation->state_long,
            'zipcode' => $postLocation->zipcode,
            'country' => $postLocation->country,
            'country_long' => $postLocation->country_long,
            'address' => $postLocation->address,
            'formatted_address' => $postLocation->formatted_address,
            'phone' => $postLocation->phone,
            'fax' => $postLocation->fax,
            'email' => $postLocation->email,
            'website' => $postLocation->website,
            'map_icon' => $postLocation->map_icon          
          ));     
    } else {
      update_post_meta($post_id, 'rencontre_participants', '|');
    }
    rencontre_messages::send_creation_message($post_id);
    return $post_id;
  }
  /** Returns all rencontres.
   * @param $what An array of parameters to filter the rencontres to be returned:
   * - "mes_rencontres" => "false", if true only select the current user rencontres (as participants), true to obtain current ID rencontres, a number to select a given user ID.
   * - "mes_propres_rencontres" => "false", if true only select the current user rencontres (as author), true to obtain current ID rencontres, a number to select a given user ID.
   * - "user_id" => The user ID, default is the current user ID.
   * - "structure" => "false", if not false, only select the related structure rencontres,
   * - "module" => "false", if not false ("1", "2", "3", "4", "5", "O" for others) only select the related module rencontres,
   * - "facilitateur" => "false", if not false ("yes" or "no") only select rencontres with or without a facilitator.
   * - "when" => "false", if not false ("past", "future") only select the passed or incoming rencontres,
   * - "nearby" => "false", if not false (true or a radius in kilometer (default is 20 kilometers) only select nearby rencontres,
   * - "where" => "false", if not false define the current location as an <tt>array(lattitude, longitude)</tt>.
   * @param $sort A string defining the result sort:
   * - 'date' Sorted by date,
   *   - from the most recent to the oldest, but, 
   *   - from the closest to the most distant in the future, if requiring only future rencontres,
   * - 'distance' Sorted by distance from the closest to the farthest.
   * @return An array with all rencontres with following format <pre>array(array(
   *   "post" => $post_of_the_rencontre, 
   *   "past" => true_or_false, 
   *   "future" => true_or_false, 
   *   "module" => title, 
   *   "module-index" => index, // 1,2,3,4,5 for the module, 0 for other meetings
   *   "facilitateur" => true_or_false, // If there is a facilitateur 
   *   "nearby" => true_or_false, // If it is nearby
   *   "mine" => true_or_false, // If the user is author
   * ));</pre>
   */
  public static function get_rencontres($what = array(), $sort = 'date') {
    $user_id = isset($what['user_id']) ? $what['user_id'] : wp_get_current_user()->ID;
    global $title_module ; // recupération de la liste des modules pour les comparer en cas de choix "other" et calculer l index
    // Ref: https://codex.wordpress.org/Template_Tags/get_posts
    $rencontres = array_map(function($result) { return array("post" => $result); }, get_posts(array(
      'posts_per_page'   => -1,
      'offset'           => 0,
      'category'         => '',
      'category_name'    => '',
      'orderby'          => 'date',
      'order'            => isset($what['when']) && $what['when'] == 'future' ? 'ASC' : 'DESC',
      'meta_key'         => '',
      'meta_value'       => '',
      'post_type'        => 'rencontre',
      'post_parent'      => '',
      'author'	   => isset($what['mes_propres_rencontres']) ? ($what['mes_propres_rencontres'] === true ? $user_id : $what['mes_propres_rencontres']) : '',
      'post_status'      => '',
      'suppress_filters' => true,
    )));
    $where = isset($what['where']) ? $what['where'] : self::get_location($user_id, "members", "coordinates");
    $nearby = isset($what['nearby']) ? ($what['nearby'] === true ? 20 : $what['nearby']) : 20;
    foreach($rencontres as $index => $rencontre) {
      $id = $rencontre["post"]->ID;
      $today =  new DateTime('NOW');
      $date1 = get_post_meta($id, 'rencontre_date_1', true);
      $date2 = get_post_meta($id, 'rencontre_date_2', true);
      if($date1 != '')
	$date1 = DateTime::createFromFormat('d/m/Y', $date1);
      if($date2 != '')
	$date2 = DateTime::createFromFormat('d/m/Y', $date2);
      // Manages past and future rencontres
      {
	if (($date1 != '') && ($date2 != '') && ($date1 > $date2)) {
	  $date = $date1; $date1 = $date2; $date2 = $date;
	}
	if (($date1 != '') && ($date2 == ''))
	  $date2 = $date1;
	if (($date1 == '') && ($date2 != ''))
	  $date1 = $date2;
	$rencontres[$index]["past"] = !($date1 != '' && $date1 > $today);
	$rencontres[$index]["future"] = !($date2 != '' && $date2 < $today);
      }
      $rencontres[$index]["module"] = get_post_meta($id, 'rencontre_module', true);
      if (preg_match("/^[^#]*#([0-9]).*$/", $rencontres[$index]["module"]))
	$rencontres[$index]["module_id"] = preg_replace("/^[^#]*#([0-9]).*$/", "$1", $rencontres[$index]["module"]);
      else
	$rencontres[$index]["module_id"] = "0";
      $rencontres[$index]["facilitateur"] = self::has_facilitateur($id);
      $rencontres[$index]["nearby"] = self::get_location_distance(self::get_location($id, "posts", "coordinates"), $where) <= $nearby ;
      $rencontres[$index]["mine"] = $user_id == $rencontre["post"]->post_author;
      $strucutre_min_sans_accent = suppr_accents(get_post_meta($id, 'structure', true));
      if(isset($what['structure'])){
        $structure_what_min_sans_accent = suppr_accents($what['structure']);  
      }else{
        $structure_what_min_sans_accent = "";
      }
      
      if((isset($what['structure']) && !strstr(strtolower($strucutre_min_sans_accent),strtolower($structure_what_min_sans_accent))) ||
	       (isset($what['module']) && ($what['module'] != get_post_meta($id, 'rencontre_module', true)) && ($what['module'] != 'other') && ($what['module'] != 'all'))||
	       (isset($what['facilitateur']) && ((($what['facilitateur'] =='yes') && !$rencontres[$index]["facilitateur"]) || 
                                           (($what['facilitateur'] =='no') && $rencontres[$index]["facilitateur"] ))) ||
	       (isset($what['when']) && (($what['when'] == 'past' && !$rencontres[$index]["past"]) || ($what['when'] == 'future' && !$rencontres[$index]["future"]))) ||	       
         (isset($what['nearby']) && !$rencontres[$index]["nearby"])) {
        unset($rencontres[$index]);
      }
      //test sur le module "other"
      if(isset($what['module']) && $what['module'] == 'other'){
        foreach($title_module as $title){
          if($rencontre["module"] == $title){
            unset($rencontres[$index]);
          }
        }
      }
      //test sur "mes rencontres" 
      if(isset($what['mes_rencontres']) && $what['mes_rencontres']!= ""){
        $mes_rencontres_id = $what['mes_rencontres'] === true ? $user_id : $what['mes_rencontres'];
        if($rencontre["post"]->post_author != $mes_rencontres_id){                  
          $participants = explode('|', get_post_meta($id, 'rencontre_participants', true));
          $participation = false;
          foreach($participants as $participant_id){
            if ($participant_id == $mes_rencontres_id){
              $participation = true;
            }
          }
          if(!$participation){
            unset($rencontres[$index]);
          }
        }
      }
    }
    if ($sort == 'distance') {
      usort($rencontres, function($r1, $r2) {
	      return 
          self::get_location_distance(self::get_location($r1["post"]->ID, "posts", "coordinates"), $where) -
          self::get_location_distance(self::get_location($r2["post"]->ID, "posts", "coordinates"), $where);
      });
    }
    return $rencontres;
  }
  /* Defines a shortcode that displays all rencontres using get_rencontres($params). */
  function cc_rencontres_table_shortcode($atts, $content = "") {
    ob_start();
    include_once("rencontre_search.php");    
    return ob_get_clean();
  }
  /** Register or unregister a participant.
   * @param $post_id The rencontre post ID.
   * @param $user_id The user ID.
   * @param $register If true register, else unregister.
   */
  public static function set_participant($post_id, $user_id, $register = true) {
    $users_id = array();
    foreach(explode('|', get_post_meta($post_id, 'rencontre_participants', true)) as $id)
      if ($id != '' && $id != $user_id)
	$users_id[] = $id;
    if ($register)
      $users_id[] = $user_id;
    update_post_meta($post_id, 'rencontre_participants', '|'.implode('|', $users_id).'|');
    if ($register)
      rencontre_messages::send_inscription_message($post_id, $user_id);
  }
  /** Checks if a participant is registered.
   * @param $post_id The rencontre post ID.
   * @param $user_id The user ID.
   * @return True if the participant is registered, false otherwise.
   */
  public static function get_participant($post_id, $user_id) {
    return strstr(get_post_meta($post_id, 'rencontre_participants', true), '|'.$user_id.'|');
  }
  /** Checks if a given rencontre has a facilitateur. */
  public static function has_facilitateur($post_id) {
    foreach(explode('|', get_post_meta($post_id, 'rencontre_participants', true)) as $id)
      if ($id != '' && bp_get_profile_field_data(array('field' => 'Facilitateur', 'user_id' => $id)))
	return true;
  }
  /** Echoes all participant emails. */
  public static function get_users_emails($post_id) {
    $emails = array(get_user_by('id', get_post($post_id)->post_author)->user_email);
    foreach(explode('|', get_post_meta($post_id, 'rencontre_participants', true)) as $id)
      if ($id != '')
	$emails[] = get_user_by('id', $id)->user_email;
    return implode(",", $emails);
  }
  /** Gets the participants around a rencontre. @todo : à finir et valider
   * @param $post_id The post ID.
   * @param $options An array with all desired options of the [gmw_nearby_locations] shortcode.
   */
  public static function get_users_around_a_rencontre($post_id, $options) {
    return self::gmw_nearby_location(wp_parse_args($options, array("item_type" => "members", "nearby" => self::get_location($post_id, "posts", "coordinates"))));
  }
  /** Gets the participants around a rencontre.
   * @param $post_id The post ID.
   * @param $radius The maximal distance to the post.
   * @return A piece of HTML with a table of the facilitators.
   */
  public static function get_facilitators_around_a_rencontre($post_id, $radius) {
    // Ici a remplacer par une requete SQL-join de deux tables si souci de performance
    // Gets all facilitators
    $users = array_filter(get_users(), function($user) {
	return bp_get_profile_field_data(array('field' => 'Facilitateur', 'user_id' => $user->ID));
      });
    $post_location = self::get_location($post_id, "posts", "coordinates");
    $users = array_map(function($user) use ($post_location) { 
	return array("login" => $user->user_login,
		     "distance" => self::get_location_distance($post_location, self::get_location($user->ID, "members", "coordinates")),
		     ); }, $users);
    $users = array_filter($users, function($user) use ($radius) { return $user['distance'] < $radius; });
    usort($users, function($u1, $u2) { return $u1['distance'] - $u2['distance']; });
    return $users;
  }
  /** Gets the rencontres around a participant. @todo : à finir et valider
   * - Does not work because of bug in [gmw_nearby_locations ..] shortcode.
   * @param $user_id The user ID.
   * @param $options An array with all desired options of the [gmw_nearby_locations] shortcode.
   */
  public static function get_rencontres_around_a_user($user_id, $options) {
    return self::gmw_nearby_location(wp_parse_args($options, array("post_types" => "rencontres", "item_type" => "posts", "nearby" => self::get_location($user_id, "members", "coordinates"))));
  }
  // Returns the gmw_nearby_locations shortcode value
  // @see http://docs.geomywp.com/nearby-locations-shortcode/
  private static function gmw_nearby_location($options) {
    // Here are all predefined options for the present usage
    $options = wp_parse_args($options, 
			     array("units" => "kilometers", 
				   "radius" => "1000", 
				   "orderby" => "distance", 
				   "show_map" => "false", 
				   "show_locations_list" => "true", 
				   "show_image" => "false", 
				   "show_distance" => "false", 
				   "address_fields" => "address",
				   "get_directions" => 0,
				   "no_results_message" => "",
				   ));
    $shortcode = '[gmw_nearby_locations '.
      join(' ', array_map(function($name, $value) { 
	    if ($name == "nearby" && is_array($value))
	      return $name.'="'.$value[0].', '.$value[1].'"'; 
	    else
	      return $name.'="'.$value.'"'; 
	  },array_keys($options), array_values($options))).']';
    // 1: Gets the shortcode HTML result
    $html_result = do_shortcode($shortcode);
    // 2: Explodes the XML structure
    $xml_parser = xml_parser_create();
    xml_parse_into_struct($xml_parser, $html_result, $array_result, $index);
    xml_parser_free($xml_parser);
    // 3: Scans the structure to retrieve the element IDs, as a post-#### class of a LI element
    $data_result = array();
    foreach($array_result as $item)
      if ($item['tag'] == 'LI' && isset($item['attributes']['CLASS'])){
        $data_result[] = preg_replace("/^[^0-9]*post-([0-9]*)[^0-9]*$/", "$1", $item['attributes']['CLASS']);  
      }	    
    return $data_result;
  }
  /** Gets the location of post or a member.
   * @param $item_id Post or member ID. If true uses the current post or member ID.
   * @param $item_type Either "posts" or "members".
   * @param $location_format Either "address", "region", or "coordinates"
   * @return The textual address or an array with lattitude,longitude, or false;.
   */
  public static function get_location($item_id = true, $item_type = "posts", $location_format = "coordinates") {
    global $wpdb; 
    if ($item_type == "posts"){
      if($item_id === true){
        $post_id =  get_post()->ID ;
      }else{
        $post_id = $item_id;
      }
      $location = $wpdb->get_row($wpdb->prepare("SELECT * FROM wp_places_locator WHERE post_id = %d", $post_id));
    }else if ($item_type == "members"){
      $location = $wpdb->get_row($wpdb->prepare("SELECT * FROM wppl_friends_locator WHERE member_id = %d", $item_id === true ? wp_get_current_user()->ID : $item_id));
    }

    if ($location){
      if($location_format == "address"){
        return $location->formatted_address." ".$location->city." ".$location->state;
      }else if($location_format == "region"){
        return $location->state;
      }else{
        return array($location->lat, $location->long);
      }
    }else{
      return false;
    }      
  }
  /** Gets the distance between two earth location coordinates.
   * @param $c1 An array of the form <tt>array(lattitude,longitude)</tt> for the 1st coordinates.
   * @param $c2 An array of the form <tt>array(lattitude,longitude)</tt> for the 2nd coordinates.
   * @return The distance in kilometers.
   */
  public static function get_location_distance($c1, $c2) {
    $lat1 = deg2rad(90 - $c1[0]);
    $lat2 = deg2rad(90 - $c2[0]);
    return 6371 * acos(cos($lat1) * cos($lat2) + sin($lat1) * sin($lat2) * cos(deg2rad($c1[1] - $c2[1])));
  }
  /** Sends an alert if someone add a new organisation for the meeting.
   * @param $input The new coordination name.
   * @param $email The email of coordinator (as provided by her or his profile).
   */
  public static function new_coordination_alert($input, $email) {
    mail("classcode-accueil@inria.fr",  "Point d'attention quant à la définition de la coordination qui fait le temps de rencontre", "Bonjour Class´Code <br/> Je viens de rentrer une nouvelle coordination sous l´intitulé « $input ». Il aurait fallu prélablement entrer cette structure dans le tableau des partenaires du maillage à compléter à chaque partenaire : https://docs.google.com/forms/d/e/1FAIpQLSfS8BXVHymwtAsTceYmnrYx_bHOKYO5xqeelBJrJ1Lz9LSoGA/viewform?usp=sf_link par le formulaire ou https://docs.google.com/spreadsheets/d/1Gg4BmcgBGbuJTyj1XMoT74X5mefSZVVzyBXQ6k108LU/edit directement dans le tableau et faire attention de bien entrer le même nom; d'ailleurs faisons ça tout de suite :)<br/> Bien Cordialement.", "From: $email\r\nCc: $email\r\nContent-type: text/html; charset=utf-8\r\nContent-Transfer-Encoding: 8bit\r\n");
  }
}
new rencontre_post_type();
?>
