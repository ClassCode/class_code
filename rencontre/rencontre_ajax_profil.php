<?php

define("WP_USE_THEMES", false);
require_once("../../../../wp-blog-header.php");

if (isset($_GET['ajax_profile']) and isset($_GET['user_id'])) {
  $user_id = $_GET['user_id'];
  $user = get_user_by('id',$user_id);  
  if($user){
    global $wpdb;    
    $userLocation = $wpdb->get_row($wpdb->prepare( "SELECT * FROM wppl_friends_locator WHERE member_id = %s", $user_id ) );

    $userLat = $userLocation->lat;
    $userLong = $userLocation->long;
    $userAddress = $userLocation->formatted_address;
    $userEmail = $user->user_email;
    echo display_classcode_profile($user_id);

?>
<script>
  jQuery(document).ready(function($){ 	
<?php
    echo "var profillatlng = new google.maps.LatLng(".$userLat.", ".$userLong.");";

?>
	  loadProfilGmap(profillatlng, '<?php echo $user->display_name;?>','<?php echo $user_id;?>', '<?php echo $userAddress;?>', '<?php echo $userEmail;?>');
  });


 </script>
<?php
  }else{
    echo '';
  }    
  exit(0);
}
?>
