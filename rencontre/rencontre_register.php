<?php

define("WP_USE_THEMES", false);
require_once("../../../../wp-blog-header.php");
//include_once('../../../../wp-includes/plugin.php');
include_once('rencontre_post_type.php');
 

// Manages the save, valid only if the wp_verify_nonce is ok
// Ref: https://codex.wordpress.org/Function_Reference/wp_nonce_field
if (isset($_POST['rencontres_edit_data_nonce']) && wp_verify_nonce($_POST['rencontres_edit_data_nonce'], 'rencontres_edit_id_nonce') && isset($_POST['post_id'])) {
  // Manages user registration
  if (isset($_POST['meetingRegister']) || isset($_POST['meetingUnregister']))
    rencontre_post_type::set_participant($_POST['post_id'], wp_get_current_user()->ID, isset($_POST['meetingRegister']));
  $post = get_post($_POST['post_id']);
  do_action( 'save_post', $_POST['post_id'], $post, true );
  header('Location: '.get_site_url().'/?post_type=rencontre&p='.$_REQUEST['post_id']);
  exit(0);
}else{
  header('Location: '.get_site_url());
  exit(0);
}





?>
