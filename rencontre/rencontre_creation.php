<?php

define("WP_USE_THEMES", false);
require_once("../../../../wp-blog-header.php");
//include_once('../../../../wp-includes/plugin.php');
include_once('rencontre_post_type.php');

// Manages the save, valid only if the wp_verify_nonce is ok
// Ref: https://codex.wordpress.org/Function_Reference/wp_nonce_field
if (isset($_POST['rencontres_edit_data_nonce']) && wp_verify_nonce($_POST['rencontres_edit_data_nonce'], 'rencontres_edit_id_nonce')) {
  $action = "update";
  if(isset($_POST['post_id']) && isset($_POST['delete']) && ($_POST['delete'] == 'true')){
    $action = 'delete';
    wp_delete_post( $_POST['post_id']);
    header('Location: '.get_site_url().'/rencontres?action='.$action);
    exit(0);
  }
  // Creates the post if not yet done
  if (!isset($_POST['post_id'])) {    
    $_REQUEST['post_id'] = rencontre_post_type::create_rencontre();
    if ($_REQUEST['post_id'] == 0) {
      echo '<h1>Il y a eu une erreur � la cr�ation de la rencontre, c�est un souci � signaler</h1>';
      exit(0);
    }else{      
      $action = "create";
    }
  }
    
  // Manages user registration
  if (isset($_POST['register']) || isset($_POST['unregister'])){
    rencontre_post_type::set_participant($_REQUEST['post_id'], wp_get_current_user()->ID, isset($_POST['register']));
  }

  //nettoyage des dates
  if(isset($_POST['rencontre_date_1']) && $_POST['rencontre_date_1']!=''){
    list($dd,$mm,$yyyy) = explode('/',$_POST['rencontre_date_1']);
    if (!checkdate((int) $mm,(int) $dd,(int) $yyyy)) {
      $_POST['rencontre_date_1']= '';
    }
  }
 
  //nettoyage du module si vide
  if(!isset($_POST['rencontre_module']) || $_POST['rencontre_module'] == ''){
    $_POST['rencontre_module']='Autre';
  }
  
  
  
  // Updates each parameter
  // Ref: https://codex.wordpress.org/Function_Reference/update_post_meta
  foreach(array('structure', 'capacity','precisions','rencontre_module', 'rencontre_date_1','rencontre_heure_1', 'rencontre_date_2','rencontre_heure_2', 'rencontre_pad', 'rencontre_hangout', 'sujet_precisions') as $name){
    if (isset($_POST[$name]))
      update_post_meta($_REQUEST['post_id'], $name, $_POST[$name]);
  }
  $post = get_post($_REQUEST['post_id']);

  include_once('../../geo-my-wp/plugins/posts/includes/admin/gmw-pt-metaboxes.php');
  include_once('../../geo-my-wp/includes/admin/geo-my-wp-admin-functions.php') ; 

  do_action( 'save_post', $_REQUEST['post_id'], $post, true );

  if(isset($_POST['duplicate']) && ($_POST['duplicate'] == 'true')){
    $action = 'duplicate';
    $_REQUEST['post_id'] = rencontre_post_type::create_rencontre($_REQUEST['post_id']);
  }
  
  if (isset($_POST['structure']) && $_POST['structure']!=''){
    $temp_Struct = stripslashes($_POST['structure']);
    include_once(WP_PLUGIN_DIR.'/class_code_v2/class_code_config.php');
    if(!in_array($temp_Struct,array_keys($data_structures))){
      $author_mail = get_user_by('ID', get_post($_REQUEST['post_id'])->post_author)->user_email;
      var_dump($author_mail);
      rencontre_post_type::new_coordination_alert($temp_Struct, $author_mail);
    }
  }
  
  //envoie des alertes : on les d�sactive dans la v2
  // rencontre_alert::alert_users($post->ID);
  
  header('Location: '.get_site_url().'/?post_type=rencontre&p='.$_REQUEST['post_id'].'&edit=true&action='.$action);
  exit(0);
}else{
  header('Location: '.get_site_url());
  exit(0);
}





?>
