<?php
/** Defines the Class'Code wordpress plugin implementation.
 * 
 * <pre>
 * Plugin Name: class_code
 * Plugin URI: http://sparticipatives.gforge.inria.fr/wp-plugins/
 * Description: Integrates the word-press Class'Code project functionnalities
 * Version: 0.3
 * Author: benjamin.ninassi@inria.fr, thierry.vieville@inria.fr
 * Author URI: http://classcode.fr
 * License: GPLv2 or later
 * </pre>
 *
 * \ingroup class_code
 */

// Voici ce qui definit le code conditionnel pendant les tests des rencontres

// Includes the Class'Code functionnality implementation
{
  include_once('posttype/quizz_post_type.php');
  //include_once('useravatar/class_code_user_avatar.php'); les avatars sont maintenant gérés par le plugin v2
  include_once('rencontre/rencontre_post_type.php');
  include_once('rencontre/rencontre_alert.php');
  //-debug- if (is_admin()) include_once('doc/error_for_all.php');
  //-debug- include_once('doc/pprint_r.php');
}
class class_code {  
  /** \private */
  function __construct() {
    // Implements the redirection to the user menu.
    add_filter('request', function ($request) {
	$user_logged_in = is_user_logged_in();
	if(!$user_logged_in){
	  show_admin_bar(false);    
	}
	if (preg_match("|^/classcode(/?((accueil)?/?))$|", $_SERVER['REQUEST_URI'])) {
	  header('Location: '.get_site_url().'/classcode-v2');
	  exit(0);
	} else if (isset($_REQUEST['class_code']) || isset($_REQUEST['cc'])) {
	  header('Location: '.get_site_url().'/classcode/accueil/documentation');
	  exit(0);
	} else if (isset($_REQUEST['cc2'])) {
	  header('Location: '.get_site_url().'/classcode/accueil/documentation#visiteAction2');
	  exit(0);
	} else if (isset($_REQUEST['ba'])) {
	  header('Location: '.get_site_url().'/classcode/accueil/aide');
	  exit(0);
	} else if (isset($_REQUEST['decodez_le_code'])) {
	  header('Location: '.get_site_url().'/wp-content/plugins/class_code/decoderlecode/index.html');
	  exit(0);
	}  else if (isset($_REQUEST['my-profile'])) {
	  if (wp_get_current_user()->ID != 0)
	    header('Location: '.site_url( "/members/".wp_get_current_user()->user_nicename."/profile/edit/group/1"));
	  else
	    header('Location: '.get_site_url().'/wp-login.php');
	  exit(0);
	} else
	  return $request;
      }, 1, 1);
    // Implements the admin menu and submenu
    add_action('admin_menu', array($this, 'admin_menu'), 20);
    // Implements the admin menu http wrapper and notification
    add_action('admin_notices', array($this, 'admin_notices'));    
    add_shortcode("include_classcode_page", function($atts, $content) {
	$pages = array(
		       "create_rencontre" => "rencontre/rencontre-edit.php",
           "find_users" => "rencontre/users-find.php"
		       );
	if (isset($pages[$atts['page']])) {
	  $file = plugin_dir_path( __FILE__ )."/".$pages[$atts['page']];
	  if (file_exists($file)) {
	    ob_start();
	    include($file);
	    return ob_get_clean();
	  } else
	    return "<pre>[include error='the $file does not exists']</pre>";
	} else 
	  return "<pre>[include error='the ".$atts['page']." does not exists']</pre>";
      });
    // @todo Utilisé seulement pour tester
    add_shortcode("include_test_gmw", function($atts, $content) {
	return rencontre_post_type::get_facilitators_around_a_rencontre($atts['post_id'], $atts['radius']);
      });
  }
  /** Implements the administration menu wrapper.
   * \private
   */
  function admin_menu() {
    $admin_page = add_menu_page('Class´Code', 'Class´Code', 'edit_posts', 'class_code_admin', array($this, 'menu_html'), 'dashicons-welcome-learn-more', '24.123');
    add_submenu_page('class_code_admin', '', 'Les quizz', 'edit_posts', 'class_code_quizz_edit', function() { echo '<script>location.replace("'.get_site_url().'/wp-admin/edit.php?post_type=quizz");</script>'; });
    add_submenu_page('class_code_admin', '', 'Les parcours', 'edit_posts', 'class_code_parcours_edit', function() { echo '<script>location.replace("'.get_site_url().'/wp-admin/edit-tags.php?taxonomy=parcours");</script>'; });
    add_action('load-'.$admin_page, function() {
	$screen = get_current_screen();
	$screen-> add_help_tab(array(
				     'id'	=> 'class_code_help_tab',
				     'title'	=> 'L´outil Class´Code',
				     'content' => '<p><a href="http://classcode.fr">classcode.fr</a> est un projet de formation de formateurs (enseignants, animateurs) qui ont vocation à initier les jeunes de la tranche d´âge 8 à 14 ans (primaire/collège) à l´informatique pour la littératie numérique, ce plugin wordpress implémente les fonctionnalités utiles pour réaliser cette formation hybride (en ligne et présentielle).</p>',
				     ));
      });
  }
  /** Implements the admin menu http wrapper and notification
   * \private
   */
  function admin_notices() {
    // Notices are either 'updated' or 'error' or 'update-nag' 
    $notices = array();
    /*
    // Manages old PHP pages
    if (isset($_REQUEST['register_include_post_type']) && $_REQUEST['register_include_post_type'] == 'please_do_it') {
      foreach(array(
		    "userprofile/class_code_user_menu" => "Bienvenue dans les parcours de Formation Class´Code",
		    "userprofile/class_code_user_data" => "Mes données",
		    "userprofile/class_code_user_basket_menu" => "Mon panier de lien",
		    "userprofile/class_code_user_avatar_menu" => "Mon avatar",
		    "posttype/parcours_taxinomy_result" => "Statistiques sur les parcours",
		    ) as $page => $title)
	include_post_type::register_page($page, $title);
      $notices[] = array('update-nag' => "Les <a href='".get_site_url()."/wp-admin/edit.php?post_type=include_post_type'>pages internes sont mises à jour</a> avec: «<tt>".print_r(include_post_type::$pages, true)."</tt> »");
    }
    */
    /*
    if (isset($_REQUEST['show_users_statistics']) && $_REQUEST['show_users_statistics'] == 'please_do_it') {
      // Echoes user extra profile data
      {
	$html = "";
	foreach(array('location' => 'Localisation', 'class_code_role' => 'Rôle dans Class´Code') as $field => $label) {
	  $users = get_users(array('meta_key' => $field));
	  $html .= "<tr><td>$label </td><td>:</td><td> #".count($users)."</td></tr>\n";
	}
	$notices[] = array('updated' => "<b>Comptage des entrées:</b><table>\n$html</table>\n");
      }
      // Echoes user extra profile data
      {
	include_once(plugin_dir_path( __FILE__ ).'/posttype/parcours_taxinomy_result.php');
	$notices[] = array('updated' => "<b>Le parcours de découverte de la formation:</b><pre>".print_r(parcours_taxinomy_result::get_parcours_score('decouverte-de-la-formation'), true)."</pre>");
      }
    }
    if (isset($_REQUEST['show_users_data']) && $_REQUEST['show_users_data'] == 'please_do_it') {
      echo '<table><tr><th>Identifiant</th><th>Email</th><th>Prénom</th><th>Nom</th><th>Structure</th><th>Adresse</th><th>Région</th></tr>';
      foreach(get_users() as $user)
	echo '<tr><td>'.$user->user_login.'</td><td>'.$user->user_email.'</td><td>'.$user->first_name .'</td><td>'.$user->last_name.'</td><td>'.bp_get_profile_field_data(array('field' => 'Structure', 'user_id' => $user->ID)).'</td><td>'.rencontre_post_type::get_location($user->ID, "members", "address").'</td><td>'.rencontre_post_type::get_location($user->ID, "members", "region").'</td></tr>';
      echo '</table>';
    }    
    */
    /*
    if (isset($_REQUEST['show_oc_courses']) && $_REQUEST['show_oc_courses'] == 'please_do_it') {
      OpenClassroomsAPI::test();
    }
    if (isset($_REQUEST['reset_oc_courses']) && $_REQUEST['reset_oc_courses'] == 'please_do_it') {
      OpenClassroomsAPI::reset();
    }
    */
    /* 
    if (isset($_REQUEST['update_data_structure']) && $_REQUEST['update_data_structure'] == 'please_do_it') {
      include_once('structures/update_data_structure.php');
      echo update_data_structure();
    }
    */
    if (isset($_REQUEST['show_users_mails']) && $_REQUEST['show_users_mails'] == 'please_do_it') {
      echo '<hr/><pre>[';
      foreach(get_users() as $user)
	echo $user->user_email.',';
      echo ']</pre><hr/>';
    }
    // Implements the notification
    {
      foreach ($notices as $notice)
	foreach($notice as $class => $message)
	  echo "<div class='$class'><p>$message</p></div>";
    }
  }
  /** Implements the administration menu.
   * \private
   */
  function menu_html() {
    echo '<div style="padding-right:20px;"><h1>Tableau de bord de Class´Code</h1>';
    echo "<hr/><div style='float:right'><a href='https://classcode.fr'><img height='60px' alt='classcode.fr' src='".plugins_url()."/class_code/doc/LOGO-ClassCode-coul.jpg'/></a></div>";
    echo "<h2>Fonctions du backoffice</h2><table>";
    
    // echo "<tr><td><a class='button' href='?page=class_code_admin&update_data_structure=please_do_it'>Mise à jour des données des structures</a></td><td>Intègre les données du <a target='_blank' href='https://drive.google.com/open?id=0B42D-mwhUovqekI3UUtscXNVcUk'>tableau des coordinateurs</a></td></tr>";
    
    // echo "<tr><td><a class='button' href='?page=class_code_admin&show_oc_courses=please_do_it'>Test de l'API OC-C´C </a></td><td> (juste pour le developpement du lien avec l'API OC)</td></tr>";
    // echo "<tr><td><a class='button' href='?page=class_code_admin&reset_oc_courses=please_do_it'>Reset data de l'API OC-C´C </a></td><td> (juste pour le developpement du lien avec l'API OC)</td></tr>";
    
    //echo "<tr><td>"; test_rencontre_alert(); echo "</td></tr>";
    
    echo "</table>";
    
    echo "<hr/><h2>Outils liés aux contenus interactifs</h2><table style='margin-left:50px'>";
    foreach(array("rencontre" => "Les rencontres" , 
		  "quizz" => "Questions de quizz" , 
		  //- "votewall" => "Mur de vote temps réel", 
		  //- "poster" => "Poster de visite virtuelle", 
		  //- "clicarea" => "Images avec zones cliquables",
		  ) as $post_type => $post_title) {
      echo "<tr><td><h3><a href='".get_site_url()."/wp-admin/edit.php?post_type=$post_type'>$post_title</a></h3></td><td style='padding-left:50px'><h3><a class='button' href='".get_site_url()."/wp-admin/post-new.php?post_type=$post_type'>Ajouter</a></h3></td>".($post_type == "quizz" ? "<td style='padding-left:50px'><h3><a href='".get_site_url()."/wp-admin/edit-tags.php?taxonomy=parcours'>Édition des parcours</a></h3></td>" : "").($post_type == "rencontre" ? "<td style='padding-left:50px'><h3><a href='https://classcode.fr/rencontres/'>Gestion des rencontres</a></h3></td>" : "")."</tr>";
    }
    echo "</table>";
   // echo "<div align='right'><a class='button' href='?page=class_code_admin&show_users_statistics=please_do_it'>Comptage des entrées des utilisateurs</a></div>";
   // echo "<div align='right'><a class='button' href='?page=class_code_admin&show_users_data=please_do_it'>Dump des entrées des utilisateurs</a></div>";
  // echo "<div align='right'><a class='button' href='?page=class_code_admin&show_users_mails=please_do_it'>Dump des mails des utilisateurs</a></div>";
    //
    echo "<hr/><h2>Documentation de l'implémentation pour les développeurs</h2>Les fonctionnalités de Class'Code sont implémentées sous forme de modules en langage PHP et JS:<table style='margin-left:50px'><tr>
     <td style='padding-left:40px'><h4><a href='".plugins_url()."/class_code/doc/.html/classclass__code.html'>Méta données du plugin</a></h4></td>
     <td style='padding-left:40px'><h4><a href='".plugins_url()."/class_code/doc/.html/modules.html'>Modules implémentés</a></h4></td>
     <td style='padding-left:40px'><h4><a href='".plugins_url()."/class_code/doc/.html/hierarchy.html'>Hiérarchie des classes</a></h4></td>
     <td style='padding-left:40px'><h4><a href='".plugins_url()."/class_code/doc/.html/files.html'>Hiérarchie des fichiers</a></h3></td>
     </tr></table>";
    echo "<p><b>Lien vers le <a target='_blank' href='https://pad.inria.fr/p/bx42JkrE5JW0HBIQ_plateforme_classcode'>pad de développement</a></p></b>";
    //
    echo "<hr/></div>";
  }

  
}
new class_code();

// Defines the documentation modules and root classes
/** \defgroup class_code Class'Code functionnality implementation.
 */
/** \defgroup custom_post_type Wordpress custom post-type and taxinomy.
 * \ingroup class_code
   */
/** \class custom_post_type 
 * The root of Wordpress custom post-type.
 */
/** \class custom_taxinomy 
 * The root of Wordpress custom taxinomy.
 */
/** \defgroup meta_box Wordpress custom meta-box.
 * \ingroup class_code
 */
/** \defgroup userprofile Class'Code user menu and profile.
 * \ingroup class_code
 */
/*//* \class user_menu
 * The root of user menu function.
 */
/** \defgroup Javascript Javascript functions.
 * \ingroup class_code
 */
/** \class Javascript
 * The root of javascript function.
 */
/** \class Utility
 * The root of utility function or class.
 */
function classCodeHomePageId(){
  return 8139;
}

include_once(get_template_directory().'/_inc/display-functions.php');
 if((isset($_GET['v2']) && ($_GET['v2']=='false'))){
  include_once(plugin_dir_path( __FILE__ ).'/bp-profiles.php');
}
?>