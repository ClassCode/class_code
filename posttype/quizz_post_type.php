<?php
include_once(plugin_dir_path( __FILE__ ).'../metabox/help_meta_box.php');
include_once(plugin_dir_path( __FILE__ ).'../metabox/text_meta_box.php');
include_once(plugin_dir_path( __FILE__ ).'../metabox/json_meta_box.php');
include_once(plugin_dir_path( __FILE__ ).'../metabox/choice_meta_box.php');
include_once(plugin_dir_path( __FILE__ ).'../metabox/meta_box_history.php');
include_once('posttype_shortcode.php');
include_once('parcours_taxinomy.php');
/**
 * Defines a new post type called quizz to define questions or scores.

   *  - The answer in the <tt>'quizz_{$post_id}_answer'</tt> user meta-data field, as a string if a unique answer, or as a semi-colum separated list for multiple answers.
   *  - The fact the answer is correct or not is stored in the <tt>'quizz_{$post_id}_answer_ok'</tt> user meta-data field, as a 'true' or 'false' string, otherwise the question has not been answered.


 * @see https://codex.wordpress.org/Post_Types#Custom_Post_Types 
 * @see https://codex.wordpress.org/Function_Reference/register_post_type
 *
 * \ingroup custom_post_type
 * \extends custom_post_type
 */
class quizz_post_type {
  function __construct() {
    add_action('init', function() {
	register_post_type('quizz',
			   array(
				 'labels' => array(
						   'name' => 'Questions de quizz',
						   'singular_name' => 'Question de quizz',
						   'parent_item_colon' => 'Question parente',
						   ),
				 'description' => 'Ce contenu défini une question d´un quizz ou l´affichage d´un score',
				 'public'               => true,
				 'exclude_from_search'  => true,
				 'show_in_menu'         => false,
				 'supports'             => array('title', 'revisions', 'comments'),
				 'taxonomies'           => array('post_tag', 'parcours'),
				 'can_export'           => true,
				 'register_meta_box_cb' => function() {
				   // Removes a spurious meta-box
				   remove_meta_box('slugdiv', 'quizz', 'normal');
				 }
				 ));
	register_taxonomy_for_object_type('post_tag', 'quizz');
	register_taxonomy_for_object_type('parcours', 'quizz');
      });
    // Adds the post edition meta-boxes
    if (is_admin()) {
      // Displays the post-type help
      new help_meta_box('quizz_help', array(
					    'title' => 'Définition d´une question de quizz', 
					    'post_type' => 'quizz', 
					    'priority' => 'high',
					    'meta_box_render' => function($value, $post_id) {
					      echo "
<p>Pour <b>insérer une question de quizz dans un article</b>, utiliser le shortcode :<div align='center'><span class='button'><tt>[quizz id='$post_id']</tt></span></div></p>";
					      echo "
<p>Pour <b>définir une zone question de quizz</b> :
  <p style='margin-left:30px;'>Entrer les trois textes qui (i) présentent la question et s'affichent lors d'une réponse (ii) exacte ou (iii) inexacte.</p>
  <p style='margin-left:30px;'>Définir l´élément en syntaxe JSON qui est de la forme <pre>{
\"choices\" : [ \"Oui\", \"Non\", \"Ne sait pas\" ],
\"multiple\" : false,
\"text\" : 40,
\"regex\" : \"/(Oui|Non)/\"
}</pre> où :
   <p style='margin-left:60px;'>\"choices\" : est une liste de réponses prédifinies;</p>
   <p style='margin-left:60px;'>\"multiple\" : a la valeur true si il y a plusieurs réponses possibles, false sinon;</p>
   <p style='margin-left:60px;'>\"text\" : a la valeur false si on ne peut entrer une réponse alternative et donne la largeur du champ texte de la valeur alternative sinon;</p>
   <p style='margin-left:60px;'>\"regex\" : est une expression régulière qui définit si la réponse est exacte;
     <p style='margin-left:80px;'> - la réponse est exacte si elle marche l'expression régulière est fausse sinon;</p>
     <p style='margin-left:80px;'> - une réponse multiple est séparée par des ';';</p>
     <p style='margin-left:80px;'> - les expressions régulières sont définies par la norme posix;</p>
     <p style='margin-left:80px;'> - si l'expression régulière n'est pas définie alors toute réponse est exacte;</p>
     <p style='margin-left:80px;'> - attention les ``'´´ sont traduits par ``\'´´;</p>
   </p>
  </p>
</p>";
					    }));
      // Displays quizz presentation edition box
      new text_meta_box('quizz_head',  array(
					     'title' => 'Présentation de la question', 
					     'post_type' => 'quizz',
					     'textarea_rows' => 4));
      // Displays quizz question definition
      new json_meta_box('quizz_json', 
			array(
			      'title' => 'Programmation de l´élément',
			      'post_type' => 'quizz',
			      'save_content_as_short_code' => true,
			      'models' => array(
						"Modèle de question" => '{
"choices" : [ "Oui", "Non", "Ne sait pas" ],
"multiple" : false,
"text" : 40,
"regex" : "/(Oui|Non)/"
}')));
      // Displays the quizz true answer information
      new text_meta_box('quizz_true',  array(
					     'title' => 'Affichage lors d´une réponse exacte', 
					     'post_type' => 'quizz',
					     'textarea_rows' => 4));
      // Displays the quizz false answer information
      new text_meta_box('quizz_false',  array(
					      'title' => 'Affichage lors d´une réponse inexacte', 
					      'post_type' => 'quizz',
					      'textarea_rows' => 4));
    } else {
      // Defines a post short code to render the front page content
      new posttype_shortcode('quizz', function ($post) {
	  $json = json_decode(get_post_meta($post->ID, 'quizz_json', true), true);
	  if ($json) {   
	    ob_start();
	    echo "<div class='quizz'>";
	    // Displays the quizz presentation
	    text_meta_box::echo_value_as_content($post->ID, "quizz_head");
	    // Displays the quizz meta-box
	    new choice_meta_box("quizz_".$post->ID."_answer", wp_parse_args(array('post_type' => false), 
									    $json));
	    // Sets the answer ok value
	    $answer = 
	      isset($_REQUEST["quizz_".$post->ID."_answer_value"]) ? $_REQUEST["quizz_".$post->ID."_answer_value"] :
	      (is_user_logged_in() ? get_user_meta(wp_get_current_user()->ID, "quizz_".$post->ID."_answer", true) : "");
	    $answer_ok = (!isset($json['regex'])) || preg_match($json['regex'], $answer);

	    // Manages the answer if any 
	    if($answer != "") {

	      // Updates the answer_ok field 
	      if (basic_meta_box::can_edit("quizz_".$post->ID."_answer") && isset($_REQUEST["quizz_".$post->ID."_answer_value"])) {
		update_user_meta(wp_get_current_user()->ID, "quizz_".$post->ID."_answer_ok", $answer_ok ? "true" : "false");
	      }

	      // Echoes a up/down thumb on the window right
	      if (isset($json['regex']) && is_user_logged_in())
		echo "<div id='quizz_".$post->ID."_answer_ok' style='float:right'><img src='".get_site_url()."/wp-content/plugins/class_code/posttype/img/thumb_".($answer_ok ? "up" : "down").".png'/></div>";
	      // Echoes the related quizz information
	      text_meta_box::echo_value_as_content($post->ID, $answer_ok ? "quizz_true" : "quizz_false");
	    }
	    echo "</div>";
	    return ob_get_clean();
	  } else 
	    return "[quizz error='syntax error in the element definition JSON syntax]";
	});
    }
  }
}
new quizz_post_type();
?>