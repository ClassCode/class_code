<?php
/**
 * Defines a non-editable custom post-type rendering an internal PHP page.
 *
 * \private && \deprecated
 * \ingroup custom_post_type
 * \extends custom_post_type
 */
class include_post_type
{
  /** Gets the custom post-type link corresponding to the a given internal page. */
  public static function get_link($page) {
    $pages = self::use_pages();
    return get_site_url().(isset($pages[$page]) ? '?p='.$pages[$page] : '');
  }
  /** Registers non-editable custom post-type rendering an internal PHP page.
   * @param $page The page slug.
   * - The corresponding PHP page is <tt>wordpress/wp-content/plugins/class_code/$page.php</tt>.
   * @param $title The page title.
   */
  public static function register_page($page, $title) {
    $pages = self::use_pages();
    if (isset($pages[$page]))
      wp_delete_post($pages[$page], true);
    $post = array(  
		  'post_title'     => $title,
		  'post_name'      => $page,
		  'post_content'   => "[include_post_type page='$page']",
		  'post_status'    => 'publish',
		  'post_author'   => 1,
		  'post_type'      => 'include_post_type',
		  'post_category'  => array(get_category_by_slug('presentation')->term_id),
		    ); 
    $post_ID = wp_insert_post($post);
    self::$pages[$page] = $post_ID;
    update_site_option('include_post_type', self::$pages);
  }
  // Cache mechanism of the non-editable custom post-type rendering an internal PHP page.
  public static $pages = false;
  private static function use_pages() {
    if (!self::$pages)
      self::$pages = get_site_option('include_post_type');
    if (!self::$pages)
      self::$pages = array();
    return self::$pages;
  }
  
  function __construct() {
    add_action('init', function() {
	register_post_type('include_post_type',
			   array(
				 'description'          => 'Page interne au plugin',
				 'public'               => true,
				 'capability_type'      => 'page',
				 'exclude_from_search'  => true,
				 'show_in_menu'         => false,
				 'taxonomies'           => array('category'),
				 'can_export'           => false,
				 'supports'             => array('title', 'comments'),
				 ));
      });
    add_shortcode("include_post_type", function($atts, $content) {
	$file = plugin_dir_path( __FILE__ )."../".$atts['page'].".php";
	if (file_exists($file)) {
	  ob_start();
	  include($file);
	  return ob_get_clean();
	} else
	  return "<pre>[include error='the $file does not exists']</pre>";
      });
  }
}
new include_post_type();
?>