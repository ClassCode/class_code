<?php
include_once(plugin_dir_path( __FILE__ ).'../metabox/help_meta_box.php');
include_once(plugin_dir_path( __FILE__ ).'../metabox/image_selection_meta_box.php');
include_once('posttype_shortcode.php');

/**
 * Defines a new post type called clicarea to define clicable image.
 *
 * @see link_meta_box
 *
 * \ingroup custom_post_type
 * \extends custom_post_type
 */
class clicarea_post_type {
  function __construct() {
    add_action('init', function() {
	register_post_type('clicarea',
			   array(
				 'labels' => array(
						   'name' => 'Images avec zones cliquables',
						   'singular_name' =>'Image avec zones cliquables'
						   ),
				 'description'          => 'Ce contenu défini une image avec des zones rectangulaires cliquables qui ouvrent un lien dans la fenêtre d´affichage',
				 'public'               => true,
				 'exclude_from_search'  => true,
				 'show_in_menu'         => false,
				 'supports'             => array('title', 'comments', 'revisions'),
				 'can_export'           => true,
				 'register_meta_box_cb' => function() {
				   // We declare 'comments' to load the content editor for link edition but hide it
				   remove_meta_box('commentstatusdiv', 'clicarea', 'normal');
				   remove_meta_box('commentsdiv', 'clicarea', 'normal');
				   // This is a spurious meta_mob to be removed
				   remove_meta_box('slugdiv', 'clicarea', 'normal');
				 }
				 )
			   );
      }); 
    // Adds the post edition meta-boxes
    if (is_admin()) {
      new help_meta_box('clicarea_help', array(
					       'title' => 'Utilisation d´image à zone cliquable', 
					       'post_type' => 'clicarea', 
					       'priority' => 'high',
					       'meta_box_render' => function($value, $post_id) {
						 echo "
<p>Pour <b>insérer une image à zone cliquable dans un article</b>, utiliser le shortcode :<div align='center'><span class='button'><tt>[clicarea id='$post_id']</tt></span></div></p>";
						 echo "
<p>Pour <b>définir une zone cliquable</b> :
<p style='margin-left:30px;'>Sélectioner une zone rectangulaire à la souris dans l'image</p>
  <p style='margin-left:60px;'>Cliquer sur le coin supérieur-gauche de la sélection.</p>
  <p style='margin-left:60px;'>Faire glisser la souris vers le coin inférieur-droit de la sélection.</p>
  <p style='margin-left:60px;'>Cliquer une 2ème fois pour valider la sélection.</p>
<p style='margin-left:30px;'>Puis aller éditer le lien en cliquant sur le crayon dans la list des sélections qui sont sous l'image</p>
  <p style='margin-left:60px;'>Ou supprimer la sélection en cliquant sur la croix en X si il y a une erreur.</p>
<p style='margin-left:30px;'>Recommencer pour chaque et sauvegarder.</p>
</p>";
					       }));
      new image_selection_meta_box('clicarea_image', array(
							   'title' => 'Sélection de zones cliquables',
							   'post_type' => 'clicarea',
							   'priority' => 'high',
							   ));
    } else {
      new posttype_shortcode('clicarea');
    }
  }
}
new clicarea_post_type();
?>