<?php
/**
 * Defines a shortcode mechanism to render a custom post-type in another document.
 *
 * @see https://codex.wordpress.org/Shortcode_API
 *
 * \ingroup custom_post_type
 * \extends Utility
 */
class posttype_shortcode
{
  /** Defines the shortcode inclusion mechanism.
   * - This defines a shortcode of name <tt>[$post_type id="$post_id"]</tt>
   * - The default behavior is to include the post content (without the post-title) after applying 'the_content' filters.
   *
   * Typical usage is:<pre>
   * new posttype_shortcode('post_type', function($post) { ... }); 
   *</pre> or simply: <pre>
   * new posttype_shortcode('post_type');
   * </pre>
   * @param $post_type The post type on which this shortcode applies.
   * @param $post_render A callback function of the form <tt>post_render($post)</tt> called to render the post.
   */
  public function __construct($post_type = 'post', $post_render = false) {
    $this->post_type = $post_type;
    $this->post_render = $post_render ? $post_render : function($post) {
      return str_replace(']]>', ']]&gt;', apply_filters('the_content', $post->post_content));
    };
    add_shortcode($post_type, array($this, 'shortcode'));
  }
  /** Implements the shortcode
   * \private
   */
  function shortcode($atts, $content) {
    // Calls the content_render() function if the id is defined, the post exists, and has the expected post type.
    if (isset($atts['id'])) {
      $post = get_post($atts['id']);
      if ($post) {
	if ($post->post_type = $this->post_type) {
	  return call_user_func($this->post_render, $post);
	} else
	  return "<pre>[".$this->post_type." error='the <a href='". get_site_url()."/?p=".$atts['id']."'>post</a> of id ".$atts['id']." is not of type ".$this->post_type." but ".$post->post_type." ']</pre>";
      } else
	return "<pre>[".$this->post_type." error='the post of id ".$atts['id']." is undefined']</pre>";
    } else 
      return "<pre>[".$this->post_type." error='the ".$this->post_type." id is undefined']</pre>";
  }
}
?>