<?php
/**
 * Defines a new hierarchical taxinomy to group posts in sequential reading.
 * - This is a hierachical sequential taxinomy.
 *
 * @see https://codex.wordpress.org/Function_Reference/register_taxonomy
 *
 * \ingroup custom_post_type
 * \extends custom_taxinomy
 */
class parcours_taxinomy {
  function __construct() {
    add_action('init', function() {
	register_taxonomy('parcours', 'quizz', 
			  array('label'             => 'Parcours',
				'public'            => true,
				'hierarchical'      => true,
				'show_ui'           => true,
				'show_in_menu'      => false,
				'show_admin_column' => true,
				'sort'              => true
				));
	register_taxonomy_for_object_type('parcours', 'post'); 
	register_taxonomy_for_object_type('parcours', 'quizz');
      });
    // Adapts the display of the taxinomy panel
    add_filter('manage_edit-parcours_columns', function($columns) {
	// Hiddes teh original name and description in menu
	unset($columns['name']);
	unset($columns['description']);
	unset($columns['posts']);
	// Adds a new suitable edition column and a result column
	$columns['edit'] = 'Nom';
	$columns['list'] = 'Compte';
	$columns['result'] = 'Résultats';
	return $columns;
      });
    add_filter("manage_parcours_custom_column", function($content, $column_name, $parcours_id) {
	$parcours = get_term($parcours_id, 'parcours');
	switch ($column_name) {
	  // Implements the modify/display new edit column
	case 'edit' :
	  $content .= "<strong>".$parcours->name."</strong><br/><div class=row-actions'><span class='edit'><a href='".get_site_url()."/wp-admin/edit-tags.php?action=edit&taxonomy=parcours&tag_ID=".$parcours->term_id."'>Modifier</a> | </span><span class='view'><a href=".get_site_url()."/?parcours=".$parcours->slug."&post_type[0]=quizz&post_type[1]=post'>Afficher</a></span></div>";
	  break;
	  // Implements the related post-types edition page
	case 'list' :
	  $content .= $parcours->count."<br/><div class=row-actions'><span class='posts'><a href='".get_site_url()."/wp-admin/edit.php?post_type=quizz&parcours=".$parcours->slug."'>quizz</a> | </span><span class='posts'><a href='".get_site_url()."/wp-admin/edit.php?post_type=post&parcours=".$parcours->slug."'>article</a></span></div>";
	  break;
	  // Implements a display of the parcours statistics
	case 'result' :
	  $content .= '<a href="'.get_site_url().'?class_code_page=posttype/parcours_taxinomy_result&parcours_slug='.$parcours->slug.'">résultats</a>';
	  break;
	}
	return $content;
      }, 10, 3);
    // Modifies the taxinomy edition form
    add_action('parcours_edit_form',function() {
	$term_id = $_GET['tag_ID'];
	// These functions are for further functionnality implementation
	{
	  // $parcours = get_term_by('id', $term_id, 'parcours');
	  // $meta = get_option("parcours_{$term_id}");
	}
	// Implements a delete button
	{
	  echo "<div><span class='button'> <input type='checkbox' id='delete-parcours' name='delete-parcours' value='ah-que-je-veux' onChange='if(document.getElementById(\"delete-parcours\").checked) alert(\"Attention cette suppression sera définitive\")'/>Supprimer la catégorie.</span></div>";
	}
      });
    add_action('edited_parcours', function($term_id) {
	// These functions are for further functionnality implementation
	{
	  // update_option("parcours_{$term_id}", $_REQUEST['field-name-1']);
	}
	// Implements the delete button modification (notice: this option is not protected)
	{
	  if ($_REQUEST['delete-parcours'] == 'ah-que-je-veux')
	    wp_delete_term($term_id, 'parcours');
	}
      }, 10, 2);
    // Suppresses the description field
    add_action('admin_footer-edit-tags.php', function() {
	// Implements hidding descrption field in edit-tag menu and edition using jQuery page post-edition
	global $current_screen;
	if($current_screen->id == 'edit-parcours') {
      echo "
<script type='text/javascript'>
  jQuery(document).ready(function($) {
    $('#tag-description').parent().remove();
    $('.term-description-wrap').remove();
  });
</script>";
	}
      });
  }
}
new parcours_taxinomy();
?>