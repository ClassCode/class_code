<?php
include_once(plugin_dir_path( __FILE__ ).'../metabox/help_meta_box.php');
include_once(plugin_dir_path( __FILE__ ).'../metabox/bookmarks_meta_box.php');
/**
 * Defines a new post type called vote-wall to propose real time questions and feedback.
 * \ingroup custom_post_type
 * \extends custom_post_type
 */
class poster_post_type {
  function __construct() {
    add_action('init', function() {
	register_post_type('poster',
			   array(
				 'labels' => array(
						   'name' => 'Posters pour visite virtuelle',
						   'singular_name' => 'Poster pour visite virtuelle',
						   ),
				 'description' => 'Ce contenu défini un poster qui correspond à une visite de site',
				 'public'               => true,
				 'exclude_from_search'  => true,
				 'show_in_menu'         => false,
				 'supports'             => array('title', 'comments', 'revisions'),
				 'taxonomies'           => array(),
				 'can_export'           => true,
				 'register_meta_box_cb' => function() {
				   // We declare 'comments' to load the content editor for link edition but hide it
				   remove_meta_box('commentstatusdiv', 'poster', 'normal');
				   remove_meta_box('commentsdiv', 'poster', 'normal');
				   // Removes a spurious meta-box
				   remove_meta_box('slugdiv', 'poster', 'normal');
				 }
				 ));
      });
    // Adds the post edition meta-boxes
    if (is_admin()) {
      // Displays the post-type help
      new help_meta_box('poster_help', array(
					     'title' => 'Définition de la question à poser', 
					     'post_type' => 'poster', 
					     'priority' => 'high',
					     'meta_box_render' => function($value, $post_id) {
					       echo "
<p>Pour <b>définir le poster .</b> :
</p>";
					     }));
      // Displays the more link list
      new bookmarks_meta_box('poster_more', array(
						  'title' => 'En savoir plus', 
						  'post_type' => 'poster', 
						  'priority' => 'high',
						  ));
    }
  }
}
new poster_post_type();
?>