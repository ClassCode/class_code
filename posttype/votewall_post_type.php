<?php
include_once(plugin_dir_path( __FILE__ ).'../metabox/help_meta_box.php');
include_once(plugin_dir_path( __FILE__ ).'../metabox/json_meta_box.php');
include_once(plugin_dir_path( __FILE__ ).'../metabox/choice_meta_box.php');
include_once('posttype_shortcode.php');
/**
 * Defines a new post type called vote-wall to propose real time questions and feedback.
 * \ingroup custom_post_type
 * \extends custom_post_type
 */
class votewall_post_type {
  function __construct() {
    add_action('init', function() {
	register_post_type('votewall',
			   array(
				 'labels' => array(
						   'name' => 'Murs de notes et votes',
						   'singular_name' => 'Mur de notes et votes',
						   ),
				 'description' => 'Ce contenu défini un mur de vote et notes en temps réel',
				 'public'               => true,
				 'exclude_from_search'  => true,
				 'show_in_menu'         => false,
				 'supports'             => array('title', 'revisions'),
				 'taxonomies'           => array(),
				 'can_export'           => true,
				 'register_meta_box_cb' => function() {
				   // Removes a spurious meta-box
				   remove_meta_box('slugdiv', 'votewall', 'normal');
				 }
				 ));
      });
    // Adds the post edition meta-boxes
    if (is_admin()) {
      // Displays the post-type help
      new help_meta_box('votewall_help', array(
					       'title' => 'Définition de la question à poser', 
					       'post_type' => 'votewall', 
					       'priority' => 'high',
					       'meta_box_render' => function($value, $post_id) {
						 echo "
<p>Pour <b>insérer un vote avec un mur dans un article</b>, utiliser le shortcode :<div align='center'><span class='button'><tt>[votewall id='$post_id']</tt></span></div></p>";
						 echo "
<p>Pour <b>définir la question du vote</b> :
  <p style='margin-left:30px;'>Définir l´élément en syntaxe JSON qui est de la forme <pre>{
\"title\" : \"Alors ça vous plaît ?\",
\"choices\" : [ \"Oui\", \"Non\", \"Ne sait pas\" ],
\"text\" : 40
}</pre> où :
   <p style='margin-left:60px;'>\"title\" : est une le titre de la question;</p>
   <p style='margin-left:60px;'>\"choices\" : est une liste de réponses prédifinies;</p>
   <p style='margin-left:60px;'>\"text\" : a la valeur false si on ne peut entrer une réponse alternative et donne la largeur du champ texte de la valeur alternative sinon;</p>
  </p>
</p>";
					  }));
      // Displays the question definition
      new json_meta_box('votewall_json', 
			array(
			      'title' => 'Programmation de l´élément',
			      'post_type' => 'votewall',
			      'save_content_as_short_code' => true,
			      'models' => array(
						"Modèle de question" => '{
"title" : "Alors : ça vous plaît ?",
"choices" : [ "Oui", "Non", "Ne sait pas" ],
"text" : 40
}')));
      // Displays the PAD link edition window
      new basic_meta_box('votewall_pad', 
			 array(
			       'title' => 'Insertion d´un notepad, entrer son lien public',
			       'post_type' => 'votewall',
			       ));
    } else {
      // Defines a post short code to render the front page content
      new posttype_shortcode('votewall', function ($post) {
	  // Displays a frame with pad, if any.
	  {
	    $padlink = get_post_meta($post->ID, 'votewall_pad', true);
	    if ($padlink && $padlink != '')
	      echo '<iframe id="notepad" width="100%" height="400px;" src="'.$padlink.'" scrolling="no"><p>Your browser does not support iframes.</p></iframe><script>location.hash="#notepad"; document.getElementById("notepad").scrollIntoView(true);</script>';
	  }
	  // Displays the vote meta-box
	  {
	    $json = json_decode(get_post_meta($post->ID, 'votewall_json', true), true);
	    if ($json) {
	      // Displays the votewall meta-box
	      new choice_meta_box("votewall_".$post->ID."_answer", wp_parse_args(array(
										       'post_type' => false,
										       'save_frontend_value_history' => true), 
										 $json));
	      // Displays the votewall history result
	      {
		$this->name = "votewall_".$post->ID."_answer";
		$this->user_id = is_user_logged_in() ? get_current_user_id() : 0;
		if ($this->user_id != 0) {
		  // Echoes the history-count in a meta-box
		  new help_meta_box('votewall_result', array(
							     'title' => 'Comptage des réponses du jour',
							     'post_type' => false,
							     'meta_box_render' => function($value, $post_id) {
							       echo meta_box_history::get_history_count($this->name, $this->user_id, "HTML");
							     }));
		  // Clears history after 1 day
		  meta_box_history::clear_history($this->name, $this->user_id, time() - 60 * 60 * 24);
		}
	      }
	    } else 
	      return "[votewall error='syntax error in the element definition JSON syntax]";
	}
	});
    }
  }
}
new votewall_post_type();
?>