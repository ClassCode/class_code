<?php
include_once('parcours_taxinomy.php');

/**
 * This factory defines calculation on results to the parcours taxinomy.
 *
 * @see http://wordpress.stackexchange.com/questions/28467/custom-columns-on-edit-tags-php-main-page
 * @see http://shibashake.com/wordpress-theme/modify-custom-taxonomy-columns
 *
 * \ingroup custom_post_type 
 * \extends Utility
 */
class parcours_taxinomy_result {
  /** Returns all questions of a given parcours,
   * @param $parcours_slug The parcours name.
   * @return A array with all custom type quizz posts corresponding to this parcours.
   */
  static function get_questions($parcours_slug) {
    return get_posts(array(
			   'posts_per_page'   => 10000,
			   'post_status'      => 'any',
			   'post_type' => 'quizz',
			   'tax_query' => array(array(
						      'taxonomy' => 'parcours',
						      'field' => 'slug',
						      'terms' => $parcours_slug))
			   ));
  }
  /** Returns the score of a user for a given parcours.
   * @param $user_login The user login name.
   * @param $questions The parcours name questions, as provided by <tt>parcours_taxinomy_result::get_questions($parcours_slug);</tt>.
   * @return An associative array with :
   * - <tt>true</tt> : the number of true answers.
   * - <tt>false</tt> : the number of false answers.
   * - <tt>done</tt> : the number of true or false answers.
   * - <tt>count</tt> : the total number of questions.
   * - <tt>true_ratio</tt> : the percentage (from 0% to 100%) for correct answers over the total number of answered questions.
   * - <tt>done_ratio</tt> : the percentage (from 0% to 100%) for answered questions over the total number of questions.
   */
  static function get_user_score($user_login, $questions) {
    $user = get_user_by('login', $user_login);
    $result = array("false" => 0, "true" => 0);
    foreach($questions as $question) {
      $answer = get_user_meta($user->ID, "quizz_".$question->ID."_answer_ok", true);
      if (in_array($answer, array("false", "true")))
	$result[$answer]++;
     }
    $result["done"] = $result["true"] + $result["false"];
    $result["count"] = count($questions);
    $result["true_ratio"] = $result["done"] == 0 ? 0 : 100.0 * $result["true"] / $result["done"];
    $result["done_ratio"] = $result["count"] == 0 ? 0 : 100.0 * $result["done"] / $result["count"];
    return $result;
  }
  /** Returns the score of a question for all users.
   * @param $question_id The question id.
   * @return An associative array with :
   * - <tt>true</tt> : the number of true answers.
   * - <tt>false</tt> : the number of false answers.
   * - <tt>answers</tt> : an associative array with choosen answers as key and the count for each answer as value.
   * - <tt>users</tt> : an array with all users display-name that have considered this question.
   */
  static function get_question_score($question_id) {
    $users = get_users(array(
			     'meta_key' => "quizz_".$question_id."_answer_ok"
			     ));
    $result = array("false" => 0, "true" => 0, "answers" => array(), "users" => array());
    foreach($users as $user) {
      $answer = get_user_meta($user->ID, "quizz_".$question_id."_answer_ok", true);
      if (in_array($answer, array("false", "true")))
	$result[$answer]++;
      $answer = get_user_meta($user->ID, "quizz_".$question_id."_answer", true);
      if(isset($result["answers"][$answer]))
	$result["answers"][$answer]++;
      else
	$result["answers"][$answer] = 1;
      $result['users'][] = $user->display_name;
    }
    return $result;
  }
  /** Returns the score of a parcours for all users.
   * @param $parcours_slug The parcours name.
   * @return An associative array with :
   * - <tt>true</tt> : the number of true answers.
   * - <tt>false</tt> : the number of false answers.
   * - <tt>users</tt> : an associative array with the user login as key and there score as value, 
   * as provided by get_user_score().
   * - <tt>questions</tt> : an associative array with the question id as key and there score as value, 
   * as provided by get_question_score().
   */
  static function get_parcours_score($parcours_slug) {
    $questions = parcours_taxinomy_result::get_questions($parcours_slug);
    $result = array("false" => 0, "true" => 0, "questions" => array(), "users" => array());
    foreach ($questions as $question) {
      $score = parcours_taxinomy_result::get_question_score($question->ID);
      foreach (array("false", "true") as $value)
	$result[$value] += $score[$value];
      $result["questions"][$question->ID] = $score;
      foreach($score["users"] as $user)
	if (!isset($result["users"][$user]))
	  $result["users"][$user] = parcours_taxinomy_result::get_user_score($user, $questions);
    }
    return $result;
  }
}

?>