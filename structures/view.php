<html>
  <head>
    <title>Coordinations de Class´Code</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  </head>
  <body>
    <h1>Table des contacts des coordinations</h1>
    <table>
      <tr><th>Nom</th><th>Contact</th><th>Structure</th></tr>

<?php
  include_once('./ClassCode_config_coordinations.php');
  foreach($data_coordinations as $name => $data) {
    $email = preg_replace('/@/', '-at-', $data['email']);
    echo "<tr><td>$name</td><td><a title='".$email."' href='mailto:".$email."'>".$data['who']."</a></td><td><a title='".$data['url']."' href='".$data['url']."'><img height='80' width='120' src='".$data['logo']."'/></td></tr>\n";
  }
?>

</table></body></html>
  
