<?php // Voici les donnees des coordinations
$classcodeUrl='';
if(empty($_SERVER["HTTPS"])){
  $classcodeUrl='http://';
}else{
  $classcodeUrl='https://';
}
$classcodeUrl.=$_SERVER["SERVER_NAME"];

$data_coordinations = array (
  'Région Auvergne-Rhônes-Alpes' => 
  array (
    'url' => 'https://classcode.fr',
    'logo' => $classcodeUrl.'/wp-content/themes/pixees-theme/_img/classcode_toplinkbar_blue.png',
    'who' => 'Class´Code, pour la région Auvergne-Rhônes-Alpes',
    'email' => 'classcode-accueil@inria.fr',
    'slug' => '',
    'region' => '/(auvergne|rh.ne[\s-]*alpes)/i',
    'emails' => '/.*@ac-(clermont|grenoble|lyon)\.fr/i',
    'structure' => false,
    'alert' => true,
  ),
  'Région Bretagne' => 
  array (
    'url' => 'https://classcode.fr',
    'logo' => $classcodeUrl.'/wp-content/themes/pixees-theme/_img/classcode_toplinkbar_blue.png',
    'who' => 'Class´Code, pour la région Bretagne',
    'email' => 'classcode-accueil@inria.fr',
    'slug' => '',
    'region' => '/(brittany|bretagne)/i',
    'emails' => '/.*@ac-(rennes)\.fr/i',
    'structure' => false,
    'alert' => true,
  ),
  'Région Bourgogne-Franche-Comté' => 
  array (
    'url' => 'http://www.univ-fcomte.fr/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo-UFranchecomte-150x116.jpeg',
    'who' => 'Françoise Greffier, pour la région Bourgogne-Franche-Comté',
    'email' => 'francoise.greffier@univ-fcomte.fr',
    'slug' => 'gfrancesca',
    'region' => '/(bourgogne|franche[\s-]*comt.)/i',
    'emails' => '/.*@ac-(besancon|dijon)\.fr/i',
    'structure' => false,
    'alert' => true,
  ),
  'Région Centre-Val-de-Loire' => 
  array (
    'url' => 'https://classcode.fr',
    'logo' => $classcodeUrl.'/wp-content/themes/pixees-theme/_img/classcode_toplinkbar_blue.png',
    'who' => 'Class´Code, pour la région Centre-Val-de-Loire',
    'email' => 'classcode-accueil@inria.fr',
    'slug' => '',
    'region' => '/(centre|val[\s-]*de[\s-]*loire)/i',
    'emails' => '/.*@ac-(orleans-tours|limoges)\.fr/i',
    'structure' => false,
    'alert' => true,
  ),
  'Région Grand-Est' => 
  array (
    'url' => 'http://www.lespetitsdebrouillards.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Laure Digonnet, Class´Code pour la région Grand-Est',
    'email' => 'l.digonnet@lespetitsdebrouillards.org',
    'slug' => 'laure-digonnet',
    'region' => '/(alsace|champagne|ardenne|lorraine|grand[\s-]*est)/i',
    'emails' => '/.*@ac-(nancy-metz|reims|strasbourg)\.fr/i',
    'structure' => false,
    'alert' => true,
  ),
  'Région Hauts-de-France' => 
  array (
    'url' => 'http://www.univ-lille1.fr/',
    'logo' => 'https://project.inria.fr/classcode/files/2016/07/logo-universite-lille-150x72.png',
    'who' => 'Yann Secq, Class´Code pour la région Hauts-de-France',
    'email' => 'yann.secq@univ-lille1.fr',
    'slug' => 'yann-secq',
    'region' => '/(nord|pas[\s-]*de[\s-]*calais|picardie|haut[\s-]*de[\s-]*france)/i',
    'emails' => '/.*@ac-(amiens|lille)\.fr/i',
    'structure' => false,
    'alert' => true,
  ),
  'Région Ile-de-France' => 
  array (
    'url' => 'http://simplon.co/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_simplonco-150x47.jpg',
    'who' => 'Arthur Romano, Class´Code pour la région Ile-de-France',
    'email' => 'aromano@simplon.co',
    'slug' => 'aromano@simplon.co',
    'region' => '/.le[\s-]*de[\s-]*france/i',
    'emails' => '/.*@ac-(creteil|paris|versailles)\.fr/i',
    'structure' => false,
    'alert' => true,
  ),
  'Région Normandie' => 
  array (
    'url' => 'https://classcode.fr',
    'logo' => $classcodeUrl.'/wp-content/themes/pixees-theme/_img/classcode_toplinkbar_blue.png',
    'who' => 'Class´Code, pour la région Normandie',
    'email' => 'classcode-accueil@inria.fr',
    'slug' => '',
    'region' => '/(normandy|normandie)/i',
    'emails' => '/.*@ac-(caen|rouen)\.fr/i',
    'structure' => false,
    'alert' => true,
  ),
  'Région Nouvelle-Aquitaine' =>
  array (
    'url' => 'http://emf.fr',
    'logo' => 'http://emf.fr/wp-content/uploads/2012/07/EMFlogo2016.png',
    'who' => 'Thierry Pasquier, Class´Code, pour la région Nouvelle-Aquitaine',
    'email' => 'thierry.pasquier@emf.ccsti.eu',
    'slug' => 'tpasquier',
    'region' => '/(aquitaine|limousin|poitou[\s-]*charentes|nouvelle-aquitaine)/i',
    'emails' => '/.*@ac-(bordeaux|poitiers)\.fr/i',
    'structure' => false,
    'alert' => true,
  ),   
  'Région Occitanie' => 
  array (
    'url' => 'http://lacompagnieducode.org/',
    'logo' => 'http://www.lacompagnieducode.org/images/logos/cdc.png',
    'who' => 'Valérie Letard, Class´Code pour la région Occitanie',
    'email' => 'valerie.letard@lacompagnieducode.org',
    'slug' => 'valcdc',
    'region' => '/(languedoc|roussillon|midi[\s-]*pyr.n.es|occitanie)/i',
    'emails' => '/.*@ac-(toulouse|montpellier)\.fr/i',
    'structure' => false,
    'alert' => true,
  ),   
  'Region Pays de la Loire' => 
  array (
    'url' => 'http://www.pingbase.net/',
    'logo' => 'https://project.inria.fr/classcode/files/2016/07/LOGO-PING-150x81.png',
    'who' => 'Fanny Giraudeau, Class´Code pour la région Pays de la Loire',
    'email' => 'fanny.giraudeau@pingbase.net',
    'slug' => 'fanny-ping',
    'region' => '/(pays[\s-]*de[\s-]*la[\s-]*loire)/i',
    'emails' => '/.*@ac-(nantes)\.fr/i',
    'structure' => false,
    'alert' => true,
  ),
  'Région Provence Alpes Côte d´Azur' => 
  array (
    'url' => 'http://www.regionpaca.fr/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo-Paca-150x75.jpeg',
    'who' => 'Pauline Asselin De Williencourt, Class´Code pour la région Provence Alpes Côte d´Azur',
    'email' => 'passelin@regionpaca.fr',
    'slug' => 'asselin',
    'region' => '/(provence|paca)/i',
    'emails' => '/.*@ac-(aix-marseille|nice)\.fr/i',
    'structure' => false,
    'alert' => true,
  ),
  'La Martinique' => 
  array (
    'url' => 'http://www.espe-martinique.fr/',
    'logo' => 'https://project.inria.fr/classcode/files/2016/07/LOGO-U-Antilles-250x59.png',
    'who' => 'Nathalie Methelie, Class´Code pour la Martinique',
    'email' => 'nathalie.methelie@espe-martinique.fr',
    'slug' => 'methnat',
    'region' => '/(fort[\s-]*de[\s-]*france|martinique)/i',
    'emails' => '/.*@ac-(martinique)\.fr/i',
    'structure' => false,
    'alert' => true,
  ),
  'La Corse' => 
  array (
    'url' => 'https://classcode.fr',
    'logo' => $classcodeUrl.'/wp-content/themes/pixees-theme/_img/classcode_toplinkbar_blue.png',
    'who' => 'Class´Code, pour la Corse',
    'email' => 'classcode-accueil@inria.fr',
    'slug' => '',
    'region' => '/(corsica|corse)/i',
    'emails' => '/.*@ac-(corse)\.fr/i',
    'structure' => false,
    'alert' => true,
  ),
  'Canopé' => 
  array (
    'url' => 'https://www.reseau-canope.fr/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo-Canope-150x71.jpeg',
    'who' => 'Olivier Banus, Class´Code, pour Canopé',
    'email' => 'olivier.banus@reseau-canope.fr',
    'slug' => 'obanus',
    'region' => false,
    'emails' => false,
    'structure' => '/canop./i',
    'alert' => true,
  ),
 'La Ligue de l´Enseignement' => 
  array (
    'url' => 'http://www.laligue.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2016/07/LOGO-LIGUE-BB-CMJN-150x84.png',
    'who' => 'Melpomeni Papadopoulou, Class´Code, pour La Ligue de l´Enseignement',
    'email' => 'mpapadopoulou@laligue.org',
    'slug' => '',
    'region' => false,
    'emails' => false,
    'structure' => '/ligue/i',
    'alert' => true,
  ),
  'Les ESPEs' => 
  array (
    'url' => 'http://www.univ-fcomte.fr/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo-UFranchecomte-150x116.jpeg',
    'who' => 'Françoise Greffier, pour les ESPEs',
    'email' => 'francoise.greffier@univ-fcomte.fr',
    'slug' => 'gfrancesca',
    'region' => false,
    'emails' => false,
    'structure' => '/espe/i',
    'alert' => true,
  ),
  'Les Maisons des sciences' => 
  array (
    'url' => 'http://www.maisons-pour-la-science.org',
    'logo' => 'https://project.inria.fr/classcode/files/2016/07/logo-maisons-pour-la-science-150x70.png',
    'who' => 'Claire Calmet, Class´Code, pour les maisons des sciences',
    'email' => 'claire.calmet@fondation-lamap.org',
    'slug' => 'clac',
    'region' => false,
    'emails' => false,
    'structure' => '/maison[\s-]*des[\s-]*sciences/i',
    'alert' => true,
  ),  
  'Les Médiathèques' => 
  array (
    'url' => 'http://www.ma-mediatheque.net/',
    'logo' => 'https://project.inria.fr/classcode/files/2016/07/logo-mediaheque-CASA.png',
    'who' => 'Sophie Domergue, Class´Code, pour les médiathèques',
    'email' => 's.domergue@agglo-casa.fr',
    'slug' => '',
    'region' => false,
    'emails' => false,
    'structure' => '/m.diath.eque/i',
    'alert' => true,
  ),
  'Les PEPs' => 
  array (
    'url' => 'http://www.lespep.org',
    'logo' => $classcodeUrl.'/wp-content/themes/pixees-theme/_img/classcode_pictos/logo-lespep.png',
    'who' => 'Carole Dalloz, Class´Code, pour Les PEPs',
    'email' => 'c.dalloz@lespep.org',
    'slug' => '',
    'region' => false,
    'emails' => false,
    'structure' => '/pep/i',
    'alert' => true,
  ),  
  'Les Petits Débrouillards' => 
  array (
    'url' => 'http://www.lespetitsdebrouillards.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Laure Digonnet, Class´Code, pour Les Petits Débrouillards',
    'email' => 'l.digonnet@lespetitsdebrouillards.org',
    'slug' => 'laure-digonnet',
    'region' => false,
    'emails' => false,
    'structure' => '/(les[\s-]*petits[\s-]*d.brouillards|pti.*d.b)/i',
    'alert' => true,
  ),
  'Inria' => 
  array (
    'url' => 'https://www.inria.fr/mecsci',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_inria_fr-150x54.jpg',
    'who' => 'Thierry Viéville, Class´Code, pour Inria',
    'email' => 'thierry.vieville@inria.fr',
    'slug' => 'tvieville',
    'region' => false,
    'emails' => '/.*@ac-(guyane|guadeloupe|reunion)\.fr/i',
    'structure' => '/inria/i',
    'alert' => true,
  ),
 'CERPEP' => 
  array (
    'url' => 'https://classcode.fr',
    'logo' => $classcodeUrl.'/wp-content/themes/pixees-theme/_img/classcode_toplinkbar_blue.png',
    'who' => 'Class´Code, pour le CERPEP',
    'email' => 'classcode-accueil@inria.fr',
    'slug' => 'cerpep',
    'region' => false,
    'emails' => false,
    'structure' => '/cerpep/i',
    'alert' => false,
  ),
 'CLASSCODE' => 
  array (
    'url' => 'https://classcode.fr',
    'logo' => $classcodeUrl.'/wp-content/themes/pixees-theme/_img/classcode_toplinkbar_blue.png',
    'who' => 'Class´Code, la foule',
    'email' => 'classcode-accueil@inria.fr',
    'slug' => 'classcode',
    'region' => '/.*/',
    'emails' => '/.*/',
    'structure' => '/.*/',
    'alert' => false,
  ),
);
?>