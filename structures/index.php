<?php
define('WP_USE_THEMES', false);
require_once("../../../../wp-load.php");
include_once('./ClassCode_config_coordinations.php');
include_once('../rencontre/rencontre_post_type.php'); 
include_once('../message/rencontre_messages.php'); 

$classcodeUrl='';
if(empty($_SERVER["HTTPS"])){
  $classcodeUrl='http://';
}else{
  $classcodeUrl='https://';
}
$classcodeUrl.=$_SERVER["SERVER_NAME"];

/**
 * Defines the mechanism to manage coordinators messages regarding Class'Code hybrid formation data.
 */
class user_messages {
  /** Displays the minimal dashboard */
  public static function show() {
    global $data_coordinations;
    // Ici https://pixees.fr/wp-content/plugins/class_code/structures?who=nicename permet à un administrateur de voir le tableau de bord de qq
    if (in_array('administrator', wp_get_current_user()->roles) && isset($_REQUEST['who'])) {
      $user_nicename = $_REQUEST['who'];
      $user = get_user_by('slug', $_REQUEST['who']);
      $user_email = $user->ID > 0 && !isset($_REQUEST['who']) ? $user->user_email : '';
    } else if (wp_get_current_user()->ID > 0) {
      $user_nicename = wp_get_current_user()->user_nicename;
      $user_email = wp_get_current_user()->user_email;
    } else {
      $user_nicename = false;
    }
    if ($user_nicename) {
      $nothing = true;
      $options = '';
      $output = '';
      foreach($data_coordinations as $data)
	if ($data['slug'] == $user_nicename || $data['email'] == $user_email) {
	  $nothing = false;
	  if ($data['structure'])
	    $options .= '
  <option value="rencontres-structure">La liste des rencontres de votre structure</option>
  <option value="rencontres-sans-facilitateur-structure">La liste des rencontres de votre structure sans facilitateur</option>
  <option value="participants-structure">La liste des participants de votre structure</option>';
	  if ($data['region']) 
	    $options .= '
  <option value="rencontres-region">La liste des rencontres dans votre région</option>
  <option value="rencontres-sans-facilitateur-region">La liste des rencontres dans votre région sans facilitateur</option>
  <option value="participants-region">La liste des participants dans votre région</option>';
	  if (isset($_REQUEST['what']))
	    switch($_REQUEST['what']) {
	    case "rencontres-structure" :
	      $output .= rencontre_messages::get_html_table_rencontres("de votre structure", self::get_rencontres_by(false, $data['structure']), false, true);
	      break;
	    case "rencontres-sans-facilitateur-structure" :
	      $output .= rencontre_messages::get_html_table_rencontres("de votre structure, sans facilitateur", self::get_rencontres_by(false, $data['structure'], "no"),false,true);
	    break;
	    case "participants-structure" :
	      $output .= self::get_html_table_users("de votre structure", self::get_users_by(false, $data['structure'], $data['emails']));
	      break;
	    case "rencontres-region" :
	      $output .= rencontre_messages::get_html_table_rencontres("de votre région", self::get_rencontres_by($data['region']),false,true);
	      break;
	    case "rencontres-sans-facilitateur-region" :
	      $output .= rencontre_messages::get_html_table_rencontres("de votre région, sans facilitateur", self::get_rencontres_by($data['region'], false, "no"),false,true);
	    break;
	    case "participants-region" :
	      $output .= self::get_html_table_users("de votre region", self::get_users_by($data['region'], false, $data['emails']));
	      break;
	    }
	}
      echo '<p>Bonjour '.($user_email == '' ? '<tt>'.$user_nicename.'</tt>' : '<a href="'.$GLOBALS['classcodeUrl'].'/members/'.$user_nicename.'">'.$user_nicename.'</a>').' !</p>';
      if ($options != '') {
	echo '<form style="padding:40px;" action="#" method="get"><select name="what"><option>Options de votre tableau de bord: </option>'.$options.'</select><input type="submit" value="Envoyer"/>'.(isset($_REQUEST['who']) ? '<input type="hidden" name="who" value="'.$_REQUEST['who'].'"/>' : '').'</form>';
	if (isset($_REQUEST['what']))
	  echo '<script>var options = document.getElementsByTagName("option"); for (i = 0; i < options.length; i++) if (options[i].value =="'.$_REQUEST['what'].'") options[i].selected =true; </script>';
      }
      if ($output != '')
	echo '<hr>'.$output.'<hr>';
      if ($nothing)
	echo '<p>Il n´y pas de donnée correspondant à votre login sur cette page.</p>';
    } else 
      echo '<p>Il faut être connecté à <a href="'.$GLOBALS['classcodeUrl'].'/wp-login.php">https://classcode.fr</a> pour utiliser cette page.</p>';
    }
  /** Returns a list of users given some criterion.
   * @param region A regular expression specifying the region; value is converted to lower-case and spaces are trimmed.
   * @param structure A regular expression specifying the structure; value is converted to lower-case and spaces are trimmed.
   * @param email A regular expression specifying the email; value is converted to lower-case and spaces are trimmed.
   * @param facilitateur If not false, but "yes" or "no", filters on users who are facilitator.
   */
  private static function get_users_by($region = false, $structure = false, $email = false, $facilitateur = false) {
    return array_map(function($user) { return $user->ID; }, array_filter(get_users(), function($user) use ($region, $email, $structure, $facilitateur) {
	  /*
	  if ($facilitateur && ($facilitateur == "yes") != bp_get_profile_field_data(array('field' => 'Facilitateur', 'user_id' => $user->ID)))
	    return false;
	  */
	  if ($email && preg_match($email, $user->user_email))
	    return true;
	  if ($structure && (preg_match($structure, bp_get_profile_field_data(array('field' => 'Structure', 'user_id' => $user->ID)))  ||
			     preg_match($structure, bp_get_profile_field_data(array('field' => 'Cadre', 'user_id' => $user->ID)))))
	    return true;
	  if ($region && preg_match($region, rencontre_post_type::get_location($user->ID, "members", "region")))
	    return true;
	  return false;
	}));
  }
  /** Returns a list of rencontres given some criterion.
   * @param region A regular expression specifying the region; value is converted to lower-case and spaces are trimmed.
   * @param structure A regular expression specifying the structure; value is converted to lower-case and spaces are trimmed.
   * @param facilitateur If not false, but "yes" or "no", filters on rencontres who have facilitator.
   */
  private static function get_rencontres_by($region = false, $structure = false, $facilitateur = false) {
    return array_map(function($post) { return $post['post']->ID; }, array_filter(rencontre_post_type::get_rencontres(array(), "date"), function($post) use ($region, $structure, $facilitateur) {
        $post_id = $post['post']->ID;
	$yes = false;
	if ($structure && preg_match($structure, get_post_meta($post_id, 'structure', true)))
	  $yes = true;
	if ($region && preg_match($region, rencontre_post_type::get_location($post_id, "posts", "region")))
	  $yes = true;
	if (!$yes)
	  return false;
	// Checks if there is a facilitator
	if ($facilitateur) {
	  $thereis = false;
	  foreach(explode('|', get_post_meta($post_id, 'rencontre_participants', true)) as $user_id)
	    if ($user_id != '' && bp_get_profile_field_data(array('field' => 'Facilitateur', 'user_id' => $user_id)))
	      $thereis = true;
	  return (($facilitateur == "yes") && $thereis) || (($facilitateur == "no") && !$thereis);
	}
	return true;
	}));
  }
  // Returns a table corresponding to users IDs
  public static function get_html_table_users($header, $user_ids) {
    if (count($user_ids) == 0)
      return '<p class="title">Pas de personne '.$header.' :</p>';
    $header = '<p class="title">'.(count($users) == 1 ? 'La personnes' : 'Les personnes').' '.$header.' :</p>';
    $html = '<table class="users"><tr><th>Nom, Prénom</th><th>Email</th><th>Facilitateur<br/>Organisateur<br/>Animateur<br/>Enseignant</th><th>Structure</th><th>Lieu</th><th>Inscription</th><th>Rencontres</br> Passées/Futures*</th></tr>';
    $total_count = 0; $facilitateur_count = 0; $organisateur_count = 0; $withlocation_count = 0; $profil_count = 0; $professeur_count = 0; $animateur_count = 0;
    $rencontres = array();
    {
      foreach(rencontre_post_type::get_rencontres() as $rencontre) {
	foreach(explode('|', get_post_meta($rencontre['post']->ID, 'rencontre_participants', true)) as $user_id)
	  if ($user_id != '') {
	    if (!isset($rencontres[$user_id])) $rencontres[$user_id] = array();
	    $rencontre_participants[$user_id][] = $rencontre;
	  }
	$user_id = $rencontre['post']->post_author;
	if (!isset($rencontres[$user_id])) $rencontres[$user_id] = array();
	$rencontres[$user_id][] = $rencontre;
      }
    }
    $users_data = array();
    foreach($user_ids as $user_id) {
      $user_data = array(
			 'nom' => bp_get_profile_field_data(array('field' => 'Nom', 'user_id' => $user_id)).', '.bp_get_profile_field_data(array('field' => 'Prénom', 'user_id' => $user_id)),
			 'email' => get_user_by('ID',$user_id)->user_email,
			 'structure' => bp_get_profile_field_data(array('field' => 'Structure', 'user_id' => $user_id)),
			 'lieu' => preg_replace("/\\\\*'/", "´", rencontre_post_type::get_location($user_id, "members", "address").', '.rencontre_post_type::get_location($user_id, "members", "region")),
			 'slug' => get_user_by('id', $user_id)->user_nicename,
			 'facilitateur' => bp_get_profile_field_data(array('field' => 'Facilitateur', 'user_id' => $user_id)),
			 'organisateur' => count(rencontre_post_type::get_rencontres(array('mes_propres_rencontres' => true, 'user_id' => $user_id))) > 0,
			 'profil' => bp_get_profile_field_data(array('field' => 'Profil', 'user_id' => $user_id)),
			 'inscription-date' => strtotime(get_userdata($user_id)->user_registered),
			 'modules' => "",
			 );
      $user_data['animateur'] = preg_match('/animateur/i', $user_data['profil']);
      $user_data['enseignant'] = preg_match('/professeur/i', $user_data['profil']);
      if (is_array($rencontres[$user_id]))
	foreach($rencontres[$user_id] as $rencontre) {
	  if ($rencontre['past']) 
	    $user_data['modules'] .= " #".$rencontre['module_id']."-P";
	  if ($rencontre['future']) 
	    $user_data['modules'] .= " #".$rencontre['module_id']."-F";
	}
      $users_data[] = $user_data;
    }
    usort($users_data, function($m1, $m2) { return $m2['inscription-date'] - $m1['inscription-date']; }); 
    foreach($users_data as $user_data) {
      // Performs some counts
      {
	$total_count++; 
	if ($user_data['facilitateur']) $facilitateur_count++; 
	if ($user_data['organisateur']) $organisateur_count++;
	if (trim($user_data['lieu']) != ',') $withlocation_count++;
	if (trim($user_data['profil']) != '') $profil_count++;
	if ($user_data['animateur']) $animateur_count++;
	if ($user_data['enseignant']) $professeur_count++;
      }
      $html .= '<tr><td><a href="'.$GLOBALS['classcodeUrl'].'/members/'.$user_data['slug'].'">'.($user_data['nom'] == ', ' ? $user_data['slug'] : $user_data['nom']).'</a></td><td>'.$user_data['email'].'</td><td align="center">'.($user_data['organisateur'] ? ' <b>O</b> ' : '').($user_data['facilitateur'] ? ' <b>F</b> ' : '').($user_data['animateur'] ? ' <b>A</b> ' : '').($user_data['enseignant'] ? ' <b>E</b> ' : '').'</td><td>'.$user_data['structure'].'</td><td>'.$user_data['lieu'].'</td><td>'.date("Y/m/d", $user_data['inscription-date']).'</td><td>'.$user_data['modules'].'</td></tr>';
    }
    return $header.'<ul>
<li>Nombre de personnes ayant renseigné un lieu : '.$withlocation_count.' / '.$total_count.' '.round(100.0*$withlocation_count/$total_count).'%</li>
<li>Nombre de personnes facilitatrices : '.$facilitateur_count.' / '.$withlocation_count.' '.round(100.0*$facilitateur_count/$withlocation_count).'%</li>
<li>Nombre de personnes organisatrices : '.$organisateur_count.' / '.$withlocation_count.' '.round(100.0*$organisateur_count/$withlocation_count).'%</li>
<li>Nombre de personnes ayant renseigné un profil : '.$profil_count.' / '.$total_count.' '.round(100.0*$profil_count/$total_count).'%</li>
<li>Nombre de personnes animateures : '.$animateur_count.' / '.$profil_count.' '.round(100.0*$animateur_count/$profil_count).'%</li>
<li>Nombre de personnes professeures : '.$professeur_count.' / '.$profil_count.' '.round(100.0*$professeur_count/$profil_count).'%</li>
</ul><hr/>'.$html.'</table>(*) Numéro de module de la rencontre, 0 si autre rencontre. P pour passée, F pour future.';
  }
}
?>

<?php header('X-Frame-Options: GOFORIT'); ?>
<?php include_once(get_template_directory().'/_inc/display-functions.php'); ?>
<!DOCTYPE html>
<html>
 <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>"/>    
  <title>Tableau de bord Class'Code</title>
  <link href="<?php the_theme_file('/classcode.css');?>" type="text/css" rel="stylesheet" />
  <?php wp_head(); ?>
</head>
<body id="body" style="padding:0px;margin-left:auto;margin-right:auto;background-color:white">
<img style="width:100%" src="<?php echo $classcodeUrl; ?>/wp-content/plugins/class_code/message/header.png" />
<div style="padding:0px 10px;">
  <?php user_messages::show(); ?>
</div>
<img style="display:block;width:90%;margin-left:auto;margin-right:auto;" src="<?php echo $classcodeUrl; ?>/wp-content/plugins/class_code/message/trailer.png" />
</body></html>

