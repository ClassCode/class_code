<?php

function update_data_structure() {

  // Gets the google spreadsheet
  $data_structure_csv = file_get_contents('https://docs.google.com/spreadsheets/d/1Gg4BmcgBGbuJTyj1XMoT74X5mefSZVVzyBXQ6k108LU/export?exportFormat=csv');

  // Converts as an array
  $data_structure_array = str_getcsv($data_structure_csv, "\n");
  foreach($data_structure_array as &$row) $row = array_map(function($value) { return trim($value); }, str_getcsv($row));

  // Detects the config file fields
  $fields = array("name" => "Nom de la structure", "logo" => "Logo de la structure", "url" => "Site Web", "who" => "Nom, Prénom du contact", "email" => "Email pour contacter la structure");
  foreach($fields as $name => &$index) 
    if (!($index = array_search($index, $data_structure_array[0])))
      echo "Aie : le champ de nom $index n'est plus dans le spreadshee google : qq chose a du changer, il faut revoir cet intergiciel\n";

  // Generates the structure
  $data_structure = array();
  for($ii = 1; $ii < count($data_structure_array); $ii++) 
    if ($data_structure_array[$ii][$fields['name']] != '')
      $data_structure[$data_structure_array[$ii][$fields['name']]] = array(
        "url" => $data_structure_array[$ii][$fields['url']],
	"logo" => $data_structure_array[$ii][$fields['logo']],
	"who" => $data_structure_array[$ii][$fields['who']],
	"email" => $data_structure_array[$ii][$fields['email']]);
  ksort($data_structure);

  // Saves and checks
  file_put_contents("./ClassCode_config_structures.php", 
'<?php // AUTOMATICALLY GENERATED FROM update_data_structure.php DO NOT EDIT
$data_structures = '.var_export($data_structure, true).'
?>');
  system("php -l ./ClassCode_config_structures.php");
  
  // Displays as an HTML array
  $html = '<table>';
  foreach($data_structure as $name => $item)
    $html .= '<tr><td>'.$name.'</td><td><a href="mailto:'.$item['email'].'">'.$item['who'].'</a></td><td><a href="'.$item['url'].'"><img height="40px" src="'.$item['logo'].'"/></a></td></tr>';
  $html .= '</table>';
  return $html;
}
?>
