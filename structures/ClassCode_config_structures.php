<?php // AUTOMATICALLY GENERATED FROM update_data_structure.php DO NOT EDIT
$data_structures = array (
  'Atelier Canopé 13-Marseille' => 
  array (
    'url' => 'http://www.cndp.fr/crdp-aix-marseille/spip.php?rubrique107',
    'logo' => 'http://www.cndp.fr/crdp-aix-marseille/IMG/jpg/logocanopeblanc.jpg',
    'who' => 'Nicolas, Michel',
    'email' => 'contact.atelier13@reseau-canope.fr',
  ),
  'Atelier Canopé Strasbourg' => 
  array (
    'url' => 'http://www.crdp-strasbourg.fr/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo-Canope-150x71.jpeg',
    'who' => 'Boulom, Laetitia',
    'email' => 'laetitia.boulom@reseau-canope.fr',
  ),
  'Atelier Canopé de la Mayenne' => 
  array (
    'url' => 'http://www.crdp-nantes.fr/canope-academie-de-nantes/se-former/accompagnements-formations-et-rencontres/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo-Canope-150x71.jpeg',
    'who' => 'Guitterb,',
    'email' => 'guitterb@gmail.com',
  ),
  'Atelier Canopé d´Amiens' => 
  array (
    'url' => 'https://canope.ac-amiens.fr/tice/',
    'logo' => 'http://www.cndp.fr/crdp-amiens/squelettes/crdp/images/canope-amiens.jpg',
    'who' => 'Olivier, Jean-Christophe',
    'email' => 'jean-christophe.olivier@reseau-canope.fr',
  ),
  'Atelier Canopé – 06' => 
  array (
    'url' => 'https://www.reseau-canope.fr/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo-Canope-150x71.jpeg',
    'who' => 'Banus, Olivier',
    'email' => 'olivier.banus@reseau-canope.fr',
  ),
  'Atelier Canopé – Evry' => 
  array (
    'url' => 'http://cddp91.ac-versailles.fr/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo-Canope-150x71.jpeg',
    'who' => 'Irbah, Mouloud',
    'email' => 'mouloud.irbah@reseau-canope.fr',
  ),
  'CEMEA Alsace' => 
  array (
    'url' => 'http://cemea-alsace.fr/',
    'logo' => 'http://cemea-alsace.fr/docs_pdf/cemea/logo_cemea_alsace.png',
    'who' => 'Fiegel, C.F.',
    'email' => 'cf.fiegel@gmail.com',
  ),
  'Canopé Beauvais' => 
  array (
    'url' => 'http://www.cndp.fr/crdp-amiens/cddpoise/',
    'logo' => 'https://www.reseau-canope.fr/fileadmin/template/images/logoReseauCanope.jpg',
    'who' => 'Jakobowski, Patrick',
    'email' => 'Patrick.jakobowski@reseau-canope.fr',
  ),
  'Canopé Guyanne' => 
  array (
    'url' => 'https://www.reseau-canope.fr/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo-Canope-150x71.jpeg',
    'who' => 'Jean-Luc, Gnocchi',
    'email' => 'jean-luc.gnocchi@ac-guyane.fr',
  ),
  'Canopé Troyes' => 
  array (
    'url' => 'http://www.cndp.fr/crdp-reims/index.php?id=1113',
    'logo' => 'https://www.reseau-canope.fr/fileadmin/template/images/logoReseauCanope.jpg',
    'who' => 'Lhomme, Catherine',
    'email' => 'catherine.lhomme@reseau-canope.fr',
  ),
  'Canopé des Vosges' => 
  array (
    'url' => 'http://www.cndp.fr/crdp-nancy-metz/88/accueil.html',
    'logo' => 'http://www.cndp.fr/crdp-nancy-metz/fileadmin/template/harmonisation/img/logo_canope.png',
    'who' => 'Chalifour, Samuel',
    'email' => 'samuel@chalifour.fr',
  ),
  'Centre Social du Chemillois' => 
  array (
    'url' => 'http://centresocial-chemille.asso.fr/',
    'logo' => 'http://www.cc-region-chemille.fr/mediatheque/images/st_lezin/f-r-saint-lezin/csc.jpg',
    'who' => 'Godereau, Yvan',
    'email' => 'yvan.godreau@centresocial-chemille.asso.fr',
  ),
  'Club des programmeurs écrivains et de la culture des jeux vidéo' => 
  array (
    'url' => '',
    'logo' => '',
    'who' => 'Drouillon, Frédéric',
    'email' => 'fdr@free.fr',
  ),
  'Code Club France' => 
  array (
    'url' => 'http://www.clubcode.org/',
    'logo' => 'https://static.wixstatic.com/media/4bf55a_201520f623cd44309828f1efd227b0d2.png/v1/fill/w_141,h_141,al_c,usm_0.66_1.00_0.01/4bf55a_201520f623cd44309828f1efd227b0d2.png',
    'who' => 'Schuft, Fabien',
    'email' => 'fschuft@udaf10.com',
  ),
  'Cybercentre du Pays de Pouzauges' => 
  array (
    'url' => 'http://cybercentre.paysdepouzauges.fr',
    'logo' => 'http://cybercentre.paysdepouzauges.fr/wp-content/uploads/2016/10/Logo_Cybercentre_Pays_Pouzauges.png',
    'who' => 'Morin, Laurent',
    'email' => 'l.morin@paysdepouzauges.fr',
  ),
  'ESPE de Martinique' => 
  array (
    'url' => 'http://www.espe-martinique.fr/',
    'logo' => 'https://project.inria.fr/classcode/files/2016/07/LOGO-U-Antilles-250x59.png',
    'who' => 'Methelie, Nathalie',
    'email' => 'nathalie.methelie@espe-martinique.fr',
  ),
  'Ecole Normale Supérieure de Rennes' => 
  array (
    'url' => 'http://www.ens-rennes.fr/',
    'logo' => 'http://www.ens-rennes.fr/images/principal/logo.png',
    'who' => 'Santiago, Bautista',
    'email' => 'santiago.bautista@ens-rennes.fr',
  ),
  'Espace Mendès France' => 
  array (
    'url' => 'http://emf.fr',
    'logo' => 'http://emf.fr/wp-content/uploads/2012/07/EMFlogo2016.png',
    'who' => 'Pasquier, Thierry',
    'email' => 'thierry.pasquier@emf.ccsti.eu',
  ),
  'Hatlab' => 
  array (
    'url' => 'http://www.hatlab.fr',
    'logo' => 'https://project.inria.fr/classcode/files/2016/10/logo-hatlab-.png',
    'who' => 'Lalanne, Francis',
    'email' => 'lalannefrancis1@gmail.com',
  ),
  'Informatique Lycée' => 
  array (
    'url' => 'http://informatiquelycee.fr',
    'logo' => 'http://informatiquelycee.fr/img/reseau.jpg',
    'who' => 'Roche, David',
    'email' => 'david.roche@ac-grenoble.fr',
  ),
  'Inria' => 
  array (
    'url' => 'https://www.inria.fr/mecsci',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_inria_fr-150x54.jpg',
    'who' => 'Viéville, Thierry',
    'email' => 'thierry.vieville@inria.fr',
  ),
  'Inria Grenoble - Rhônes Alpes' => 
  array (
    'url' => 'https://www.inria.fr/centre/grenoble/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_inria_fr-1024x369.jpg',
    'who' => 'Collin, Marie',
    'email' => 'Marie.Collin@inria.fr',
  ),
  'Inria, Bordeaux - Sud-Ouest' => 
  array (
    'url' => 'https://www.inria.fr/centre/bordeaux',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_inria_fr-1024x369.jpg',
    'who' => 'Courbon, Martine',
    'email' => 'martine.courbin@inria.fr',
  ),
  'Inria, Sophia Antipolis - Méditerranée' => 
  array (
    'url' => 'https://www.inria.fr/centre/sophia/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_inria_fr-1024x369.jpg',
    'who' => 'François, Valérie',
    'email' => 'Valerie.Francois@inria.fr',
  ),
  'La Cantine d´Aubuisson' => 
  array (
    'url' => 'http://lacantine-toulouse.org/',
    'logo' => 'http://www.lacompagnieducode.org/images/logos/cdc.png',
    'who' => 'Fauck, Thierry',
    'email' => 'thierry.fauck@fr.ibm.com',
  ),
  'La Compagnie du Code' => 
  array (
    'url' => 'http://lacompagnieducode.org/',
    'logo' => 'http://www.lacompagnieducode.org/images/logos/cdc.png',
    'who' => 'Decoster, Nicolas',
    'email' => 'nicolas.decoster@lacompagnieducode.org',
  ),
  'La Digitale Académie' => 
  array (
    'url' => 'http://lda.ma/',
    'logo' => 'http://lda.ma/wp-content/uploads/2016/05/Logo-LDA-2016.png',
    'who' => 'El Kettani, Mehdi',
    'email' => 'Mehdi@lda.ma',
  ),
  'Les PEPs 21' => 
  array (
    'url' => 'http://www.lespep.org',
    'logo' => 'https://pixees.fr/wp-content/themes/pixees-theme/_img/classcode_pictos/logo-lespep.png',
    'who' => 'Dalloz, Carole',
    'email' => 'c.dalloz@lespep.org',
  ),
  'Les PEPs 28' => 
  array (
    'url' => 'http://www.lespep.org',
    'logo' => 'https://pixees.fr/wp-content/themes/pixees-theme/_img/classcode_pictos/logo-lespep.png',
    'who' => 'Dalloz, Carole',
    'email' => 'c.dalloz@lespep.org',
  ),
  'Les PEPs 44' => 
  array (
    'url' => 'http://www.lespep.org',
    'logo' => 'https://pixees.fr/wp-content/themes/pixees-theme/_img/classcode_pictos/logo-lespep.png',
    'who' => 'Dalloz, Carole',
    'email' => 'c.dalloz@lespep.org',
  ),
  'Les PEPs 57' => 
  array (
    'url' => 'http://www.lespep.org',
    'logo' => 'https://pixees.fr/wp-content/themes/pixees-theme/_img/classcode_pictos/logo-lespep.png',
    'who' => 'Dalloz, Carole',
    'email' => 'c.dalloz@lespep.org',
  ),
  'Les PEPs 66' => 
  array (
    'url' => 'http://www.lespep.org',
    'logo' => 'https://pixees.fr/wp-content/themes/pixees-theme/_img/classcode_pictos/logo-lespep.png',
    'who' => 'Dalloz, Carole',
    'email' => 'c.dalloz@lespep.org',
  ),
  'Les Petits Débrouillards - Antenne Tourcoing' => 
  array (
    'url' => 'http://www.lespetitsdebrouillards.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Malhaprez, Adrien',
    'email' => 'a.malhaprez@lespetitsdebrouillards.org',
  ),
  'Les Petits Débrouillards - Antenne de Quimper' => 
  array (
    'url' => 'https://www.facebook.com/LesPetitsDebrouillardsSudFinistere',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Mariez, Matthieu',
    'email' => 'm.mariez@debrouillonet.org',
  ),
  'Les Petits Débrouillards - Antennes de Lorient et Vannes' => 
  array (
    'url' => 'http://www.lespetitsdebrouillardsbretagne.org/-56-Morbihan-.html',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Manneville, Claire',
    'email' => 'c.manneville@debrouillonet.org',
  ),
  'Les Petits Débrouillards - Grand Besançon' => 
  array (
    'url' => 'http://www.lespetitsdebrouillards.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Djameï, Aurélien',
    'email' => 'a.djamei@debrouillonet.org',
  ),
  'Les Petits Débrouillards - Ile de France' => 
  array (
    'url' => 'http://www.lespetitsdebrouillards-idf.org',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'El Aidy, Tamer',
    'email' => 't.elaidy@lespetitsdebrouillards-idf.org',
  ),
  'Les Petits Débrouillards Grand Ouest' => 
  array (
    'url' => 'http://lespetitsdebrouillardsgrandouest.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Auffret, Antony',
    'email' => 'aauffret@lespetitsdebrouillards.org',
  ),
  'Les Petits Débrouillards Grand Ouest - Antenne d\'Angers' => 
  array (
    'url' => 'http://www.lespetitsdebrouillards-paysdelaloire.org/-49-Maine-et-Loire-.html',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Sersiron, Pierre',
    'email' => 'p.sersiron@lespetitsdebrouillards.org',
  ),
  'Les Petits Débrouillards Grand Ouest - Antenne de Saint-Brieuc' => 
  array (
    'url' => 'http://www.lespetitsdebrouillardsbretagne.org',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Briens, Christophe',
    'email' => 'cbriens@lespetitsdebrouillards.org',
  ),
  'Les Petits Débrouillards Grand Ouest - Antenne de la Sarthe' => 
  array (
    'url' => 'http://www.lespetitsdebrouillards-paysdelaloire.org',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Raimbault, Corinne',
    'email' => 'c.raimbault@lespetitsdebrouillards.org',
  ),
  'Les Petits Débrouillards Grand Ouest - antenne de Nantes' => 
  array (
    'url' => 'http://www.lespetitsdebrouillards.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Hegic, Jadranka',
    'email' => 'j.hegic@lespetitsdebrouillards.org',
  ),
  'Les Petits Débrouillards Grand-Est' => 
  array (
    'url' => 'http://www.lespetitsdebrouillards.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Digonnet, Laure',
    'email' => 'l.digonnet@lespetitsdebrouillards.org',
  ),
  'Les Petits Débrouillards Grand-Est - Antenne Epinal' => 
  array (
    'url' => 'http://www.lespetitsdebrouillards.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Demonet, Arthur',
    'email' => 'a.demonet@lespetitsdebrouillards.org',
  ),
  'Les Petits Débrouillards Grand-Est - Antenne Grand-Troyes' => 
  array (
    'url' => 'http://www.lespetitsdebrouillards.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Digonnet, Laure',
    'email' => 'l.digonnet@lespetitsdebrouillards.org',
  ),
  'Les Petits Débrouillards Grand-Est - Antenne Nancy Couronne' => 
  array (
    'url' => 'http://www.lespetitsdebrouillards.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Bardon, Camille',
    'email' => 'c.bardon@debrouillonet.org',
  ),
  'Les Petits Débrouillards Grand-Est - Atelier Les Beaux Boulons' => 
  array (
    'url' => 'http://www.lespetitsdebrouillards.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Digonnet, Laure',
    'email' => 'l.digonnet@lespetitsdebrouillards.org',
  ),
  'Les Petits Débrouillards Grand-Est - GEN Metz' => 
  array (
    'url' => 'http://www.lespetitsdebrouillards.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Digonnet, Laure',
    'email' => 'l.digonnet@lespetitsdebrouillards.org',
  ),
  'Les Petits Débrouillards Grand-Est - GEN Nancy' => 
  array (
    'url' => 'http://www.lespetitsdebrouillards.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Digonnet, Laure',
    'email' => 'l.digonnet@lespetitsdebrouillards.org',
  ),
  'Les Petits Débrouillards Grand-Est - GEN Thionville' => 
  array (
    'url' => 'http://www.lespetitsdebrouillards.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Digonnet, Laure',
    'email' => 'l.digonnet@lespetitsdebrouillards.org',
  ),
  'Les Petits Débrouillards Grand-Ouest - Antenne de Brest' => 
  array (
    'url' => 'http://lespetitsdebrouillardsgrandouest.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Auffret, Antony',
    'email' => 'aauffret@lespetitsdebrouillards.org',
  ),
  'Les Petits Débrouillards PACA' => 
  array (
    'url' => 'http://www.lespetitsdebrouillardspaca.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Sanchez, Yann',
    'email' => 'y.sanchez@lespetitsdebrouillards.org',
  ),
  'Les Petits Débrouillards Poitou Charentes' => 
  array (
    'url' => 'http://www.lespetitsdebrouillardspc.org',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Remaud, Guillaume',
    'email' => 'guillaume.remaud@lespetitsdebrouillardspc.org',
  ),
  'Les Petits Débrouillards – Antenne e Saint nazaire' => 
  array (
    'url' => 'http://www.lespetitsdebrouillards.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_lesptitdeb-150x75.png',
    'who' => 'Breyne, Jacques',
    'email' => 'j.breyne@orange.fr',
  ),
  'Lycée franco-australien/Telopea park school de Canberra' => 
  array (
    'url' => 'http://www.telopea.act.edu.au/fr',
    'logo' => 'http://lfac.lyceefrancoaustralien-efs.org/wp-content/uploads/2017/02/TPS-Logo.png',
    'who' => 'Navaux, Christophe',
    'email' => 'chris.navaux@gmail.com',
  ),
  'Maison pour la science Nord - Pas-de-Calais' => 
  array (
    'url' => 'http://www.maisons-pour-la-science.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2016/07/logo-maisons-pour-la-science-150x70.png',
    'who' => 'Luu, Thi-Lan',
    'email' => 'thi-lan.luu@maisons-pour-la-science.org',
  ),
  'Maison pour la science en Alpes-Dauphiné' => 
  array (
    'url' => 'http://www.maisons-pour-la-science.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2016/07/logo-maisons-pour-la-science-150x70.png',
    'who' => 'Geronimi, Alix',
    'email' => 'alix.geronimi@maisons-pour-la-science.org',
  ),
  'Maison pour la science en Bretagne' => 
  array (
    'url' => 'http://www.maisons-pour-la-science.org/',
    'logo' => 'https://project.inria.fr/classcode/files/2016/07/logo-maisons-pour-la-science-150x70.png',
    'who' => 'Fontaine, Laurence',
    'email' => 'laurence.fontaine@maisons-pour-la-science.org',
  ),
  'Maison pour la science en Centre-Val de Loire' => 
  array (
    'url' => 'http://www.maisons-pour-la-science.org/cvl',
    'logo' => 'https://project.inria.fr/classcode/files/2016/07/logo-maisons-pour-la-science-150x70.png',
    'who' => 'Faux, Allain-Gérald',
    'email' => 'allain-gerald.faux@maisons-pour-la-science.org',
  ),
  'Maison pour la science en Lorraine' => 
  array (
    'url' => 'http://www.maisons-pour-la-science.org/fr/lorraine',
    'logo' => 'https://project.inria.fr/classcode/files/2016/07/logo-maisons-pour-la-science-150x70.png',
    'who' => 'Rossignon, Jean-Paul',
    'email' => 'jean-paul.rossignon@univ-lorraine.fr',
  ),
  'Médiathèque de Sèvres' => 
  array (
    'url' => 'http://mediatheque.sevres.fr',
    'logo' => 'https://upload.wikimedia.org/wikipedia/commons/9/91/Blason_S%C3%A8vres_92.svg',
    'who' => 'Lozes, Étienne',
    'email' => 'lozes@lsv.fr',
  ),
  'Pasc@line' => 
  array (
    'url' => 'http://www.assopascaline.fr',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo-Pascaline-150x47.jpeg',
    'who' => 'Colmant, Christian',
    'email' => 'christian.colmant@assopascaline.fr',
  ),
  'PiNG' => 
  array (
    'url' => 'http://www.pingbase.net/',
    'logo' => 'https://project.inria.fr/classcode/files/2016/07/LOGO-PING-150x81.png',
    'who' => 'Giraudeau, Fanny',
    'email' => 'Fanny.Giraudeau@univ-nantes.fr',
  ),
  'Région Provence Alpes Côte d\'Azur' => 
  array (
    'url' => 'http://www.regionpaca.fr/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo-Paca-150x75.jpeg',
    'who' => 'Asselin De Williencourt, Pauline',
    'email' => 'passelin@regionpaca.fr',
  ),
  'Simplon.co' => 
  array (
    'url' => 'http://simplon.co/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo_simplonco-150x47.jpg',
    'who' => 'Romano, Arthur',
    'email' => 'aromano@simplon.co',
  ),
  'Sqylab' => 
  array (
    'url' => 'http://www.sqylab.org',
    'logo' => 'https://project.inria.fr/classcode/files/2016/10/logo-sqylab.jpg',
    'who' => 'Lalanne, Francis',
    'email' => 'sqylab@hatlab.fr',
  ),
  'Université Evry-Val d\'Essonne' => 
  array (
    'url' => 'http://www.univ-evry.fr/fr/index.html',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo-UEvry-144x144.jpeg',
    'who' => 'Hutzler, Guillaume',
    'email' => 'guillaume.hutzler@univ-evry.fr',
  ),
  'Université Lille 1' => 
  array (
    'url' => 'http://www.univ-lille1.fr/',
    'logo' => 'https://project.inria.fr/classcode/files/2016/07/logo-universite-lille-150x72.png',
    'who' => 'Secq, Yann',
    'email' => 'yann.secq@univ-lille1.fr',
  ),
  'Université de Franche-Comté' => 
  array (
    'url' => 'http://www.univ-fcomte.fr/',
    'logo' => 'https://project.inria.fr/classcode/files/2011/12/Logo-UFranchecomte-150x116.jpeg',
    'who' => 'Greffier, Françoise',
    'email' => 'francoise.greffier@univ-fcomte.fr',
  ),
)
?>