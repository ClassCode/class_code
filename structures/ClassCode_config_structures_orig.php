<?php // AUTOMATICALLY GENERATED DO NOT EDIT
$data_structures = array(
  "Inria" => 
  array(
	"logo" => "https://pixees.fr/wp-content/themes/pixees-theme/_img/classcode_pictos/logo-inria.png",
	"url" => "https://www.inria.fr/mecsci",
	"who" => "Thierry Viéville",
	"email" => "thierry.vieville@inria.fr",
	),
  "Atelier Canopé 06" => "Canopé",
  "Canopé" => 
  array(
	"logo" => "https://pixees.fr/wp-content/themes/pixees-theme/_img/classcode_pictos/logo-canope.png",
	"url" => "https://www.reseau-canope.fr/",
	"who" => "Olivier Banus",
	"email" => "olivier.banus@reseau-canope.fr",
	),
  "Les Petits Débrouillards" =>
  array(
	"logo" => "https://pixees.fr/wp-content/themes/pixees-theme/_img/classcode_pictos/logo-lesptitsdeb.png",
	"url" => "http://www.lespetitsdebrouillards.org",
	"who" => "Laure Digonnet",
	"email" => "l.digonnet@lespetitsdebrouillards.org",
	),
  "Les PEPs 21" => "Les PEPs",
  "Les PEPs 28" => "Les PEPs",
  "Les PEPs 44" => "Les PEPs",
  "Les PEPs 57" => "Les PEPs",
  "Les PEPs 66" => "Les PEPs",
  "Les PEPs" =>
  array(
	"logo" => "https://pixees.fr/wp-content/themes/pixees-theme/_img/classcode_pictos/logo-lespep.png",
	"url" => "http://www.lespep.org",
	"who" => "Carole Dalloz",
	"email" => "c.dalloz@lespep.org",
	),
);
?>