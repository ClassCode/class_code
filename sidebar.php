<aside id="sidebar">

<?php
   // Displays the user menu if connected
  if (is_user_logged_in()) {
    include_once('posttype/include_post_type.php');
    echo "<section><br/><h1><a href='".get_site_url()."?cc'> Mon menu</a></h1><ul>";
     echo "<li><h3><a target='_blank' href=' ".get_site_url()."/wp-admin/profile.php' title='Mon profil'>Mon profil</a></h3></li>";
   echo "<li><h3><a href='".include_post_type::get_link("userprofile/class_code_user_basket_menu")."' title='Panier de liens'>Mon panier de liens</a></h3></li>";
    echo "<li><h3><a href='".include_post_type::get_link("userprofile/class_code_user_avatar_menu")."' title='Mon avatar'>Mon avatar</a></h3></li>";
    echo "<li><h3><a href='".include_post_type::get_link("userprofile/class_code_user_data")."' title='Mes données'>Mes données</a></h3></li>";
    echo "<li><a href='".wp_logout_url(get_permalink())."' title='Me déconnecter'>Me déconnecter</a></li>";
    echo "</ul></section><div id='line-sidebar'></div>";
  } else {
    echo "<div style='clear:both;width:100%;text-align:center;'><a class='button' href='".wp_login_url(get_permalink())."' title='Login'>Je me connecte</a></div><div id='line-sidebar'></div>";
  }
?>

  <section style="margin:10px auto 10px 0px;">
   <h1>À votre écoute</h1>
   <ul style="width:100%" id="contact-bar">
     <li><a href="mailto:pixees-accueil@inria.fr?subject=Ma%20question%20%C3%A0%20propos%20de%20Class´Code&amp;body=%20Ma%20question%20:%20%0A%0A%20Qui%20je%20suis%20%20:" id="mail"></a></li>
     <!--li><a href="javascript:alert('+33 492687688');" title="+33 4 92 68 76 88" id="tel"></a></li-->
    </ul>
    <p>Je ne sais pas bien ce que je cherche ou je ne le trouve pas ? J'ai des idées, des propositions, des questions ? J'aimerais être accompagner ou participer ? Class´Code est au contact !</p>
</section><div id="line-sidebar"></div>  

<section><?php 
 {
   // Displays the user avatar if connected
  if (is_user_logged_in()) {
    include_once(plugin_dir_path( __FILE__ ).'./userprofile/class_code_user_avatar_echo.php');
    class_code_user_avatar_echo();
    echo "<div style='clear:both;width:100%;text-align:center;'><tt>".wp_get_current_user()->display_name."</tt> est connecté.</div>";
  }
}
?></section><div id="line-sidebar"></div>

  <section style="margin:10px auto 10px 0px;">
   <h1>Version de test!</h1>
    <a href="mailto:pixees-accueil@inria.fr?subject=Ma%20question%20%C3%A0%20propos%20de%20l´espace%20Class´Code&amp;body=%20Ma%20question%20:%20%0A%0A%20Qui%20je%20suis%20%20:" id="mail"></a>
    À utiliser avec précaution. Faites nous tous vos retours sur ces éléments, cela aidera.
</section><div id="line-sidebar"></div>  

<section>
  <div style="margin:10px auto 0px 20px;"><a target="_blank" href="https://classcode.fr" style="display:inline-block; width:185px; height:70px; background:url('<?php echo get_site_url(); ?>/wp-content/plugins/class_code/doc/LOGO-ClassCode-coul-small.jpg') 0 0 no-repeat;"></a></div>
  <div style="margin:10px auto 0px 20px;width:190px; height:110px; background:url('<?php echo get_site_url(); ?>/wp-content/plugins/class_code/doc/Logo-PIA-CDC.png') 0 0 no-repeat;"></div><div>Class´Code est soutenu au titre du Programme d’Investissements d’Avenir dont la Caisse des Dépôts est opérateur.</div>
</section><div id="line-sidebar"></div>

</aside>
