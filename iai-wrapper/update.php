<?php
{
  // Gdrive arrays IDs
  $data_sheets_IDS = array(
		       "Questionnements" => "1IkqZP0WwdttFcMYVEn53DzpnaTESKtZYFqhJ6hA46jw",
		       "Événements" => "1lFRQpuBVGoLg0Sj90nHjHsxKMFzmHv6oC7X0AT3AmmM",
		       "Ressources" => "1xlI91FqGZ1XLltIcPe5_3IvASOVbSWKC5sdlGxrfWVk",
		       "Partenaires" => "1f18x3mIHlNXpFNRlMfJA6YGmPLbil5WxP8L71jlS2Fg",
		       );
  // Gdrive arrays displayed fields
  $data_sheets_fields = array(
			      "Questionnements" => array("Catégorie", "Domaine", "Question"),
			      "Événements" => array("Titre", "Type", "Cible", "Date", "Lieu", "Contact", "URL"),
			      "Ressources" => array("Titre", "URL", "URL2", "Auteur", "Éditeur", "Langue", "Objectif didactique ou pédagogique"),
			      "Partenaires" => array("Nom", "URL", "Contact"),
			      );	
  // Returns the value the record of the given $index for the given $fieldName in the given $sheetName
  function get_data($sheetName, $fieldName, $index) {
    global $data_csv_contents, $data_sheets_fields_index;
    if (array_key_exists($sheetName, $data_csv_contents) && array_key_exists($index+1, $data_csv_contents[$sheetName]) &&
	array_key_exists($sheetName, $data_sheets_fields_index) && array_key_exists($fieldName, $data_sheets_fields_index[$sheetName]) &&
	array_key_exists($data_sheets_fields_index[$sheetName][$fieldName], $data_csv_contents[$sheetName][$index+1])) {
      return $data_csv_contents[$sheetName][$index+1][$data_sheets_fields_index[$sheetName][$fieldName]];
    } else {
      return "";
    }
  }
  // Returns the value the record of the given $index for the given $fieldName in the given $sheetName
  function get_data_count($sheetName) {
    global $data_csv_contents, $data_sheets_fields_index;
    return count($data_csv_contents[$sheetName]) - 1;
  }
  $data_csv_contents = array();
  $data_sheets_fields_index = array();
  // Loads the sheets and generates the indexes
  {
    // Gets a CSV file and properly manages \n in enclosure
    function file_get_csv_contents($location, $delimiter = ',', $enclosure = '"', $escape = '\\') {
      $text = file_get_contents($location);
      for($in = false, $ii = 0; $ii < strlen($text); $ii++) {
	if (!$in && $text[$ii] == $enclosure) {
	  $in = true;
	} else if ($in && $text[$ii] == $enclosure && ($ii > 0 && $text[$ii-1] != $escape)) {
	  $in = false;
	}
	if ($in && $text[$ii] == "\n") $text[$ii] = "\v";
      }
      $data = array_map('str_getcsv', explode("\n", $text));
      foreach($data as &$row) foreach($row as &$cell) $cell = str_replace("\v", "\n", $cell);
      return $data;
    }
    foreach($data_sheets_IDS as $sheetName => $sheetID) {
      // Loads the contents
      $data_csv_contents[$sheetName] = file_get_csv_contents("https://docs.google.com/spreadsheets/d/".$sheetID."/export?exportFormat=csv");
      $data_sheets_fields_index[$sheetName] = array();
      foreach($data_sheets_fields[$sheetName] as $fieldName)
	for($index = 0; $index < count($data_csv_contents[$sheetName][0]); $index++)
	  if ($data_csv_contents[$sheetName][0][$index] == $fieldName)
	    $data_sheets_fields_index[$sheetName][$fieldName] = $index;
    }
  }

  // Now displays all data as HTML file
  {
       $html = "<html>\n<head>\n<meta charset='UTF-8'/>\n";
	$html .= "<style> \n";
	$html .= "td {word-wrap: break-word;} \n";
	$html .= "label {font-weight: bold;} \n";
	$html .= "</style>";
	$html .= '<link href="DataTables-1.10.18/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />';
	$html .= '<link href="DataTables-1.10.18/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css" />';
	$html .="\n</head>\n";
	
	$html .= "<body style='max-width:1100px;'>\n";
	$html .= "<div id='loader'><img src='loading.gif' alt='Chargement en cours' title='Chargement en cours'></div> \n";
	$html .= "<div id='tablesContent' style='display:none;'>\n";
    foreach($data_sheets_fields as $sheetName => $fieldNames) {
      $html .= "<h2> Table des <a target='_blank' href='https://docs.google.com/spreadsheets/d/".$data_sheets_IDS[$sheetName]."'>". $sheetName."</a></h2>\n";
	  $html .= "<table id='".$sheetName."Table' class='display' style='max-width:1100px;width:100%;'>\n";
      $html .= "<thead>\n  <tr>\n";
	  foreach($fieldNames as $fieldName) 
	    $html .= "    <th>".$fieldName."</th>\n";
      $html .= "  </tr>\n</thead>\n";
	  
	  $html .= "<tbody>\n";
      for($index = 0; $index < get_data_count($sheetName); $index++) {
		$empty = true;
	    $line = "  <tr>\n";
	    foreach($fieldNames as $fieldName){
	      $value = rtrim(get_data($sheetName, $fieldName, $index));
	      $empty = $empty && $value == "";
	      if (preg_match("/^URL/", $fieldName) && $value != ""){
			$line .= "    <td><a target='_blank'' href='".$value."'>url</a></td>\n";
	      }else{
	        $line .= "    <td>".$value."</td>\n";
          }
	    }
	    $line .= "  </tr>\n";
		if (!$empty){
	      $html .= $line;
        }
      }
	  
      $html .= "</tbody>\n</table>\n";
    }
	$html .= "</div>\n";
	//datatable
	$html .= "<script src='jQuery-3.3.1/jquery-3.3.1.min.js' type='text/javascript'></script>\n";
    $html .= "<script src='DataTables-1.10.18/js/jquery.dataTables.min.js' type='text/javascript'></script>\n";
	$html .= "<script src='DataTables-1.10.18/js/dataTables.responsive.min.js' type='text/javascript'></script>\n";
    $html .= "<script type='text/javascript'>\n";
    $html .= "$(document).ready( function () {\n";
	foreach($data_sheets_fields as $sheetName => $fieldNames) {
	  $html .= "  $('#".$sheetName."Table').DataTable({\n";
	  $html .= " responsive: true, \n";
	  $html .= " oLanguage: { \n";
      $html .= "   oPaginate: { \n";
      $html .= "     sFirst:'Première Page',\n";
      $html .= "     sLast: 'Dernière Page', \n";
      $html .= "     sNext: 'Page Suivante', \n";
      $html .= "     sPrevious: 'Page Précédente' \n";
      $html .= "  }, \n";
	  $html .= "  sInfo: 'Affichage des résultats _START_ à _END_ of _TOTAL_', \n";
      $html .= "  sLengthMenu: 'Afficher _MENU_ résultats', \n";
      $html .= "  sSearch: 'Rechercher ', \n";       
      $html .= "  sInfoThousands: ' ', \n";
      $html .= "  sEmptyTable: 'Aucune donnée à afficher', \n";
      $html .= "  sInfoEmpty: 'Aucune donnée à afficher' \n";
      $html .= "}, \n";
      $html .= "aLengthMenu: [[10, 20, 50,100, -1], [10, 20, 50,100, 'Tous les']], \n";
	  $html .= "dom: 'lfirtp' \n";
	  
      $html .="});\n";	
    }
	$html .= "$('#loader').hide(); \n";
	$html .= "$('#tablesContent').show(); \n";
    $html .= "} );\n";
    $html .= "</script>\n";
    $html .= "</body>\n</html>\n";
    file_put_contents("index.html", $html);
  }
}
?>
