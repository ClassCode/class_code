<?php 
/** print_r a PHP variable in an HTML output.
 * @param $var The PHP variable to print.
 * @param $header An optional header to print before the value.
 * - If false the value si not echoed, only returned.
 * @param $dump Whether to dump all variable property (if true) or simply print the variable value (if false)
 * @return The HTML text printing the value.
 */
function pprint_r($var, $header = "", $dump = false) {
  if ($dump) {
    ob_start();
    var_dump($var);
    $html = ob_get_clean();
  } else {
    $html = print_r($var, true);
  }
  $html = "<pre>$header$html</pre>";
  if ($header !== false)
    echo $html;
  return $html;
}
?>