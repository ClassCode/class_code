<?php // In included, forces error reporting, but skiping other spurious plugins errors
error_reporting(E_ALL | E_STRICT);
function error_for_all_handler($errno, $errstr, $errfile, $errline) {
  if (!preg_match("#.*/(wp-includes/plugin.php|wp-content/plugins/sociable/.*)#", $errfile)) {
    //-echo "<pre>"; debug_print_backtrace(); echo "</pre>";
    echo "<pre> In $errfile, line $errline : #$errno , '$errstr' </pre>";
  }
}
set_error_handler('error_for_all_handler');
?>