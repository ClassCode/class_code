<?php
/**
 * Defines extended user fields in the profile update menu.
 *
 * - Provides a user editable localisation functionnality, initialize from the user machine IP, if available. The results is, 
 *   - retrieved using the function <tt>get_user_meta($user_id, 'location', true);</tt>
 *   - providing the location as a JSON field of the form :
 *  <center><tt>{ 'city' = 'Antibes' , 'region' = 'Provence-Alpes-Côte d'Azur', 'country' = 'France', 'latitude' = '43.562', 'longitude' = '7.1278'}</tt></center>
 *
 * #### Implementation details
 * - Directly modifies the default wordpress user profiles, using the following tutorials
 * @see http://wpengineer.com/2173/custom-fields-wordpress-user-profile
 * @see http://premium.wpmudev.org/blog/how-to-simplify-wordpress-profiles-by-removing-personal-options
 * - Uses google API and HTML geolocation API
 * @see http://www.w3schools.com/html/html5_geolocation.asp
 *
 * \private && \deprecated
 * \ingroup userprofile
 * \extends Utility
 */
class extended_user_profile {
  //
  // Wordpress user profile modification mechanism
  // 
  function __construct() {
    // Hooks to modify the user panel
    add_action('show_user_profile', array($this, 'extended_user_profile_fields'));
    add_action('edit_user_profile', array($this, 'extended_user_profile_fields'));
    add_action('personal_options_update', array($this, 'save_extended_user_profile_fields'));
    add_action('edit_user_profile_update', array($this, 'save_extended_user_profile_fields'));
    // For google map API
    if (is_admin()) {
      add_action('wp_head', function() {
	  echo '<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />';
	});
      add_action('admin_enqueue_scripts', function() {
	  wp_enqueue_script('google_maps', "https://maps.google.com/maps/api/js?sensor=false");
	});
    }
  }
  /** Implements modifying personal options as jQuery page post-edition. \private */
  function extended_user_profile_fields($user) {
    echo '<script type="text/javascript">
  jQuery(document).ready(function($) {
    // Hides the profile options
    $("form#your-profile > h3:first").hide(); 
    $("form#your-profile > table:first").hide(); 
    $("form#your-profile").show(); 
    // Hides redundant name fields
    $(".user-nickname-wrap").hide();
    $(".user-display-name-wrap").hide();
    // Hides AMT fields 
    $(".user-amt_twitter_author_username-wrap").hide(); 
    $(".user-amt_facebook_author_profile_url-wrap").hide(); 
    $(".user-amt_googleplus_author_profile_url-wrap").hide();
    // Display additional contact field
    $("table.form-table:eq(2)").append("'.
      extended_user_profile::esc_js_string($this->extended_user_profile_location_field($user)).'");
    // Display additional info field
    $("table.form-table:eq(3)").append("'.
      extended_user_profile::esc_js_string($this->extended_user_profile_class_code_role_field($user)).'");
  });</script>
';
    // Echoes related javascript 
    $this->echo_location_script();
    $this->echo_class_code_role_script();
  }
  /** Escapes a string to be used as a JS string with double quote.
   * - Escapes ``"´´ chars and line end 
   * @param $string The input string.
   * @return The escaped string
   */
  static function esc_js_string($string) {
    return preg_replace("/\n/", "\\n", preg_replace("/\"/", "\\\"", $string));
  }
  /** Implements the save hook. \private */
  function save_extended_user_profile_fields($user_id) {
    if (current_user_can('edit_user', $user_id)) {
      update_user_meta($user_id, 'location', $_POST['location']);
      update_user_meta($user_id, 'class_code_role', $_POST['class_code_role']);
    }
  }

  //
  // Geolocalisation mechanism
  //

  /** Implements the additional user profile location field. \private */
  function extended_user_profile_location_field($user) {
    // Gets location from Web service and local user data
    $value = get_the_author_meta('location', $user->ID);
    $this->where = wp_parse_args($value != "" ? 
			   json_decode($value, true) :
			   array(),
			   array("city" => "", "region" => "", "country" => "France", "latitude" => "48.8", "latitude" => "2.2"));
    $html = '
  <tr>
    <th><label for="location">Ville, région</label></th>
    <td>
      <input type="hidden" id="location" name="location" value="'.esc_attr($value).'"/>
      <input onKeyup="user_profile_location_update();" on Keypress="return event.keyCode != 13;" size="32" type="text" name="location-city" id="location-city" value="'.$this->get_where('city').'" />
      <input onKeyup="user_profile_location_update();" on Keypress="return event.keyCode != 13;" size="32" type="text" name="location-region" id="location-region" value="'.$this->get_where('region').'"/>
      <input onKeyup="user_profile_location_update();" on Keypress="return event.keyCode != 13;" size="10" type="text" name="location-country" id="location-country" value="'.$this->get_where('country').'"/>
      <div style="margin:auto">Localisation : (<input onKeyup="user_profile_location_update();" on Keypress="return event.keyCode != 13;" size="12" type="text" name="location-latitude" id="location-latitude" value="'.$this->get_where('latitude').'"/>,
      <input onKeyup="user_profile_location_update();" on Keypress="return event.keyCode != 13;" size="12" type="text" name="location-longitude" id="location-longitude" value="'.$this->get_where('longitude').'"/>)</div>
      <div style="display:none;width=100%" id="geolocation_data_update"><a style="float:right" class="button" onClick="user_profile_geolocation_update(); return false;">Mettre à jour à partir de la géo-localisation</a></div>
      <div style="clear:both" class="description">Entrer la localisation permet de proposer des éléments de proximité</div>
      <div id="geolocation_display" style="margin:10px auto 0px;border: 1px solid #111;width:600px;height:300px"></div>
    </td>
  </tr>';
    return $html;
  }
  private function get_where($field, $default = "") {
    return isset($this->where[$field]) && $this->where[$field] != "" ? $this->where[$field] : $default;
  }
  /** Echoes the profile_location scripts. \private */
  function echo_location_script() { 
    echo extended_user_profile::render_location(array('zoom' => 8));
    echo '<script>
// Uses the HTML5 geolocation API
var user_profile_geolocation = undefined;
{
  // Gets the latitude,longitude
  if (navigator.geolocation)
   navigator.geolocation.getCurrentPosition(function(position) {

     user_profile_geolocation = position.coords;
     document.getElementById("geolocation_data_update").style.display = "block";

     // Uses google API for geocoder
     var geocoder = new google.maps.Geocoder;
     geocoder.geocode({ location : { lat: position.coords.latitude, lng: position.coords.longitude }}, 
       function(results, status) {
         if (status === google.maps.GeocoderStatus.OK) {
           if (results[1]) {
             var c = results[1]["address_components"];
             for(var i = 0; i < c.length; i++) {
               if (c[i]["types"][0] == "locality")
                 user_profile_geolocation.city = c[i]["long_name"];
               if (c[i]["types"][0] == "administrative_area_level_1")
                 user_profile_geolocation.region = c[i]["long_name"];
               if (c[i]["types"][0] == "country")
                 user_profile_geolocation.country = c[i]["long_name"];
             }
           }
         }
       });
    });
}
function user_profile_geolocation_update() {
  if (user_profile_geolocation.city)
    document.getElementById("location-city").value = user_profile_geolocation.city;
  if (user_profile_geolocation.region)
    document.getElementById("location-region").value = user_profile_geolocation.region;
  if (user_profile_geolocation.country)
    document.getElementById("location-country").value = user_profile_geolocation.country;
  document.getElementById("location-latitude").value = user_profile_geolocation.latitude;
  document.getElementById("location-longitude").value = user_profile_geolocation.longitude;
  user_profile_location_update();
}
function user_profile_location_update() {
  document.getElementById("location").value = JSON.stringify({ 
   city : document.getElementById("location-city").value,
   region : document.getElementById("location-region").value,
   country : document.getElementById("location-country").value,
   latitude : document.getElementById("location-latitude").value,
   longitude : document.getElementById("location-longitude").value
  });
  render_location_geolocation_display(document.getElementById("location-latitude").value, document.getElementById("location-longitude").value);
}
 setTimeout(function() { 
  render_location_geolocation_display('.$this->get_where('latitude', 0).','.$this->get_where('longitude', 0).'); 
}, 1000);
</script>';
  }
  /** Echoes a function displaying the google map in a HTML element
   * - The javascript function is of the form <tt>render_location_{id}(latitude, longitude)</tt> where 
   *   - <tt>{id}</tt> is the HTML element id, 
   *   - presumely a div typically :
   *     <tt>&lt;div style="margin:auto;border:1px solid #111;width:600px;height:300px;" id="geolocation_display">&lt;/div></tt>
   * @param $arguments An associative array of arguments with:
   * - <tt>id</tt> The map display HTML element id, This <tt>&lt;div></tt> element must be pre-defined. Default is <tt>geolocation_display</tt>
   * - <tt>zoom</tt> The map zoom, form 0 to 20. Default is 10.
   * - <tt>mode</tt> The map display mode <tt>ROADMAP, SATELLITE, HYBRID, TERRAIN</tt>. Default is <tt>ROADMAP</tt>.
   *
   * #### Implementation details
   * @see https://openclassrooms.com/courses/google-maps-javascript-api-v3
   * @see https://developers.google.com/maps/documentation/javascript/reference
   */
  static function render_location($arguments) {
    $arguments = wp_parse_args($arguments, array(
						 'zoom' => 10,
						 'mode' => 'ROADMAP',
						 'id' => 'geolocation_display',
						 ));
    return '
<script>
function render_location_'.$arguments['id'].'(latitude, longitude) {
  var pos_'.$arguments['id'].' = new google.maps.LatLng(latitude, longitude);
  if (map_'.$arguments['id'].' == undefined) {
    map_'.$arguments['id'].' = new google.maps.Map(document.getElementById("'.$arguments['id'].'"), {
      center: pos_'.$arguments['id'].',
      zoom: '.$arguments['zoom'].',
      mapTypeId: google.maps.MapTypeId.'.$arguments['mode'].'
    });
    new google.maps.Marker({
      position: pos_'.$arguments['id'].',
      map: map_'.$arguments['id'].'
    });
  } else {
    map_'.$arguments['id'].'.setCenter(pos_'.$arguments['id'].');
  }
}
var map_'.$arguments['id'].';
</script>';
  }

  //
  // Class'Code role menu implementation
  //

  /** Implements the additional user profile class_code_role field. \private */
  function extended_user_profile_class_code_role_field($user) {
    $value = get_the_author_meta('class_code_role', $user->ID);
    $answers = $value != "" ? json_decode($value, true) : array();
    $html = '
  <tr>
    <th><label for="class_code_role">Mon rôle dans Class´Code</label></th>
    <td>   
       <input type="hidden" id="class_code_role" name="class_code_role" value="'.esc_attr($value).'"/>';
    $this->echo_checkbox_count = 0;
    $html .= 
      $this->echo_checkbox($answers, 0, "animateur d´activités péri-scolaires", false) .
      $this->echo_checkbox($answers, 0, "animateur d´activités extra-scolaires", false) .
      $this->echo_checkbox($answers, 0, "professeur de primaire", false) .
      $this->echo_checkbox($answers, 0, "professeur de secondaire en:", "(maths, techno, autres)") .
      $this->echo_checkbox($answers, 0, "informaticien", "(chercheur, étudiant, industriel¸…)") .
      $this->echo_checkbox($answers, 1, "qui aimerait aussi faciliter les formations en présentiel", false) .
      $this->echo_checkbox($answers, 1, "qui va juste suivre la formation", false) .
      "<div>autre :</div>" .
      $this->echo_checkbox($answers, 1, "parent d´élèves", false) .
      $this->echo_checkbox($answers, 1, "responsable de structure", "(je précise laquelle)") .
      $this->echo_checkbox($answers, 1, "étudiant en", "(je précise la discipline)") .
      $this->echo_checkbox($answers, 1, "un simple curieux qui vient pour", "(et oui bienvenue aussi!)");
    $html .= '
    </td>
  </tr>';
    return $html;
  }
  /** Echoes a checkbox field with additional information. \private */
  function echo_checkbox($answers, $tab, $choice, $more) {
    $html = "<div style='margin-left:".(20 * $tab)."px;'>";
    $nn = ++$this->echo_checkbox_count;
    $html .= "<input type='checkbox' id='class_code_role-$nn' name='class_code_role-$nn' value='".esc_attr($choice)."' ".(isset($answers[$choice]) ? " checked='true'" : "")." onChange='class_code_role_update();'/>$choice\n";
    $html .= "<input onKeyup='class_code_role_update();' onKeypress='return event.keyCode != 13;' size='16' type='".($more ? "text" : "hidden")."' id='class_code_role-$nn-more' name='class_code_role-$nn-more' value='".(isset($answers[$choice]) ? esc_attr($answers[$choice]) : "")."'/>";
    $html .= $more ? $more : "";
    $html .= "</div>";
    return $html;
  }
  /** Echoes the class_code_role scripts, must called after extended_user_profile_class_code_role_field(). \private */
  function echo_class_code_role_script() {
    echo '<script>
function class_code_role_update() {
  var answers = "";
  for(var n = 1; n <= '.$this->echo_checkbox_count.'; n++)
    if (document.getElementById("class_code_role-"+n).checked)
      answers += (answers == "" ? "" : ",") + 
        "\"" + document.getElementById("class_code_role-"+n).value + "\" : \"" +
        document.getElementById("class_code_role-"+n+"-more").value + "\""; 
  document.getElementById("class_code_role").value = "{" + answers + "}";
}
</script>';
  }
}
new extended_user_profile();
?>