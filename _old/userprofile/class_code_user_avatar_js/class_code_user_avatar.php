<?php // Defines the functions to display an avatar in a page

/** \class class_code_user_avatar_echo
 * Displays the user avatar edition/display.
 *
 * Init:
 * - class_code_user_avatar_echo_enqueue_script() is called in his file here to enqueue the required JS scripts
 *
 * Display :
 * - class_code_user_avatar_echo_display($avatar) is used to display the avatar providing the JSON avatar parameter has been saved in the user parameters
 *
 * Editor :
 * - class_code_user_avatar_echo_editor() is used to display the editor
 * - class_code_user_avatar_echo_editor_save() may be used to save the JSON avatar parameter has been saved as a user parameters
 * 
 * \private && \deprecated
 * \ingroup userprofile
 * \extends Utility
 */

/** This function is to be called to enqueue the JS scripts. */
function class_code_user_avatar_echo_enqueue_script() {
  add_action('wp_enqueue_scripts', function() {
      wp_enqueue_script('class_code_avatar_angular', plugins_url().
			"/class_code/userprofile/class_code_user_avatar_js/angular.min.js");
      wp_enqueue_script('class_code_avatar', plugins_url().
			"/class_code/userprofile/class_code_user_avatar_js/avatar.js?v=13");
    });
}
class_code_user_avatar_echo_enqueue_script();
/** This function echoes the avatar editor.
 * @param $avatar A JSON string that specifies the initial avatar properties.
 */
function class_code_user_avatar_echo_editor($avatar='{color:"#00FF00",Coiffure:1,Corps:1,Pieds:1,Mains:1,Yeux:1,Bouche:1}') {
  $avatar_img =  plugins_url()."/class_code/userprofile/class_code_user_avatar_js/avatar_img";
  echo '<script>myAvatarValue = '.$avatar.';</script>'."\n".'
<div id="avatarCapmathsE" ng-app="avatarCapmathsE" ng-controller="AvatarCtrl">
<style>
.noselect {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
</style>
<table id="class_code_user_avatar_save" class="noselect">
   <tr>
      <td>
         <div ng-repeat="part in parts" style="width:50px;padding:2px;height:20px;font-weight:bold" ng-style="{color: arrows.color}">
            {{ part.name }}
         </div>
      </td>
      <td>
         <div ng-repeat="part in parts" style="padding:2px;height:20px;">
            <div ng-include="\''.$avatar_img.'/RightArrow.svg\'" ng-click="changePart(part, -1)" style="width:20px;cursor:pointer"></div>
         </div>
      </td>
      <td >
         <div class="big" ng-repeat="transform in [bigTransform]" style="width:150px;height:150px;" ng-include="\''.$avatar_img.'/All.svg\'" onload="avatarLoaded(\'big\')">
         </div>
      </td>
   </tr>
   <tr>
      <td colspan="3" style="padding-left:4px;">
         <div ng-repeat="color in colors" style="width:21px;height:20px;display:inline-block;margin:0px;cursor:pointer" ng-include="\''.$avatar_img.'/Paint.svg\'" ng-click="changeColor(color)"></div>
      </td>
   </tr>
</table>
</div><script>angular.bootstrap(document.getElementById("avatarCapmathsE"), [\'avatarCapmathsE\']);</script>'."\n";
}
/** This function echoes a button to save the avatar editor result. 
 * - This simply generates a minimal form that reads the JS variable 'myAvatarValue' that contains the JSON avatar parameter and then call a page with ?class_code_user_avatar_data="<the JSON avarar parameter?".
*/
function class_code_user_avatar_echo_editor_save() {
echo '<form method="post" action="#class_code_user_avatar_save" onSubmit="document.getElementById(\'class_code_user_avatar_data\').value = JSON.stringify(myAvatarValue);">
  <input type="hidden" id="class_code_user_avatar_data" name="class_code_user_avatar_data" value="undefined"/>
  <input style="display:block;margin-left:auto;margin-right:auto;" type="submit" class="button" value="Sauvegarder"/>
</form>';
}
/** This function echoes the avatar display. 
 * @param $avatar A JSON string that specifies the avatar properties.
 */
function class_code_user_avatar_echo_display($avatar='{color:"#FF0000",Coiffure:1,Corps:1,Pieds:1,Mains:1,Yeux:1,Bouche:1}') {
  $avatar_img =  plugins_url()."/class_code/userprofile/class_code_user_avatar_js/avatar_img";
  echo '<script>myAvatarValue = '.$avatar.';</script>'."\n".'
<div id="avatarCapmaths" ng-app="avatarCapmaths" ng-controller="AvatarCtrl">
<div class="small" ng-repeat="transform in [smallTransform]" style="margin:2px;width:80px;height:80px;" ng-include="\''.$avatar_img.'/All.svg\'" onload="avatarLoaded(\'small\')">
</div>
</div><script>angular.bootstrap(document.getElementById("avatarCapmaths"), [\'avatarCapmaths\']);</script>';
}

?>