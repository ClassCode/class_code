<?php // Voici ma page pour gérer mon panier de lien
include_once(plugin_dir_path( __FILE__ ).'../posttype/include_post_type.php');
/** \class class_code_user_basket_menu
 * Defines the Class'Code link basket user menu. 
 *
 * \private && \deprecated
 * \ingroup userprofile
 * \extends user_menu
 */
include_once(plugin_dir_path( __FILE__ ).'../metabox/bookmarks_meta_box.php');
function class_code_user_basket_menu() {
  new bookmarks_meta_box("basket", array(
					 'title' => 'Mon panier de lien',
					 'post_type' => false,
					 'fields' => array("title", "icon"),
					 'default' => '[
     { "title":"Class´Code: Maîtriser la pensée informatique pour la transmettre", "href":"https://classcode.fr", "icon":"https://classcode.fr/files/2015/09/LOGO-ClassCode-coul.jpg"},
     { "title":"Pixees: ressources pour les sciences du numérique", "href":"https://pixees.fr", "icon":"https://pixees.fr/wp-content/uploads/2014/07/favicon.png"}
    ]',
					 ));
}
class_code_user_basket_menu();
?>