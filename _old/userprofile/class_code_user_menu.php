<?php // Voici le menu de mon espace personnel
include_once(plugin_dir_path( __FILE__ ).'../posttype/include_post_type.php');
include_once('class_code_user_avatar_echo.php');
/*//* \class class_code_user_menu
 * Defines the Class'Code general user menu. 
 *
 * \ingroup userprofile
 * \extends user_menu
 */
function class_code_user_menu() {
  $post_id = 6200; // 18076
  echo str_replace(']]>', ']]&gt;', apply_filters('the_content', get_post($post_id)->post_content));
}
class_code_user_menu();
?>