<?php // Voici ma page pour gérer mon avatar
include_once(plugin_dir_path( __FILE__ ).'../posttype/include_post_type.php');
include_once('class_code_user_avatar_js/class_code_user_avatar.php');
include_once('class_code_user_avatar_echo.php');
/** \class class_code_user_avatar_menu
 * Defines the Class'Code avatar user menu. 
 *
 * \private && \deprecated
 * \ingroup userprofile
 * \extends user_menu
 */
function class_code_user_avatar_menu() {

  // Udates the user database
  {
    if (isset($_REQUEST['class_code_user_avatar_data'])) {
      update_user_meta(get_current_user_id(), 'class_code/avatar', $_REQUEST['class_code_user_avatar_data']);
    }
  }
  // Gets the avatar data
  $data = is_user_logged_in() ? get_user_meta(get_current_user_id(), 'class_code/avatar', true) : "undefined";
  {
    if ($data && $data != 'undefined') 
      class_code_user_avatar_echo_editor($data);
    else
      class_code_user_avatar_echo_editor();
    class_code_user_avatar_echo_editor_save();
  }
}
class_code_user_avatar_menu();
?>