<?php // Defines the functions to display an avatar in a page
include_once('class_code_user_avatar_js/class_code_user_avatar.php');

/** \class class_code_user_avatar_echo
 * Displays the user avatar in a page.
 *
 * \private && \deprecated
 * \ingroup userprofile
 * \extends Utility
 */
function class_code_user_avatar_echo() {
  // Gets the avatar data
  $data = is_user_logged_in() ? get_user_meta(get_current_user_id(), 'class_code/avatar', true) : "undefined";
  //echo '<div style="width:100%"><div style="width:82px;margin:10px auto 10px auto">';
  {
    if ($data && $data != 'undefined') 
      class_code_user_avatar_echo_display($data);
    else
      class_code_user_avatar_echo_display();
  }
  //echo '</div></div>';
}
?>