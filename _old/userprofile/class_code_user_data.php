<?php // Voici ma page pour afficher mes données 
/** \class class_code_user_data
 * Defines the Class'Code data rendering. 
 *
 * \private && \deprecated
 * \ingroup userprofile
 * \extends user_menu
 */
function class_code_user_data() {

  // User profile data 
  {
    echo "<b>Les données de mon profil:</b><table>";

    foreach(array(
		  "user_firstname" => "Prénom", 
		  "user_lastname" => "Nom", 
		  "user_login" => "Identifiant", 
		  "user_email" => "E-mail", 
		  "user_url" => "Site Web") as $field => $label) {
      $value = wp_get_current_user()->$field;
      if ($value != "")
	echo "<tr><td>$label</td><td>$value</td></tr>\n";
    }
    
    $location = get_the_author_meta('location', wp_get_current_user()->ID);
    echo "<tr><td>Localisation</td><td>$location</td></tr>\n";
    $class_code_role = get_the_author_meta('class_code_role', wp_get_current_user()->ID);
    echo "<tr><td>Rôle dans Class´Code</td><td>$class_code_role</td></tr>\n";

    echo "</table><hr/>";
  }
  // Parcours user data 
  {
    echo "<b>Le parcours de découverte de la formation:</b><pre>";

    include_once(plugin_dir_path( __FILE__ ).'../posttype/parcours_taxinomy_result.php');
    $result = parcours_taxinomy_result::get_user_score(wp_get_current_user()->user_login, parcours_taxinomy_result::get_questions('decouverte-de-la-formation'));
    foreach($result as $key => $value) {
      if ($value == 0) 
	unset($result[$key]);
    }
    print_r($result);
    echo "</pre><hr/>";
  }
}
class_code_user_data();
?>