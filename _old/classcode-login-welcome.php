<?php // Redefine user notification function
if (!function_exists('wp_new_user_notification')) {
  function wp_new_user_notification( $user_id, $plaintext_pass = '' ) {
    $user = new WP_User($user_id);
    $user_login = stripslashes($user->user_login);
    $user_email = stripslashes($user->user_email);
    
    @wp_mail(get_option('admin_email'), "Nouvel enregistrement sur Class´Code", sprintf("L'utilisateur %s<%s> s'est enregistré #passwd=%d", $user_login, $user_email, !empty($plaintext_pass)));
    
    if (empty($plaintext_pass))
      return;
    
    $message = "Vous venez de vous enregistrer sur Class´Code : Bienvenue.\r\n\r\n";
    $message .= "Voici le lien pour vous connecter: ".wp_login_url().".\r\n";
    $message .= "  Identifiant  : ".$user_login."\r\n";
    $message .= "  Mot de passe : ".$plaintext_pass."\r\n";
    $message .= "En cas de problème contacter classcode-accueil@inria.fr, nous sommes à votre disposition\r\n\r\n";
    $message .= "À bientôt sur Class´Code!\r\n";
    wp_mail($user_email, "Bienvenue sur Class´Code", $message);
    
  }
}
?>
